drop database if exists `woodoo`;
create database `woodoo`;
use `woodoo`;

# 24-09-2016

CREATE TABLE USER(
	user_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	first_name text,
	last_name text,
	dob varchar(50),
	gender varchar(10),
	email varchar(50),
	mobile_no bigint unsigned,
	password text,
	phone_value bigint unsigned,
	state text,
	city text,
	pin_code text,
	address_one text,  # pic, video
	address_two text,
	membership_plan text,
	facebook_id varchar(50),
	my_referral_code varchar(15),
	used_referral_code varchar(15),
	otherCityName varchar(45),  
	PRIMARY KEY(user_id),
	INDEX user_table_index0(email),
	INDEX user_table_index1(facebook_id),
	INDEX user_table_index2(user_id),
	INDEX uesr_table_index3(my_referral_code),
	INDEX uesr_table_index4(used_referral_code),
	UNIQUE KEY user_table_referral_code(my_referral_code)
	) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
	

CREATE TABLE CONVERSION_DATA(
	id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	offerName TEXT,
	user_id BIGINT UNSIGNED NOT NULL,
    date_time datetime,
    offer_id BIGINT,
    approved_payout DOUBLE,
    conversion_status BOOLEAN,
    conversion_time BIGINT UNSIGNED,
    conversion_id BIGINT UNSIGNED NOT NULL,
    status varchar(10),
    users_payout BIGINT UNSIGNED,
    
	index(offer_id),
	index(conversion_time),
	index(user_id),
	index(conversion_id),
	index(status),

	PRIMARY KEY(id)
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE WOO_LOG(
	id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	user_id BIGINT UNSIGNED NOT NULL,
    date_time BIGINT UNSIGNED NOT NULL,
    woo_point INT,
    
	index(user_id),

	PRIMARY KEY(id)
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;









//new   

CREATE TABLE `woodoo`.`MANUAL_ADDS` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `url` TEXT NULL,
  `description` TEXT NULL,
  `urlToImage` TEXT NULL,
  `title` TEXT NULL,
  PRIMARY KEY (`id`));
  
  ALTER TABLE `woodoo`.`MANUAL_ADDS` 
ADD COLUMN `screen_no` INT NULL AFTER `title`,
ADD COLUMN `status` TINYINT(1) NOT NULL DEFAULT 0 AFTER `screen_no`;

ALTER TABLE `woodoo`.`USER` 
ADD COLUMN `signUpDate` VARCHAR(45) NULL AFTER `otherCityName`,
ADD COLUMN `age` BIGINT(20) NULL AFTER `signUpDate`;

ALTER TABLE `woodoo`.`MANUAL_ADDS` 
ADD COLUMN `endDate` BIGINT(20) NULL AFTER `status`;


ALTER TABLE `woodoo`.`USER` 
ADD COLUMN `status` TINYINT NULL AFTER `age`;


24th May
ALTER TABLE `woodoo`.`MANUAL_ADDS` 
ADD COLUMN `startDate` BIGINT(20) NULL AFTER `endDate`;


21 June
ALTER TABLE `woodoo`.`CONVERSION_DATA` 
ADD COLUMN `hour` BIGINT(20) NULL AFTER `left_points`;

22 June
ALTER TABLE `woodoo`.`CONVERSION_DATA` 
ADD COLUMN `uniqueId` BIGINT(20) NULL AFTER `hour`;


