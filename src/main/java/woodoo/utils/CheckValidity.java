package woodoo.utils;



import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import woodoo.model.NotNull;

public final class CheckValidity {
	
Class clazz;
	
	public CheckValidity(Class clazz) {
		super();
		this.clazz = clazz;
	}
	
	public static CheckValidity getInstance(Class clazz){
		return new CheckValidity(clazz);
	}

/*	public String check(Class<?> clazz,Object ...objects){
		for(Integer i=1; i<=objects.length; i++){
			if(objects[i-1]==null){
				return dsf(clazz,i);
			}
		}
		return null;
	}*/

	
	public int check(Object ...objects){
		for(Integer i = 1; i <= objects.length; i++){
			if(objects[i-1] == null){
				return i;
			}
		}
		return 0;
	}
	
//	public String dsf(Class<?> clazz,Integer i){
//		User user  = new User();
//		int j = 1;
//		for(Field field  : clazz.getDeclaredFields()){
//			if(field.isAnnotationPresent(NotNull.class)){
//				if(i.equals(j)){
//					return field.getName();
//				}
//				j++;
//			}
//		}
//		return null;
//	}
	
	public String check(Object object) throws IllegalArgumentException, IllegalAccessException{
		Field[] fields = clazz.cast(object).getClass().getDeclaredFields();
		
		for(Field field  : fields){
			field.setAccessible(true);

			if(field.isAnnotationPresent(NotNull.class) && field.get(object) == null){
				return field.getName();
			}
		}
		return null;
	}
	
}
