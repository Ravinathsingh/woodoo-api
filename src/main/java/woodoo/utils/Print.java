package woodoo.utils;


import java.io.IOException;
import java.util.Date;
import java.util.Scanner;

public class Print {
	public static boolean  debug = true;
	
	public static void debug(Object x, String className){
		if(debug)
			System.out.println(x + " " + new Date(Timings.getCurrentTime()) + " " + className );
	}
	
	public static void live(Object x, String className){
		System.out.println(x +  " " + new Date(Timings.getCurrentTime()) + " " + className);
	}
	
	public static void debug(Object x){
		if(debug)
			System.out.println(x + " " + new Date(Timings.getCurrentTime()));
	}
	
	public static void live(Object x){
		System.out.println(x +  " " + new Date(Timings.getCurrentTime()) );
	}
//	
//	public static void main(String[] args) {
//		Print.debug(1,null);
//		Print.live(2,null);
//	}
	
	
	public static int GetJumpCount(int input1,int input2,int[] input3)
    {
	    //Write code here
	    int noOfJumps = 0;
	    int leftHeight = input2;
	    int ableToJump = input1-input2;
	    
	    for(int i : input3)
	    {
	        if(i <= input1)
	        {
	            noOfJumps++;
	            System.out.println(i+"   1   "+noOfJumps);
	        }
	        else
	        {
                noOfJumps = noOfJumps+(i / ableToJump);
                int remaining = i % ableToJump;
                if(remaining > 0 && (ableToJump+remaining) != input1)
                {
    	            System.out.println(i+"   2   "+noOfJumps);
                    noOfJumps ++;
                }
	            System.out.println(i+"  3   "+noOfJumps);
	        }
	    }
	    
	    
	    return noOfJumps;
    }
	 public static void main(String[] args) throws IOException{
	        Scanner in = new Scanner(System.in);
	        int output = 0;
	        int ip1 = Integer.parseInt(in.nextLine().trim());
	        int ip2 = Integer.parseInt(in.nextLine().trim());
	        int ip3_size = 0;
	        ip3_size = Integer.parseInt(in.nextLine().trim());
	        int[] ip3 = new int[ip3_size];
	        int ip3_item;
	        for(int ip3_i = 0; ip3_i < ip3_size; ip3_i++) {
	            ip3_item = Integer.parseInt(in.nextLine().trim());
	            ip3[ip3_i] = ip3_item;
	        }
	        output = GetJumpCount(ip1,ip2,ip3);
	        System.out.println(String.valueOf(output));
	    }
}
