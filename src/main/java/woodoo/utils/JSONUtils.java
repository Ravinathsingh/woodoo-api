package woodoo.utils;

import java.util.List;

import org.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JSONUtils {
	
	public static GsonBuilder gsonBuilder = new GsonBuilder().serializeNulls();
	public static Gson gson = gsonBuilder.create();
	
	public static String getSuccessJson() {
		JSONObject obj = new JSONObject();
		obj.put("status", "success");
		return obj.toString();
	}
	
	public static  String getSuccessJson(Object o){
		System.out.println(o);
		JSONObject obj = new JSONObject();
		obj.put("status", "success");
		if(o==null)
		{
			obj.put("data", JSONObject.NULL);
		}
		else
		{
			obj.put("data", new JSONObject(gson.toJson(o)));
		}
		return obj.toString();
	}
	
	public static  String getSuccessJsonUserLogin(Object o, Boolean is_olduser){
		System.out.println(o);
		JSONObject obj = new JSONObject();
		obj.put("status", "success");
		if(o==null)
		{
			obj.put("data", JSONObject.NULL);
		}
		else
		{
			obj.put("data", new JSONObject(gson.toJson(o)));
		}
		obj.put("is_olduser", is_olduser);
		return obj.toString();
	}
	
	public static  String getSuccessJson1(Object o){
		System.out.println(o);
		JSONObject obj = new JSONObject();
		obj.put("status", "success");
		if(o==null)
		{
			System.out.println("inside null.");
			obj.put("data", JSONObject.NULL);
		}
		else
		{
			System.out.println("Inside data."+o);
			obj.put("data", new JSONObject((o)));
		}
		return obj.toString();
	}
	
	public static  String getSuccessJson(List list){
		JSONObject obj = new JSONObject();
		obj.put("status", "success");
		obj.put("data", new org.json.JSONArray(gson.toJson(list)));
		return obj.toString();
	}
	
	public static  String getSuccessJson(String message){
		JSONObject obj = new JSONObject();
		obj.put("status", "success");
		obj.put("message", message);
		return obj.toString();
	}
	
	public static  String getFailJson(String reason){
		JSONObject obj = new JSONObject();
		obj.put("status", "fail");
		obj.put("reason", reason);
		return obj.toString();
	}
	
	public static  String getFailJsonWithDoesNotExist(){
		JSONObject obj = new JSONObject();
		obj.put("status", "fail");
		obj.put("reason", "Entity does not exist");
		return obj.toString();
	}
	
	public static String getFailJson() {
		JSONObject obj = new JSONObject();
		obj.put("status", "fail");
		return obj.toString();
	}

	public static String getFailWithReasonJson(String reason) {
		JSONObject obj = new JSONObject();
		obj.put("status", "fail");
		obj.put("reason", reason);
		return obj.toString();
	}
}
