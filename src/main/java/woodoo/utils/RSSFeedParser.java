package woodoo.utils;


import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;


import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import woodoo.views.NewsView;


public class RSSFeedParser {
    static final String TITLE = "title";
    static final String DESCRIPTION = "description";
    static final String CHANNEL = "channel";
    static final String LANGUAGE = "language";
    static final String COPYRIGHT = "copyright";
    static final String LINK = "link";
    static final String AUTHOR = "author";
    static final String ITEM = "item";
    static final String PUB_DATE = "pubDate";
    static final String GUID = "guid";
    static final String MEDIA = "media";
    static final String CONTENT = "content";

    final URL url;

    public RSSFeedParser(String feedUrl) {
        try {
            this.url = new URL(feedUrl);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public ArrayList<NewsView> readFeed() {
    	ArrayList<NewsView> news=new ArrayList<NewsView>();
        try {
            boolean isFeedHeader = true;
            // Set header values intial to the empty string
            String description = "";
            String title = "";
            String link = "";
            String Image = "";
            String copyright = "";
            String author = "";
            String pubdate = "";
            String guid = "";

            // First create a new XMLInputFactory
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            // Setup a new eventReader
            InputStream in = read();
            XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
            // read the XML document
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                if (event.isStartElement()) {
                	 String localPart = event.asStartElement().getName()
                             .getLocalPart();
                     if(localPart.equalsIgnoreCase(ITEM))
                     {
                     	if (isFeedHeader) {
                     		
                         isFeedHeader = false;
                     	}
                     	                        
                                        
                     }else if(localPart.equalsIgnoreCase(TITLE))
                     {
                     	if(!isFeedHeader)
                     	 title = getCharacterData(event, eventReader);
                     }
	                else if(localPart.equalsIgnoreCase(PUB_DATE))
	                {
	                	if(!isFeedHeader)
	                	 pubdate = getCharacterData(event, eventReader);
	                }
                     else if(localPart.equalsIgnoreCase(DESCRIPTION))
                     {
                     	if(!isFeedHeader)
                     	{
                     	description = eventReader.getElementText();
                     	boolean isFound = description.indexOf("<img src") !=-1? true: false;
                     	if(isFound)
                     	{
                     		Image=description.substring(description.indexOf("src")+5,description.indexOf("border=\"1")-2);
                     		
                     	}else
                     	{
                     		Image="";
                     	}
                     	boolean isFound2 = description.indexOf("a") !=-1? true: false;
                     	if(isFound2)
                     	{
                     	  description=getTagValue(description, "a");
                     	}else
                     	{
                     		description="";
                     	}
                     	}
                    }else if(localPart.equalsIgnoreCase(LINK))
                    {
                 	   if(!isFeedHeader)
                 	   link = getCharacterData(event, eventReader);
                   }
                    else if(localPart.equals(CONTENT)) {
                        Iterator<Attribute> atr = event.asStartElement().getAttributes();
                        while (atr.hasNext()) {

                            Attribute a = atr.next();
                            if(a==null)
                                continue;

                            QName qName = a.getName();
                            if(qName==null)
                                continue;

                            String name = a.getName().getLocalPart();
                            if (name!=null && name.equals("url")) {
                                Image = a.getValue();
                            }
                        }
                    }
                } else if (event.isEndElement()) {
                    if (event.asEndElement().getName().getLocalPart() == (ITEM)) {
                    	
                    	if(checkDate(pubdate)){
                    	NewsView message = new NewsView();
                        message.setAuthor("");
                        message.setDescription(description);
                        message.setPublishedAt(pubdate);
                        message.setTitle(title);
                        message.setUrl(link);
                        if(Image.length()>5)
                        {
                        message.setUrlToImage(Image);
                        }else
                        {
                        message.setUrlToImage("https://i.ibb.co/s9W8RCJ/News-Icon-Final-301118.png");	
                        }
                        news.add(message);
                    	}
                        event = eventReader.nextEvent();
                        continue;
                    }
                }
            }
        } catch (XMLStreamException e) {
            throw new RuntimeException(e);
        }
        return news;
    }
    public static String getTagValue(String xml, String tagName){
        return xml.split("<"+tagName)[1].split("</"+tagName+">")[0].split(">")[1];
    }
    
    private boolean checkDate(String date)
    {
    	DateFormat format = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z");
		Date updateLast;
		try {
			updateLast = format.parse(date);
		
		Date currentDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		if(sdf.format(updateLast).equals(sdf.format(currentDate)))
		{
			return true;
		}else if(sdf.format(updateLast).equals(sdf.format(yesterday())))
		{
			return true;
		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
    }
    private static Date yesterday() {
	    final Calendar cal = Calendar.getInstance();
	    cal.add(Calendar.DATE, -1);
	    return cal.getTime();
	}

    private String getCharacterData(XMLEvent event, XMLEventReader eventReader)
            throws XMLStreamException {
        String result = "";
        event = eventReader.nextEvent();
        if (event instanceof Characters) {
            result = event.asCharacters().getData();
        }
        return result;
    }

    private InputStream read() {
        try {
            return url.openStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
