package woodoo.utils;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.UUID;
import java.util.concurrent.Callable;
import org.springframework.web.multipart.MultipartFile;

import woodoo.model.Tuple;

public class ImageUploadThread implements Callable<Tuple<String, String>> 
{
	MultipartFile file;
	String fileName;	
	String content_type;
		
		public ImageUploadThread(MultipartFile file, String fileName, String content_type) {
			super();
			this.file = file;
			this.fileName = fileName;
			this.content_type = content_type;
		}

		public Tuple<String, String> call() throws Exception {
			 Tuple<String,String> tuple = new Tuple();
//			 String extension = getExtension(content_type);
//			 System.out.println("extension = " + extension);
			 byte[] byteArr = file.getBytes();
			 InputStream inputStream = new ByteArrayInputStream(byteArr);
			 tuple.o1 = fileName;
			 tuple.o2 = AwsS3Utils.upload(UUID.randomUUID().toString()+"_"+fileName,inputStream,getExtensionFromfilename(fileName));
			 return tuple;
		}
		
		
		public static String getExtension(String content_type){
			int i  = content_type.lastIndexOf("/", content_type.length());
			return content_type.substring(i+1, content_type.length());
		}
		
		public static String getExtensionFromfilename(String file_name){
			int i  = file_name.lastIndexOf(".", file_name.length());
			return file_name.substring(i+1, file_name.length());
		}
		
		
		public static void main(String[] args) {
			getExtension("sfd");
		}

	
}
