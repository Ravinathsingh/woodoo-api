package woodoo.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class Timings {
	public final static Long  millisInADay = 24*60*60*1000l;   
	public final static Long  TEN_MIN = 10*60*1000l;  
	public final static Long  TWENTY_MIN = 20*60*1000l; 
	public final static Long  TWENTY_FOUR_HRS = 24*60*60*1000l; 
	
	
	
	public enum Day{
		Sunday,
		Monday,
		Tuesday,
		Wednesday,
		Thursday,
		Friday,
		Saturday;
	}
	
	public class Timezone{
		public static final long IST = 11*30*60*1000;
	}
	
	
//	public enum Timezone{
//		IST(11*30*60*1000);
//		
//		private final long offset;
//		
//		private Timezone(long offset) {
//			this.offset = offset;
//		}
//		
//	}
	
	public static Long getAge(long dob) {

	    Calendar calendar = Calendar.getInstance();

	    calendar.setTimeInMillis(dob);

	    Long dobYear = (long) calendar.get(Calendar.YEAR);

	    calendar.setTimeInMillis(System.currentTimeMillis());

	    Long currentYear = (long) calendar.get(Calendar.YEAR);

	    return currentYear - dobYear;

	}

	
	public static int getDay(){
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		cal.setTimeInMillis(cal.getTimeInMillis()+11*30*60*1000);
		int  i = cal.get(cal.DAY_OF_WEEK);
		return i;
	}
	
	public static Day getDayasName(){
		int i = getDay();
		
		switch (i) {
			case 1:{
			return Day.Sunday;
			
		}
		case 2:{
			return Day.Monday;
					}
		case 3:{
			return Day.Tuesday;
			
		}
		case 4:{
			return Day.Wednesday;
			
		}
		case 5:{
			return Day.Thursday;
			
		}
		
		case 6:{
			return Day.Friday;
			
		}
		case 7:{
			return Day.Saturday;
			
		}
		default:{
			return null;
			
		}
	}
		
	}
		
	public static long getOffsetTime(long time){
	
		long offset = time % (24*60*60*1000);
		return offset;
	}
	// i.e current day starting time[12:00 AM][or 00:00:00 in UTC]
	public static long getAbsoluteTime(long offset){
		Calendar cal = Calendar.getInstance();
		long currentTime = cal.getTimeInMillis()+11*30*60*1000;//(i.e 5:30 hr*60min*60sec*1000 Note 11*30 min = 5:30 hr*60min  )
		long q = offset/(24*60*60*1000);
		long r = offset%(24*60*60*1000);
		long absoluteTime = (currentTime - getOffsetTime(currentTime)) + q*(24*60*60*1000) + r;
		return absoluteTime;
	}
	// i.e current day starting time[12:00 AM][or 00:00:00 in UTC]
	public static long getAbsoluteTime(long offset,Long epochtime){
		long currentTime = epochtime;
		long q = offset/(24*60*60*1000);
		long r = offset%(24*60*60*1000);
		long absoluteTime = (currentTime - getOffsetTime(currentTime)) + q*(24*60*60*1000) + r;
		return absoluteTime;
	}
	//method to Get Specified No. of Days i.e 7(week)duration or month
	public static Long getTimeAfterDays(long absoluteStartTimeOfDay, Integer days) {
		return absoluteStartTimeOfDay + millisInADay *(days-1);
		
	}
	
	public static long getCurrentTime() {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		long time = cal.getTimeInMillis();
		return time;
	}
	
	public static long getCurrentTime(long timezone) {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		long time = cal.getTimeInMillis() + timezone;
		return time;
	}

	public static long getNotificationSendingThresholdTime() {
		return getCurrentTime() - 24 * 3600 * 1000;
	}

	public static long getInactiveTime() {
		return getCurrentTime() - 7 * 24 * 3600 * 1000;
	}
	
	public static void printCurrentTime() {
		
		DateFormat format = new SimpleDateFormat("dd MM yyyy hh:mm a");
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		Date date = new Date(getCurrentTime());
		System.out.println("Api Call Time : "+format.format(date));
	}
	
	
	public static void main(String ss[] ) throws ParseException{

		System.out.println(getAge(736540200000l));
		
//		Timings.printCurrentTime();
//		Gson gson = new Gson();
//		System.out.println("Day of week = "+getDay());
//		System.out.println("Get Specified No. of Days = "+getTimeAfterDays(Timings.getAbsoluteTime(0, Timings.getCurrentTime()), 7));
//		System.out.println("Get Notification Sending Threshold Time = "+getNotificationSendingThresholdTime());
//		System.out.println("Get Inactive Time  = "+getInactiveTime());
//		System.out.println("Get Absolute Time 1 = "+getAbsoluteTime(0l));
//		System.out.println("Absoulute_start_time_of_today = "+getAbsoluteTime(0l, Timings.getCurrentTime()));
//		long utc_time = getOffsetTime((Timings.getCurrentTime()) + 11*30*60*1000);
//		System.out.println("Get Off set Time  = "+utc_time);
//		long ten_min = utc_time + Timings.TEN_MIN;
//		long twenty_min = utc_time + Timings.TWENTY_MIN;
//		System.out.println("Ten Min  = "+ten_min+","+"Twenty min = "+twenty_min);
//		System.out.println("Calendar  = "+gson.toJson(Calendar.getInstance()));
//		System.out.println("Current IST in millis  = "+Calendar.getInstance().getTimeInMillis());
//		System.out.println("UTC + 5:30 hr  = "+(Calendar.getInstance().getTimeInMillis()+11*30*60*1000));
//		System.out.println("System time in millis  = "+System.currentTimeMillis());
//		System.out.println("Get Absolute Time 3  = "+(getAbsoluteTime(0l, 1454739803886l)+Timings.TWENTY_FOUR_HRS));
//		System.out.println("long size = "+Long.SIZE);
//		// Time_of_start_of_this_week since last sunday
//		System.out.println("Time_of_start_of_this_week = "+Timings.getAbsoluteTime(0l,(Timings.getCurrentTime() - (getDay()-1)*(24*60*60*1000))));
//		// Time_of_start_of_this_week since last 7 days
//		System.out.println("Time_of_start_of_this_week1 = "+Timings.getAbsoluteTime(0l,(Timings.getCurrentTime() - (7*(24*60*60*1000)))));
//		System.out.println("Math ceil = "+Math.ceil(77.27272727272727));
//		System.out.println("Math ceil = "+Math.floor(77.27272727272727));
//		System.out.println("********************************");
//		long days_absolute_start_time = Timings.getAbsoluteTime(0, Timings.getCurrentTime());
//		long expiry_time = getTimeAfterDays(Timings.getAbsoluteTime(0, Timings.getCurrentTime()), 10);
//		System.out.println("No_of_days_left:- "+((expiry_time - days_absolute_start_time) / Timings.millisInADay + 1));
//		System.out.println("Absolute yesterday time:- "+(days_absolute_start_time - Timings.millisInADay));
//		System.out.println("************************************************************");
//		
//		String targetdate = "30.03.2016";
//		SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy"); // this format does not give the required answer
		
//		String targetdate = "12/04/2016";
//		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
//		
//		java.util.Date targetDate = dateFormat.parse(targetdate);
//		long targetTimeInMillis = targetDate.getTime();
//		System.out.println("targetTimeInMillis:- "+targetTimeInMillis);
//		System.out.println("days_absolute_start_time:- "+days_absolute_start_time);
//		System.out.println("Duration:- "+((targetTimeInMillis - days_absolute_start_time) / Timings.millisInADay + 1));
//		System.out.println("************************************************************");
//		//dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
//		//Date date = new Date(Timings.getCurrentTime());
//		Date date = new Date(1460399400000l);
//		System.out.println("Date from epoc time(UTC) = "+dateFormat.format(date).toString());
	}
}


