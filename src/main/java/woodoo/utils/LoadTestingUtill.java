package woodoo.utils;

import java.io.IOException;

import woodoo.network.HttpCaller;

public class LoadTestingUtill{
	
	public static String apiCaller( String url){
	final String apiUrl = url;
		
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				try{
					HttpCaller.get(apiUrl);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		thread.start();
		return null;
	}
	
	public static String multipleCaller(String api, int innerLoop, int outerLoop){
		for(int i = 0 ; i < outerLoop ; i++)
		{
			for(int j = 0 ; j < innerLoop ; j++){
				System.out.println("Calling api");
				apiCaller("http://www.google.com");
			}
		}
		return api;
	}
	public static void main(String[] args) {
		System.out.println("Calling start :: ");
		multipleCaller("api", 10, 10);
		System.out.println("Call finished !!!");
	}
}
