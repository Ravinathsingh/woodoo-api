package woodoo.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import woodoo.utils.preview.PreviewUtils;
import woodoo.utils.preview.SourceContent;
import woodoo.views.Coupons;
import woodoo.views.FlipkartView;
import woodoo.views.NewsView;
import woodoo.views.OffersView;


public class Constants {

	public static final String ACCOUNT_DEACTIVATION_MESSAGE="We regret to inform you that your WooDoo account has been deactivated. WooDoo usage and services are governed by its Terms & Conditions and Privacy Policy. Please write to: contact@woodoomobility.com for further help";
	public static final String ACCOUNT_DELETION_MESSAGE="We regret to inform you that your WooDoo account has been deleted. WooDoo usage and services are governed by its Terms & Conditions and Privacy Policy. Please write to: contact@woodoomobility.com for further help";
	
	public static final int MAX_RESULTS = 20;
	public static final int MAX_RESULTS_NOTIFICATION = 5;
	public static final int THREADS = 5;
	public static final String S3_BUCKET_URL="https://s3.ap-south-1.amazonaws.com/woodoo/";
	public static final String S3_BUCKET_URL_CODEYETI ="https://s3-us-west-1.amazonaws.com/test.codeyeti.in/";
	public static final String S3_BUCKET_NAME = "woodoo";
	public static final String S3_BUCKET_NAME_Codeyeti = "test.codeyeti.in";

	public static final String SMS_VERIFY_URL = "http://103.16.101.52:8080/bulksms/bulksms?username=ints-woodooevn&password=woo12344&type=0&dlr=1&destination={phone}&source=WOODOO&message={MESSAGE}";
	
	public static final String FCM_API_KEY = "AAAAX8xidr4:APA91bFlpfsLAskjuVJuJrITasr3z6lmZ-wHyW40yfYW2RvV8Apf-ylnA753Tjy6j1L67PEHu2RVpl20blfkNCQLbVHDFJ3oolSmcmwa1u6BKgNP-D8uHIA2WyzXe1MUiZPy5isdqENZ";
//	public static final String FCM_API_KEY = "AIzaSyBpvYG52zB4RYHcOn9w-Qaiqko7wM8I61Q";
	public static final Double referralAmount = 20.0;
	
	public static Long APICALLTIME = null;
	public static ArrayList<Coupons> coupons = null;
	public static ArrayList<OffersView> offers = null;
	public static ArrayList<OffersView> shoppingOffers = null;
	
	public static ArrayList<NewsView> espnNews = null;
	public static ArrayList<NewsView> ngNews = null;
	public static ArrayList<NewsView> toiNews = null;
	public static ArrayList<FlipkartView> flipkartData = null;
	
	public static final Long apiCallDuration = 60*60*1000l;	//one day
	public static final Long oneDooValue = 15*60*60*1000l;	//15 hours
	
	public static final String APPLICATION_NAME = "woodoo";
	public static final String 	TRACKINGURL = "http://tracking.vcommission.com/aff_c?";
	public static final String 	INSTRUCTIONS_URL_SHOPPING = "http://www.woodoo.co/Shopping.aspx";
	public static final String 	INSTRUCTIONS_URL_ACTION = "http://www.woodoo.co/ActionShopping.aspx";
	
/*
 * below are redemption apis:
 * id and key are provided by vcommission people
 * first of all we have to generate access token to call any api. Access token expires after some time, after that we have to generate new access token. 
 */
	
//	new after 
	public static final String id = "0048d3ccfe234c6c3abc5ea1dbb6fabd";//"7832652d5688154016ed250ef082039c";
	public static final String key = "337af93c2653a682041d1a403efb888d367233ac5aa632aa0a34ffbb156f3098";//"3f7f4f19f305dac19d47033eae9e3409789820932f70b617fbe88e77c828224f";
	
//	
////	test
//	public static final String id = "ceb4f599cd2852dcf89da2142b38ebba";//"7832652d5688154016ed250ef082039c";
//	public static final String key = "ba5574e9b44770be1016b8374de30d66ca7c26c1ed976cae75601cb941b7d575";//"3f7f4f19f305dac19d47033eae9e3409789820932f70b617fbe88e77c828224f";
//	
//	expeired live
//	public static final String id = "7832652d5688154016ed250ef082039c";
//	public static final String key = "3f7f4f19f305dac19d47033eae9e3409789820932f70b617fbe88e77c828224f";
	
	public static String gciURL="api.giftcardsindia.in";
	public static final String tokenUrl = "http://"+gciURL+"/access-token/get?id="+id+"&key="+key;
	public static String accessToken = null;
	public static Long expiresAt = null;
	public static  String hash = null;

	public static String brandDenomination = "http://api.giftcardsindia.in/brand/denominations?accessToken=";
	public static String brandStore="http://api.giftcardsindia.in/brand/stores?accessToken="+accessToken+"&hash=";
	public static String brandList = "http://api.giftcardsindia.in/channel/brands?accessToken=";
	
	public static String orderPlace = "http://"+gciURL+"/order/add?accessToken=";
	public static String couponGeneratorAPI = "http://api.giftcardsindia.in/order/get?receipt={receptNumber}&accessToken=";	

//APIS for screen 3
	public static String FIND_CAMPAIGN_API = "https://vcm.api.hasoffers.com/Apiv3/json?api_key=6611b47aa3fcaded85859c6fa4cab0038d7188f089149fd27bab5fededab38fa&Target=Affiliate_AdManager&Method=findAllCampaigns&filters[status]=active";
	public static String FIND_CREATIVE_API = "https://vcm.api.hasoffers.com/Apiv3/json?api_key=6611b47aa3fcaded85859c6fa4cab0038d7188f089149fd27bab5fededab38fa&Target=Affiliate_AdManager&Method=findAllCreatives&filters[status]=active&filters[ad_campaign_id]=";
	public static String OFFER_FILE_API = "https://vcm.api.hasoffers.com/Apiv3/json?api_key=6611b47aa3fcaded85859c6fa4cab0038d7188f089149fd27bab5fededab38fa&Target=Affiliate_OfferFile&Method=findById&id=";
	
//	ponits 
//	public static String vCommissionConversion = "https://api.hasoffers.com/Apiv3/json?NetworkId=vcm&Target=Affiliate_Report&Method=getConversions&api_key=6611b47aa3fcaded85859c6fa4cab0038d7188f089149fd27bab5fededab38fa&fields%5B%5D=Offer.name&fields%5B%5D=Stat.affiliate_info1&fields%5B%5D=Stat.datetime&fields%5B%5D=Stat.offer_id&fields%5B%5D=Stat.approved_payout&fields%5B%5D=Stat.conversion_status&fields%5B%5D=Stat.id&sort%5BStat.datetime%5D=asc";
	public static String vCommissionConversion = "https://vcm.api.hasoffers.com/Apiv3/json?api_key=6611b47aa3fcaded85859c6fa4cab0038d7188f089149fd27bab5fededab38fa&Target=Affiliate_Report&Method=getStats&fields[]=Stat.offer_id&fields[]=Stat.payout&fields[]=Stat.payout_type&fields[]=Stat.date&fields[]=Stat.conversions&fields[]=Offer.name&groups[]=Stat.offer_id&groups[]=Stat.date&groups[]=Stat.affiliate_info&sort[Stat.date]=desc&fields[]=Stat.affiliate_info2&fields[]=Stat.hour&filters[Stat.date][conditional]=GREATER_THAN_OR_EQUAL_TO&filters[Stat.date][values]=2018-01-01&page=";

	public static String payoutApi = "https://vcm.api.hasoffers.com/Apiv3/json?api_key=6611b47aa3fcaded85859c6fa4cab0038d7188f089149fd27bab5fededab38fa&Target=Affiliate_Offer&Method=getPayoutDetails&offer_id=";
	
	public static String vCommissionAction = "https://api.hasoffers.com/Apiv3/json?NetworkId=vcm&Target=Affiliate_Offer&Method=findMyApprovedOffers&api_key=6611b47aa3fcaded85859c6fa4cab0038d7188f089149fd27bab5fededab38fa&filters[name][NOT_LIKE]=%CPS%&limit=30&filters[payout_type][LIKE]=%CPA%";
	public static String vCommissionShopping = "https://api.hasoffers.com/Apiv3/json?NetworkId=vcm&Target=Affiliate_Offer&Method=findAll&limit=50&api_key=6611b47aa3fcaded85859c6fa4cab0038d7188f089149fd27bab5fededab38fa&filters[name][LIKE]=%CPS%20-%20India%";

//doo list
//this is live offers
//	public static String vCommissionAction = "https://api.hasoffers.com/Apiv3/json?NetworkId=vcm&Target=Affiliate_Offer&Method=findMyApprovedOffers&api_key=6611b47aa3fcaded85859c6fa4cab0038d7188f089149fd27bab5fededab38fa";
	
	public static String vCommissionFlipkartApi = "https://api.hasoffers.com/Apiv3/json?NetworkId=vcm&Target=Affiliate_OfferFile&Method=findAll&api_key=6611b47aa3fcaded85859c6fa4cab0038d7188f089149fd27bab5fededab38fa&filters%5Boffer_id%5D=412&filters%5Bwidth%5D=300&filters%5Bheight%5D=250"; 
	public static String vCommissionCoupons = "http://tools.vcommission.com/api/coupons.php?apikey=6611b47aa3fcaded85859c6fa4cab0038d7188f089149fd27bab5fededab38fa";

//	public static final String EMAIL = "codeyetilabs@gmail.com";
//	public static final String PASSWORD = "jinta@12345";
//	
	public static final String EMAIL = "contact@woodoomobility.com";
	//public static final String PASSWORD = "contact_WooDoo@108";
	public static final String PASSWORD = "Woo_contact_Doo";
	
	public static final Long myChoiceDuration = 90*24*60*60*1000l;
	public static final Long confirmedRedumptionDuration = 30*24*60*60*1000l;
	public static final double PI = 3.14159;
	
	public static final long GMT = (5*60*60*1000L+30*60*1000L);
	
	public static String generateRandomString(long size) {
		final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder builder = new StringBuilder();
		long count = size;
		
		while (count-- != 0) {
		int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
		builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}
	
	private static final Pattern urlPattern = Pattern.compile("(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)" + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*" + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
			Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

	public static long getCurrentTime() {
		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
		long time = cal.getTimeInMillis();
		return time;
	}

	public static long getNotificationSendingThresholdTime() {
		return getCurrentTime() - 24 * 3600 * 1000;
	}

	public static long getInactiveTime() {
		return getCurrentTime() - 7 * 24 * 3600 * 1000;
	}

	public static boolean matchKeyword(String post, String keywords) {
		post = post.replaceAll("[^a-zA-Z 0-9]", "").toUpperCase();
		String[] post_tokens = post.split(" ");
		String[] keywords_tokens = keywords.split(",");
		HashSet<String> set = new HashSet<String>();
		for (String str : post_tokens) {
			if (StringUtils.isNotBlank(str))
				set.add(StringUtils.trim(str));
		}
		for (String str : keywords_tokens) {
			if (set.contains(StringUtils.trim(str.toUpperCase())))
				return true;
		}
		return false;
	}

	public static String getFirstUrl(String str) {
		Matcher matcher = urlPattern.matcher(str);
		if (matcher.find()) {
			int matchStart = matcher.start(1);
			int matchEnd = matcher.end();
			// now you have the offsets of a URL match
			String link = str.substring(matchStart, matchEnd);
			if (!link.contains("http://") && !link.contains("https://"))
				link = "http://" + link;
			return link;
		}
		return null;
	}

	public static String getText(String text){
		JSONObject obj = new JSONObject(text);
		return obj.getString("text");
	}
	public static String getPreview(String text) {
		System.out.println("Getting preview of:" + text);
		JSONObject obj = new JSONObject(text);
		if (obj.has("image_url")) {
			obj.put("preview_url", obj.get("image_url"));
			obj.put("preview_pic_url", obj.get("image_url"));
		} else {
			SourceContent sourceContent = PreviewUtils.getPreview(getFirstUrl(obj.getString("text")));
			if (sourceContent != null && (StringUtils.isNotBlank(sourceContent.getHighestResolutionPic()) || (StringUtils.isNotBlank(sourceContent.getDescription())))) {
				obj.put("preview_title", sourceContent.getTitle());
				obj.put("preview_description", sourceContent.getDescription());
				obj.put("preview_url", sourceContent.getFinalUrl());
				obj.put("preview_pic_url", sourceContent.getHighestResolutionPic());
			} else {
				if (sourceContent == null)
					System.out.println("source content is null");
			}	
		}
		return obj.toString();
	}

	public static void main(String[] args) {
//		String str = getPreview("{\"text\":\"http://www.jabong.com/\",\"rating\":0,\"time\":0}");
		
//		String str1 = getPreview("{\"text\":\"http://www.codeyeti.com\",\"rating\":0,\"time\":0}");
		
//		JSONObject json = new JSONObject();
//		json.put("video", "Sumit");
//		json.put("link", "http://www.codeyeti.com/");
//		
//		if(json.has("link"))
//		{
//			System.out.println(json.get("link").toString());
//		}
//		
//		System.out.println(json.toString());
		
		String n = "100";
		getSum(n);
		System.out.println("new - "+n);
		
	}
	
	
	public static void getSum(String num)
	{
		num = num + 100;
		System.out.println(num);
	}
}
