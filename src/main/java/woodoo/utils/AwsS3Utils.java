package woodoo.utils;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.http.entity.ContentType;
import org.json.JSONObject;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;

import woodoo.model.Tuple;

public class AwsS3Utils 
{
	public static String upload(String keyName,InputStream file,String extension) throws IOException
	{
		//AWSCredentials awsCredentialsEmblaze = new BasicAWSCredentials("AKIAJEPRYRYPXKCGVLQA", "XDIcaf2jXIDIqt1aTjHyRUHLpOM8a88H2YRyOyam"); 
		ClientConfiguration clientConfiguration = new ClientConfiguration()
		        .withMaxErrorRetry(0) // 0 retries
		        .withConnectionTimeout(30000) // 30,000 ms
		        .withSocketTimeout(30000);
		AWSCredentials awsCredentials = new BasicAWSCredentials("AKIAJ5X3O6VZVSIERWPA", "SE6gq22+FSydJOAJod2fUOp/8xlGaDhQia4UJaar"); 
		AmazonS3 s3Client = new AmazonS3Client(awsCredentials,clientConfiguration); 
		ObjectMetadata metadata = new ObjectMetadata(); 
		metadata.setContentType("image/"+extension); 
    	PutObjectRequest request = new PutObjectRequest(Constants.S3_BUCKET_NAME, keyName,file, metadata); 
    	//request.withCannedAcl(CannedAccessControlList.PublicRead);
    
		PutObjectResult r = s3Client.putObject(request); 
		return Constants.S3_BUCKET_URL + keyName; 
	} 
	
	public static void main(String[] arhs) throws IOException {
	//	upload();
		String url = "https://rest.nexmo.com/sms/json?api_key=13374e44&api_secret=bfd903c4&from=CodeYeti&to=918879425129&text=service tax???";
	}

	public static HashMap<String,String> uploadFiles(MultipartHttpServletRequest multipartRequest) throws Exception
	{
		HashMap<String, String> file_map = new HashMap<String,String>();
		ExecutorService executor = Executors.newFixedThreadPool(Constants.THREADS);
		ArrayList<Callable<Tuple<String, String>>> callables = new ArrayList<Callable<Tuple<String,String>>>();
		Set set = multipartRequest.getFileMap().entrySet();
		Iterator i = set.iterator();
		
		while (i.hasNext()) 
		{
			Map.Entry me = (Map.Entry) i.next();
			String fileName = (String) me.getKey();
			System.out.println("filename " + fileName);
				
			MultipartFile file = (MultipartFile) me.getValue();
			String content_type = multipartRequest.getMultipartContentType(fileName);
			
			if (!file.isEmpty()) 
			{
				 Callable<Tuple<String, String>> callable = new ImageUploadThread(file,fileName,content_type);
				 callables.add(callable);
			}
		}
		List<Future<Tuple<String, String>>> future_list = executor.invokeAll(callables);
		executor.shutdown();
		
		for(Future<Tuple<String, String>> future : future_list)
			file_map.put(future.get().o1, future.get().o2);
		return file_map;
	}
	
	public static Tuple<String, String> uploadFile(MultipartFile file, String extension) throws Exception
	{ 
		try{
			
		if (file!=null && !file.isEmpty()) 
		{
	    	byte [] byteArr = file.getBytes(); 
	    	InputStream inputStream = new ByteArrayInputStream(byteArr);
	    	String pic_url = null;
	    	String filename = UUID.randomUUID().toString();
		    	if(extension!=null)
		    	{
		  //  		filename  = filename+"."+extension;
		    		pic_url = AwsS3Utils.upload(filename+"."+extension, inputStream,extension);
		    	}
		    	else
		    	{
		    		pic_url = AwsS3Utils.upload(filename+"."+getExtension(file.getContentType()), inputStream,getExtension(file.getContentType()));
		   // 		filename=filename;+"."+getExtension(file.getContentType());
		    	}
	    	return new Tuple<String, String>(filename,pic_url);
		}
		return null;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}
		
	public static String getExtension(String content_type){
		int i  = content_type.lastIndexOf("/", content_type.length());
		return content_type.substring(i+1, content_type.length());
	}
	

	
}
