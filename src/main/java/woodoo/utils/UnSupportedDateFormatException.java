package woodoo.utils;

public class UnSupportedDateFormatException extends Exception
{
	public UnSupportedDateFormatException() {
		super("Date format used is not supported by us.");
	}

}
