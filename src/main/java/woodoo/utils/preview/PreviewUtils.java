package woodoo.utils.preview;

import java.awt.Image;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import woodoo.model.Tuple;


public class PreviewUtils {

	public static final int ALL = -1;
	public static final int NONE = -2;

	private final static String HTTP_PROTOCOL = "http://";
	private final static String HTTPS_PROTOCOL = "https://";

	public PreviewUtils() {
	}

	public static SourceContent getPreview(String url) {
		SourceContent sourceContent = new SourceContent();
		if (url == null)
			return null;
		// int imageQuantity = 0;
		sourceContent.setFinalUrl(url);
	//	url = URLEncoder.encode(url);
		if (!sourceContent.getFinalUrl().equals("")) {
			if (isImage(sourceContent.getFinalUrl()) && !sourceContent.getFinalUrl().contains("dropbox")) {
				sourceContent.setSuccess(true);

				sourceContent.getImages().add(sourceContent.getFinalUrl());

				sourceContent.setTitle("");
				sourceContent.setDescription("");

			} else {
				try {
					Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21").get();
					System.out.println("Fetched html doc");
					sourceContent.setHtmlCode(extendedTrim(doc.toString()));

					HashMap<String, String> metaTags = getMetaTags(sourceContent.getHtmlCode());

					sourceContent.setMetaTags(metaTags);

					sourceContent.setTitle(metaTags.get("title"));
					sourceContent.setDescription(metaTags.get("description"));

					if (sourceContent.getTitle().equals("")) {
						String matchTitle = Regex.pregMatch(sourceContent.getHtmlCode(), Regex.TITLE_PATTERN, 2);

						if (!matchTitle.equals(""))
							sourceContent.setTitle(htmlDecode(matchTitle));
					}

					if (sourceContent.getDescription().equals(""))
						sourceContent.setDescription(crawlCode(sourceContent.getHtmlCode()));

					sourceContent.setDescription(sourceContent.getDescription().replaceAll(Regex.SCRIPT_PATTERN, ""));

					if (!metaTags.get("image").equals(""))
						sourceContent.getImages().add(metaTags.get("image"));
					else {
						sourceContent.setImages(getImages(sourceContent.getHtmlCode(), sourceContent.getFinalUrl()));
					}

					sourceContent.setSuccess(true);
				} catch (Exception e) {
					e.printStackTrace();
					sourceContent.setSuccess(false);
				}
			}
		}

		String[] finalLinkSet = sourceContent.getFinalUrl().split("&");
		sourceContent.setUrl(finalLinkSet[0]);

		sourceContent.setCannonicalUrl(cannonicalPage(sourceContent.getFinalUrl()));
		sourceContent.setDescription(stripTags(sourceContent.getDescription()));
		sourceContent.setHighestResolutionPic(getHighestResolutionPic(sourceContent));
		System.out.println(sourceContent.getHighestResolutionPic());
		System.out.println(sourceContent.getFinalUrl());
		return sourceContent;
		// return null;
	}

	/** Verifies if the content could not be retrieved */
	public boolean isNull(SourceContent sourceContent) {
		if (!sourceContent.isSuccess() && extendedTrim(sourceContent.getHtmlCode()).equals("") && !isImage(sourceContent.getFinalUrl()))
			return true;
		return false;
	}

	/** Gets content from a html tag */
	private static String getTagContent(String tag, String content) {

		String pattern = "<" + tag + "(.*?)>(.*?)</" + tag + ">";
		String result = "", currentMatch = "";

		List<String> matches = Regex.pregMatchAll(content, pattern, 2);

		for (int i = 0; i < matches.size(); i++) {
			currentMatch = stripTags(matches.get(i));
			if (currentMatch.length() >= 120) {
				result = extendedTrim(currentMatch);
				break;
			}
		}

		if (result.equals("")) {
			String matchFinal = Regex.pregMatch(content, pattern, 2);
			result = extendedTrim(matchFinal);
		}

		result = result.replaceAll("&nbsp;", "");

		return htmlDecode(result);
	}

	/** Gets images from the html code */
	public static List<String> getImages(String content, String url) {

		List<String> firstMatches = Regex.pregMatchAllImages(content, Regex.IMAGE_TAG_PATTERN);

		int pathCounter = 0;
		String src = "", imageSrc = "", currentImage = "";

		List<String> matches = new ArrayList<String>();
		for (String string : firstMatches) {
			if (!string.contains(">") && !string.contains("<") && !string.contains("'") && !string.contains("\"") && !string.contains("{") && !string.contains("}") && !string.contains("[")
					&& !string.contains("]"))
				matches.add(string);
		}

		for (int i = 0; i < matches.size(); i++) {

			currentImage = matches.get(i);

			if (!currentImage.startsWith("http://") && !currentImage.startsWith("https://")) {

				pathCounter = substringCount(currentImage, "\\.\\./");

				imageSrc = cannonicalImgSrc(currentImage);

				src = getImageUrl(pathCounter, cannonicalLink(imageSrc, url));

				if (!(src + imageSrc).equals(url)) {
					if (src.equals(""))
						currentImage = src + imageSrc;
					else
						currentImage = src;
				}

				matches.set(i, currentImage);
			}

		}

		return matches;
	}

	/** Counts substring occurences */
	private static int substringCount(String haystack, String needle) {
		return haystack.split(needle).length - 1;
	}

	/** Returns the original image url */
	private static String getImageUrl(int pathCounter, String url) {
		String src = "";
		if (pathCounter > 0) {
			String[] urlBreaker = url.split("/");
			for (int j = 0; j < pathCounter + 1; j++) {
				src += urlBreaker[j] + "/";
			}
		} else {
			src = url;
		}
		return src;
	}

	/** Returns the cannoncial image link */
	private static String cannonicalLink(String imagePath, String referer) {

		if (imagePath.startsWith("//"))
			imagePath = "http:" + imagePath;
		else if (imagePath.startsWith("/"))
			imagePath = "http://" + cannonicalPage(referer) + imagePath;
		else
			imagePath = referer + "/" + imagePath;

		return imagePath;
	}

	/** Returns the cannoncial image url */
	private static String cannonicalImgSrc(String imagePath) {
		imagePath = imagePath.replaceAll("\\.\\./", "");
		imagePath = imagePath.replaceAll("\\./", "");
		imagePath = imagePath.replaceAll(" ", "%20");
		return imagePath;
	}

	/** Transforms from html to normal string */
	private static String htmlDecode(String content) {
		return Jsoup.parse(content).text();
	}

	/** Crawls the code looking for relevant information */
	private static String crawlCode(String content) {
		String result = "";
		String resultSpan = "";
		String resultParagraph = "";
		String resultDiv = "";

		resultSpan = getTagContent("span", content);
		resultParagraph = getTagContent("p", content);
		resultDiv = getTagContent("div", content);

		result = resultSpan;

		if (resultParagraph.length() > resultSpan.length() && resultParagraph.length() >= resultDiv.length())
			result = resultParagraph;
		else if (resultParagraph.length() > resultSpan.length() && resultParagraph.length() < resultDiv.length())
			result = resultDiv;
		else
			result = resultParagraph;

		return htmlDecode(result);
	}

	/** Returns the cannoncial url */
	private static String cannonicalPage(String url) {

		String cannonical = "";
		if (url.startsWith(HTTP_PROTOCOL)) {
			url = url.substring(HTTP_PROTOCOL.length());
		} else if (url.startsWith(HTTPS_PROTOCOL)) {
			url = url.substring(HTTPS_PROTOCOL.length());
		}

		for (int i = 0; i < url.length(); i++) {
			if (url.charAt(i) != '/')
				cannonical += url.charAt(i);
			else
				break;
		}

		return cannonical;

	}

	/** Strips the tags from an element */
	private static String stripTags(String content) {
		return Jsoup.parse(content).text();
	}

	/** Verifies if the url is an image */
	private static boolean isImage(String url) {
		if (url.matches(Regex.IMAGE_PATTERN))
			return true;
		else
			return false;
	}

	/**
	 * Returns meta tags from html code
	 */
	private static HashMap<String, String> getMetaTags(String content) {

		HashMap<String, String> metaTags = new HashMap<String, String>();
		metaTags.put("url", "");
		metaTags.put("title", "");
		metaTags.put("description", "");
		metaTags.put("image", "");

		List<String> matches = Regex.pregMatchAll(content, Regex.METATAG_PATTERN, 1);

		for (String match : matches) {
			if (match.toLowerCase().contains("property=\"og:url\"") || match.toLowerCase().contains("property='og:url'") || match.toLowerCase().contains("name=\"url\"")
					|| match.toLowerCase().contains("name='url'"))
				metaTags.put("url", separeMetaTagsContent(match));
			else if (match.toLowerCase().contains("property=\"og:title\"") || match.toLowerCase().contains("property='og:title'") || match.toLowerCase().contains("name=\"title\"")
					|| match.toLowerCase().contains("name='title'"))
				metaTags.put("title", separeMetaTagsContent(match));
			else if (match.toLowerCase().contains("property=\"og:description\"") || match.toLowerCase().contains("property='og:description'") || match.toLowerCase().contains("name=\"description\"")
					|| match.toLowerCase().contains("name='description'"))
				metaTags.put("description", separeMetaTagsContent(match));
			else if (match.toLowerCase().contains("property=\"og:image\"") || match.toLowerCase().contains("property='og:image'") || match.toLowerCase().contains("name=\"image\"")
					|| match.toLowerCase().contains("name='image'"))
				metaTags.put("image", separeMetaTagsContent(match));
		}

		return metaTags;
	}

	/** Gets content from metatag */
	private static String separeMetaTagsContent(String content) {
		String result = Regex.pregMatch(content, Regex.METATAG_CONTENT_PATTERN, 1);
		return htmlDecode(result);
	}

	/**
	 * Unshortens a short url
	 */
	private String unshortenUrl(String shortURL) {
		if (!shortURL.startsWith(HTTP_PROTOCOL) && !shortURL.startsWith(HTTPS_PROTOCOL))
			return "";

		URLConnection urlConn = connectURL(shortURL);
		urlConn.getHeaderFields();

		String finalResult = urlConn.getURL().toString();

		urlConn = connectURL(finalResult);
		urlConn.getHeaderFields();

		shortURL = urlConn.getURL().toString();

		while (!shortURL.equals(finalResult)) {
			finalResult = unshortenUrl(finalResult);
		}

		return finalResult;
	}

	/**
	 * Takes a valid url and return a URL object representing the url address.
	 */
	private URLConnection connectURL(String strURL) {
		URLConnection conn = null;
		try {
			URL inputURL = new URL(strURL);
			conn = inputURL.openConnection();
		} catch (MalformedURLException e) {
			System.out.println("Please input a valid URL");
		} catch (IOException ioe) {
			System.out.println("Can not connect to the URL");
		}
		return conn;
	}

	/** Removes extra spaces and trim the string */
	public static String extendedTrim(String content) {
		return content.replaceAll("\\s+", " ").replace("\n", " ").replace("\r", " ").trim();
	}

	public static String getHighestResolutionPic(SourceContent content) {
		int resolution = 0;
		String highestResPic = null;
		if(content.getImages().size() ==0 )
			return null;
		ExecutorService executor = Executors.newFixedThreadPool(content.getImages().size());
		ArrayList<Worker> workers = new ArrayList<>();
		for (int i = 0; i < content.getImages().size(); i++) {
//			try {
//				URL url1 = new URL(content.getImages().get(i));
//				Image image;
//				image = ImageIO.read(url1);
//				int res_ = image.getHeight(null) * image.getWidth(null);
//				if (res_ > resolution) {
//					resolution = res_;
//					highestResPic = content.getImages().get(i);
//				}
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
			Worker worker = new Worker(content.getImages().get(i));
			workers.add(worker);

		}
		try{
			int highestRes = 0;
			List<Future<Tuple<String, Integer>>> futures = executor.invokeAll(workers);
			for (Future<Tuple<String, Integer>> future : futures) {
				Tuple<String,Integer> t = future.get();
				if(t.o2 > highestRes){
					highestRes = t.o2;
					highestResPic = t.o1;
				}
			}
			executor.shutdown();
			executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
			
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return highestResPic;
	}

	static class Worker implements Callable<Tuple<String,Integer>>{

		String url;

		public Worker(String url) {
			this.url = url;
		}

		@Override
		public Tuple<String,Integer> call() {
			try {
				URL url1 = new URL(url);
				final HttpURLConnection connection = (HttpURLConnection) url1.openConnection();
				connection.setRequestProperty(
				    "User-Agent",
				    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31");

				System.out.println(connection.getContentType());
				Image image = ImageIO.read(connection.getInputStream());
				int res= image.getHeight(null) * image.getWidth(null);
				Tuple<String,Integer> t= new Tuple();
				t.o1 = url;
				t.o2 = res;
				return t;		
			} catch (Exception ex) {
				Tuple<String,Integer> t= new Tuple();
				t.o1 = url;
				t.o2 = 0;
				return t;
			}
		}
	}

	public static void main(String[] args) throws IOException {
//		String url = "http://r2d.wikia.com/wiki/File:Sad_face_meme_i20-s287x310-315187.jpg";
		String url = "https://drive.google.com/drive/folders/0B4xDuLyJC6acREtJMmhRY0dkcmM/";

//		SourceContent content = getPreview(url);
		HashMap<String, String> content = getMetaTags(url);
		
		System.out.println("URL "+content.get("url"));
		System.out.println("title "+content.get("title"));
		System.out.println("description "+content.get("description"));
		System.out.println("image "+content.get("image"));
		
//		metaTags.put("url", "");
//		metaTags.put("title", "");
//		metaTags.put("description", "");
//		metaTags.put("image", "");
		
//		System.out.println(content.getHighestResolutionPic());

	}

}
