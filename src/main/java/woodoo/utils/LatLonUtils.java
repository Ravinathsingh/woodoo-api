package woodoo.utils;


import java.util.ArrayList;

import woodoo.model.Coordinate;
import woodoo.model.Tuple;


public class LatLonUtils {
	public static final double PI_RADIANS = 3.14159 / 180;
	public static final double EARTH_RADIUS = 6371 * 1000;

	private static double sin(double x) {
		return Math.sin(x);
	}

	private static double cos(double x) {
		return Math.cos(x);
	}

	public static double bearing(double lat1, double lon1, double lat2, double lon2) {
		lat1 = lat1 * PI_RADIANS;
		lon1 = lon1 * PI_RADIANS;
		lat2 = lat2 * PI_RADIANS;
		lon2 = lon2 * PI_RADIANS;
		double delta_lon = (lon2 - lon1);
		double bearing = Math.atan2(sin(delta_lon) * cos(lat2), (cos(lat1) * sin(lat2)) - (sin(lat1) * cos(lat2) * cos(delta_lon)));
		return bearing;
	}

	public static double straightDistanceInMeters(double lat1, double lon1, double lat2, double lon2) {
		lat1 = lat1 * PI_RADIANS;
		lon1 = lon1 * PI_RADIANS;
		lat2 = lat2 * PI_RADIANS;
		lon2 = lon2 * PI_RADIANS;
		if (lat1 == lat2 && lon1 == lon2)
			return 0;
		double distance = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2)) * 6371 * 1000;
		return distance;
	}

	public static double straightDistanceInKilometers(double lat1, double lon1, double lat2, double lon2) {

		double distance = straightDistanceInMeters(lat1, lon1, lat2, lon2);
		return distance / 1000;
	}

	public static Coordinate pointAtDistance(double lat, double lon, double bearing, double distance) {
		lat = lat * PI_RADIANS;
		lon = lon * PI_RADIANS;
		double angular_distance = distance / EARTH_RADIUS;

		double lat_ = Math.asin(sin(lat) * cos(angular_distance) + cos(lat) * sin(angular_distance) * cos(bearing));
		double lon_ = lon + Math.atan2(sin(bearing) * sin(angular_distance) * cos(lat), cos(angular_distance) - sin(lat) * sin(lat_));
		return new Coordinate(lat_ / PI_RADIANS, lon_ / PI_RADIANS);
	}

//	public static Tuple<Integer, Integer> intersection(ArrayList<Coordinate> list1, ArrayList<Coordinate> list2, boolean fromStart, boolean interCity) {
//		Tuple<Integer, Integer> tuple;
//		if (fromStart) {
//			for (int i = 0; i < list1.size(); i++) {
//				Coordinate c1 = list1.get(i);
//				Tuple<Integer, Double> nearestPoint = nearestPointDistance(list2, c1);
//				if (interCity && nearestPoint.o2 <= Constants.SAMPLING_DISTANCE_INTER_CITY) {
//					tuple = new Tuple<Integer, Integer>();
//					tuple.o1 = i;
//					tuple.o2 = nearestPoint.o1;
//					return tuple;
//				}
//				if (!interCity && nearestPoint.o2 <= Constants.SAMPLING_DISTANCE_INTRA_CITY) {
//					tuple = new Tuple<Integer, Integer>();
//					tuple.o1 = i;
//					tuple.o2 = nearestPoint.o1;
//
//					return tuple;
//				}
//
//			}
//			return null;
//		} else {
//			for (int i = list1.size() - 1; i >= 0; i--) {
//				Coordinate c1 = list1.get(i);
//				Tuple<Integer, Double> nearestPoint = nearestPointDistance(list2, c1);
//				if (interCity && nearestPoint.o2 <= Constants.SAMPLING_DISTANCE_INTER_CITY) {
//					tuple = new Tuple<Integer, Integer>();
//					tuple.o1 = i;
//					tuple.o2 = nearestPoint.o1;
//					return tuple;
//				}
//				if (!interCity && nearestPoint.o2 <= Constants.SAMPLING_DISTANCE_INTRA_CITY) {
//					tuple = new Tuple<Integer, Integer>();
//					tuple.o1 = i;
//					tuple.o2 = nearestPoint.o1;
//
//					return tuple;
//				}
//
//			}
//			return null;
//		}
//	}

	public static ArrayList<Coordinate> pointsBetween(double lat1, double lon1, double lat2, double lon2, double sampling_distance) {
		double bearing = bearing(lat1, lon1, lat2, lon2);
		double straight_distance = straightDistanceInMeters(lat1, lon1, lat2, lon2);
		ArrayList<Coordinate> list = new ArrayList<Coordinate>();
		Coordinate start = new Coordinate(lat1, lon1);
		// list.add(start);
		if (straight_distance < sampling_distance) {
			// list.add(new Coordinate(lat2, lon2));
			return list;
		}
		int k = (int) (straight_distance / sampling_distance);
		Coordinate s = start;
		for (int i = 1; i <= k; i++) {
			double distance = sampling_distance;
			Coordinate c = pointAtDistance(s.getLat(), s.getLon(), bearing, distance);
			list.add(c);
			s = c;
		}
		Coordinate end = new Coordinate(lat2, lon2);
		// list.add(end);
		return list;
	}

	public static ArrayList<Coordinate> pointsBetween(ArrayList<Coordinate> polyline, double sampling_distance) {
		if (polyline.size() <= 1)
			return polyline;
		ArrayList<Coordinate> points = new ArrayList<Coordinate>();
		points.add(polyline.get(0));
		for (int i = 1; i < polyline.size(); i++) {
			Coordinate start = polyline.get(i - 1);
			Coordinate end = polyline.get(i);
			ArrayList<Coordinate> points_ = pointsBetween(start.getLat(), start.getLon(), end.getLat(), end.getLon(), sampling_distance);
			for (Coordinate point : points_)
				points.add(point);
			points.add(end);
		}
		return points;
	}

	public static double getDistanceInMeters(ArrayList<Coordinate> route) {
		double distance = 0;
		if (route.size() < 2)
			return distance;
		Coordinate last = route.get(0);
		for (int i = 1; i < route.size(); i++) {
			Coordinate c = route.get(i);
			distance += straightDistanceInMeters(last.getLat(), last.getLon(), c.getLat(), c.getLon());
			last = c;
		}
		return distance;
	}

	public static double getDistanceInKilometers(ArrayList<Coordinate> route) {
		double distance = getDistanceInMeters(route);
		return distance / 1000;
	}

	public static Tuple<Integer, Double> nearestPointDistance(ArrayList<Coordinate> polyline, Coordinate coordinate) {
		Coordinate c = null;
		double distance = Double.MAX_VALUE;
		int index = -1;
		for (int i = 0; i < polyline.size(); i++) {
			Coordinate c_ = polyline.get(i);
			double distance_ = straightDistanceInMeters(c_.getLat(), c_.getLon(), coordinate.getLat(), coordinate.getLon());
			if (distance_ < distance) {
				c = c_;
				distance = distance_;
				index = i;
			}
		}
		Tuple t = new Tuple<Integer, Double>();
		t.o1 = index;
		t.o2 = distance;
		return t;
	}

	public static Tuple<Integer, Coordinate> nearestPoint(ArrayList<Coordinate> polyline, Coordinate coordinate) {
		Coordinate c = null;
		double distance = Double.MAX_VALUE;
		int index = -1;
		for (int i = 0; i < polyline.size(); i++) {
			Coordinate c_ = polyline.get(i);
			double distance_ = straightDistanceInMeters(c_.getLat(), c_.getLon(), coordinate.getLat(), coordinate.getLon());
			if (distance_ < distance) {
				c = c_;
				distance = distance_;
				index = i;
			}
		}
		Tuple t = new Tuple<Integer, Coordinate>();
		t.o1 = index;
		t.o2 = c;
		return t;
	}

	public static double distanceInMeters(ArrayList<Coordinate> polyline, int from, int to) {
		if (from == to)
			return 0d;
		double distance = 0;
		for (int i = from + 1; i <= to; i++) {
			Coordinate c1 = polyline.get(i - 1);
			Coordinate c2 = polyline.get(i);
			distance += straightDistanceInMeters(c1.getLat(), c1.getLon(), c2.getLat(), c2.getLon());
		}
		return distance;
	}

	public static double distanceInKilometers(ArrayList<Coordinate> polyline, int from, int to) {

		return distanceInMeters(polyline, from, to) / 1000;
	}

	// public static ArrayList<Coordinate> pointsBetween(String polyline,double
	// sampling_distance){
	//
	// if(polyline.size() <= 1)
	// return polyline;
	// ArrayList<Coordinate> points = new ArrayList<Coordinate>();
	// points.add(polyline.get(0));
	// for(int i = 1; i<polyline.size(); i++){
	// Coordinate start = polyline.get(i-1);
	// Coordinate end = polyline.get(i);
	// ArrayList<Coordinate> points_ = pointsBetween(start.getLat(),
	// start.getLon(), end.getLat(), end.getLon(), sampling_distance);
	// for(Coordinate point : points_)
	// points.add(point);
	// points.add(end);
	// }
	// return points;
	// }

	public static void main(String[] args) throws Exception {
		Coordinate c1 = new Coordinate(19.062992, 73.013923);
		Coordinate c2 = new Coordinate(19.062729, 73.005683);
		Coordinate c3 = new Coordinate(19.059382, 73.002958);
		ArrayList<Coordinate> polyline = new ArrayList<Coordinate>();
		polyline.add(c1);
		polyline.add(c2);
		polyline.add(c3);
		ArrayList<Coordinate> c = pointsBetween(polyline, 100);
		for (Coordinate coordinate : c) {
			System.out.println(coordinate.getLat() + "," + coordinate.getLon());
		}
	}
}
