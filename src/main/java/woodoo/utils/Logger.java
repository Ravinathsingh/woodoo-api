package woodoo.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class Logger {
	
	public static void geturl()
	{
		  HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		  System.out.println(request.getRequestURL() + "?" + request.getQueryString());
//		  Iterator it = request.getParameterMap().entrySet().iterator();
//		    while (it.hasNext()) {
//		        Map.Entry pair = (Map.Entry)it.next();
//		        System.out.println(pair.getKey() + " = " + pair.getValue().toString());
//		        it.remove(); // avoids a ConcurrentModificationException
//		    }
		    
		  	  BufferedReader bufferedReader = null;
			  StringBuilder stringBuilder = new StringBuilder();
			  try 
			  {
			        InputStream inputStream = request.getInputStream();
			        if (inputStream != null)
			        {
			            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
			            char[] charBuffer = new char[128];
			            int bytesRead = -1;
			            while ((bytesRead = bufferedReader.read(charBuffer)) > 0)
			            {
			                stringBuilder.append(charBuffer, 0, bytesRead);
			            }
			        } 
			        else
			        {
			            stringBuilder.append("");
			        }
			  } 
			  catch (IOException ex) 
			  {
			        ex.printStackTrace();
			  } 
			  finally 
			  {
				  if (bufferedReader != null)
				  {
					  try 
					  {
			                bufferedReader.close();
			          }
					  catch (IOException ex) 
					  {
			                ex.printStackTrace();
			          }
			       }
			   }

			   System.out.println(stringBuilder.toString());
			   request.getAttributeNames();
		}

	}
		  
		  
		 


