package woodoo.utils.test;

import java.util.Arrays;
import java.util.Scanner;

import com.mysql.fabric.xmlrpc.base.Array;

class JDoIts {
	private static Integer BS(Integer[] sortedArray, Integer element)
	{
		int l = 0;
		int r = sortedArray.length;
		
		while(l <= r)
		{
			int m = l + (r-l)/2; 
			if(sortedArray[m] == element)
				return m;
			if(sortedArray[m] > element)
				r = m;
			else
				l = m;
		}
		return -1;
	}
	
//	public static void main(String[] args) {
//		Integer[] arr = {27,45,87,23,14,90,129,52,429};
//		Arrays.sort(arr);
//		System.out.println(BS(arr, 52));
//	}
	
}


public class JDoIt {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double mealCost = scan.nextDouble(); // original meal price
        int tipPercent = scan.nextInt(); // tip percentage
        int taxPercent = scan.nextInt(); // tax percentage
        scan.close();
      
        // Write your calculation code here.
        double tipper1 = (double)tipPercent;
        double per = (tipper1 / 100);
        
        double tipper2 = (double)taxPercent;
        double per2 = (tipper2/100);
        
        double tip = (mealCost * per);
        System.out.println(tip);
        
        double tax = (mealCost * per2);
        System.out.println(tax);
        
        double Cost = mealCost + tip + tax;
        System.out.println(Cost);
        // cast the result of the rounding operation to an int and save it as totalCost 
        int totalCost = (int) Math.round(Cost);
      
        // Print your result
        System.out.println(totalCost);
    }
}