package woodoo.enums;

public enum WooStatus {
	PENDING, CONFIRMED, INSTANT, MYCHOICE
}
