package woodoo.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.TimeZone;

import org.apache.log4j.chainsaw.Main;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;

import woodoo.configuration.AppConfig;
import woodoo.dao.DaoFactory;
import woodoo.model.Conversion;
import woodoo.model.Lsimpression;
import woodoo.model.NotificationW;
import woodoo.model.Notifications;
import woodoo.model.Order;
import woodoo.model.User;
import woodoo.model.WooLog;
import woodoo.network.CouponCaller;
import woodoo.network.EmailSender;
import woodoo.network.HttpCaller;
import woodoo.network.NewsApiCaller;
import woodoo.network.OrderFailException;
import woodoo.network.OtpEmailSender;
import woodoo.network.notification.AndroidNotifications;
import woodoo.network.notification.sender.Content;
import woodoo.network.notification.sender.GCMHandler;
import woodoo.network.notification.sender.PostGCM;
import woodoo.network.redemption.RedemptionList;
import woodoo.utils.Constants;
import woodoo.utils.JSONUtils;
import woodoo.utils.Timings;
import woodoo.views.BrandDenominations;
import woodoo.views.BrandOutput;
import woodoo.views.FlipkartOutput;
import woodoo.model.RedemptionBrand;

@Service
public class RedemptionService extends AbstractService{
	
	@Autowired
	RedemptionList brandList;
	
	@Autowired
	DooService dooService;
	
	@Autowired
	public RedemptionService(DaoFactory dao, AppConfig config) {
		super(dao, config);
	}
	
	public String fillBrandListInDataBase(Session session) throws Exception
	{
		System.out.println("Scheduler started..");
		
		System.out.println(dao.truncate(session, RedemptionBrand.class)+ " rows deleted.");
		
		ArrayList<RedemptionBrand> list = new ArrayList<>();
		list.addAll(brandList.getRedemptionBrandsList());
		
		for(RedemptionBrand brand : list)
		{
			try {
			ArrayList<BrandDenominations> denominations = new ArrayList<>();
			denominations.addAll(brandList.getBrandDenominationList(brand.getHash()));
			brand.setDenominationsAll(gson.toJson(denominations));
			
//			if(brand.getIsActive().equalsIgnoreCase("true"))
//			{
				brand.setOurStatus(true);
//			}
//			else
//			{
//				brand.setOurStatus(false);
//			}
			
			BrandOutput deno = new BrandOutput();
			for(BrandDenominations denom : denominations)
			{
				
				if(denom.getValueType().equalsIgnoreCase("fixed"))
				{
					if(deno.getId() == null)
					{
						deno.setDenomination_value(denom.getName());
						deno.setId(denom.getId());
						deno.setType(denom.getType());
						deno.setSkuId(denom.getSkuId());
					}
					else
					{
						if(deno.getDenomination_value() > denom.getName())
						{
							deno.setDenomination_value(denom.getName());
							deno.setId(denom.getId());
							deno.setType(denom.getType());
							deno.setSkuId(denom.getSkuId());
						}
					}
				}
				else
				{
					if(deno.getId() == null)
					{
						deno.setDenomination_value(denom.getName());
						deno.setId(denom.getId());
						deno.setType(denom.getType());
						deno.setSkuId(denom.getSkuId());
					}
					else
					{
						if(deno.getDenomination_value() > denom.getName())
						{
							deno.setDenomination_value(denom.getName());
							deno.setId(denom.getId());
							deno.setType(denom.getType());
							deno.setSkuId(denom.getSkuId());
						}
					}
				}
			}
			brand.setAmount(deno.getDenomination_value());
			brand.setTerms("");
			brand.setDescription("");
			brand.setSkuId(deno.getSkuId());
			
			brand.setDenominations(gson.toJson(deno));
//			System.out.println("Inserting Brand : "+gson.toJson(brand));
			dao.insert(session, brand);
			session.clear();
			}catch (Exception e) {
				// TODO: handle exception
			}
		}
		return "successfully inserted "+brandList;
	}
	
	public String getBrandList(Long my_user_id) throws Exception {
		Session session = getSession();
		
		try	
		{
//			System.out.println(fillBrandListInDataBase(session));
			
			ArrayList<RedemptionBrand> brands = dao.getWooDoodao().getallBrands(session, RedemptionBrand.class);
			
			Collections.sort(brands, RedemptionBrand.sortByAmount);
			System.out.println("Response : "+JSONUtils.getSuccessJson(brands));
			return JSONUtils.getSuccessJson(brands);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String placeOrder(String brandIds, Long myChoiceMoneyValue, Long my_user_id) throws Exception 
	{
		Session session = getSession();
		try	
		{
			User user = (User) dao.getbyId(session, my_user_id, User.class);
			String[] ids = brandIds.split(",");
			ArrayList<RedemptionBrand> brands = new ArrayList<>();
			Long totalValueOfOrder = 0l;
			
			ArrayList<WooLog> woos = dao.getWooDoodao().getUnusedWoos(session, my_user_id);
			ArrayList<Conversion> doos = dao.getWooDoodao().getUnusedDoos(session, my_user_id);
			
			for(String id : ids)
			{
//				System.out.println("id "+id);
				RedemptionBrand brand = (RedemptionBrand) dao.getbyId(session, Long.parseLong(id), RedemptionBrand.class);
//				System.out.println("brand  "+brand);
				totalValueOfOrder = totalValueOfOrder + brand.getAmount();
				brands.add(brand);
			}
			
			Map<String, String> map = dooService.woodoodata(my_user_id, session, user);
			
			Double s = Double.parseDouble(map.get("available_for_my_choice_redemption"));
//			System.out.println("Mychoice value : "+s);
			myChoiceMoneyValue = s.longValue();
			
			Double s1 = Double.parseDouble(map.get("total_woo_cash"));
			
			if(totalValueOfOrder > myChoiceMoneyValue)
			{
				return JSONUtils.getFailJson("Redemption value is greater than redeemable.");
			}
			System.out.println("S1 = "+s1+"  mychoice : "+ myChoiceMoneyValue);
			if(s1 < myChoiceMoneyValue)
			{
				return JSONUtils.getFailJson("Redemption value is greater than redeemable.");
			}
             String temp="";
			for(RedemptionBrand brand : brands)
			{
				Order order = new Order();
				order.setAmount(""+brand.getAmount());
				order.setSkuId(brand.getSkuId());
				order.setUserId(my_user_id);
				order.setBrand_id(brand.getId());
				order.setImage(brand.getImage());
				
				temp=placeOrder(order);
			}
			if(temp.equalsIgnoreCase("success"))
			{
			updateUsedPoint(session, totalValueOfOrder,woos, doos);
			
			return JSONUtils.getSuccessJson("Coupon generated successfully");
			}else
			{
			  return JSONUtils.getFailJson("Coupon Creation failed. Please try after some time.");
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return JSONUtils.getFailJson("Coupon Creation failed. Please try after some time.");
//			throw new Exception();
		}
		finally 
		{
			session.close();
		}
	}
	
	public String placeOrder(Long myChoiceMoneyValue, Long my_user_id,String mob_no) throws Exception 
	{
		Session session = getSession();
		try	
		{
			User user = (User) dao.getbyId(session, my_user_id, User.class);
		//	String[] ids = brandIds.split(",");
		//	ArrayList<RedemptionBrand> brands = new ArrayList<>();
			Long totalValueOfOrder = myChoiceMoneyValue;
			
			ArrayList<WooLog> woos = dao.getWooDoodao().getUnusedWoos(session, my_user_id);
			ArrayList<Conversion> doos = dao.getWooDoodao().getUnusedDoos(session, my_user_id);
			
		/*	for(String id : ids)
			{
//				System.out.println("id "+id);
				RedemptionBrand brand = (RedemptionBrand) dao.getbyId(session, Long.parseLong(id), RedemptionBrand.class);
//				System.out.println("brand  "+brand);
				totalValueOfOrder = totalValueOfOrder + brand.getAmount();
				brands.add(brand);
			}*/
			
			Map<String, String> map = dooService.woodoodata(my_user_id, session, user);
			
			Double s = Double.parseDouble(map.get("available_for_my_choice_redemption"));
//			System.out.println("Mychoice value : "+s);
			myChoiceMoneyValue = s.longValue();
			
			Double s1 = Double.parseDouble(map.get("total_woo_cash"));
			
			if(totalValueOfOrder > myChoiceMoneyValue)
			{
				return JSONUtils.getFailJson("Redemption value is greater than redeemable.");
			}
			System.out.println("S1 = "+s1+"  mychoice : "+ myChoiceMoneyValue);
			if(s1 < myChoiceMoneyValue)
			{
				return JSONUtils.getFailJson("Redemption value is greater than redeemable.");
			}
			String orderId = null;
				orderId = "woodoo"+generateRandomString(8);
				Date date = new Date();
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				formatter.setTimeZone(TimeZone.getTimeZone("IST"));		 	  
				String strDate = formatter.format(date);
				Order order = new Order();
				order.setAmount(""+totalValueOfOrder);
				order.setSkuId("paytm");
				order.setUserId(my_user_id);
				order.setBillingName(user.getFirst_name());
				order.setBrand_id(0l);
				order.setBrand_name("PayTM");
				order.setClientOrderId(orderId);
				order.setType("PENDING");
				order.setOrderDate(strDate);
				order.setBillingMobile(mob_no);
				dao.insert(session, order);			
			    updateUsedPoint(session, totalValueOfOrder,woos, doos);
			
			return JSONUtils.getSuccessJson("Coupon generated successfully");
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return JSONUtils.getFailJson("Coupon Creation failed. Please try after some time.");
//			throw new Exception();
		}
		finally 
		{
			session.close();
		}
	}
	
	
	private void updateUsedPoint(Session session, Long pointToUse, ArrayList<WooLog> woos, ArrayList<Conversion> doos) 
	{
		Long dooNeeded = pointToUse;
		Long wooNeeded = pointToUse;
		System.out.println("pOINTS NDDED : "+pointToUse);
		
		for(WooLog log : woos)
		{
			if(wooNeeded > log.getLeft_points())
			{
				wooNeeded = wooNeeded - log.getLeft_points();
				log.setLeft_points(0l);
				System.out.println("Step 1 "+log.getLeft_points());
			}
			if(wooNeeded <= log.getLeft_points())
			{
				log.setLeft_points(log.getLeft_points()- wooNeeded);
				wooNeeded = wooNeeded - wooNeeded;
				System.out.println("Step 2 "+log.getLeft_points());
			}
			System.out.println("Updateed Woo : "+log);
			dao.update(session, log);
		}
		
		System.out.println("\n\n");
		for(Conversion log : doos)
		{
			if(dooNeeded > log.getLeft_points())
			{
				dooNeeded = dooNeeded - log.getLeft_points().longValue();
				log.setLeft_points(0.0);
			}
			if(dooNeeded <= log.getLeft_points())
			{
				log.setLeft_points(log.getLeft_points() - dooNeeded.doubleValue());
				dooNeeded = dooNeeded - dooNeeded;
			}
			System.out.println("Updated Doo : "+log);
			dao.update(session, log);
		}
	}

	public String placeOrder(Order order) throws Exception {
		Session session = getSession();
		try	
		{
//			if(order.getSkuId() != null && order.getUserId() != null)
//			{
//				Map<String, Object> map = new HashMap<>();
//				map.put("skuId", order.getSkuId());
//				map.put("userId", order.getUserId());
//				
//				Order oldorder = (Order) dao.getbyUniqueColumns(session, map, Order.class);
//				
//				if(oldorder != null)
//					return JSONUtils.getFailJson("You had already used this coupon.");
//			}
//			else
//			{
//				return JSONUtils.getFailJson("please send value of skuId");
//			}
			
			if(order.getUserId() == null || order.getSkuId() == null)
				return JSONUtils.getFailJson("userId or sklId is empty.");
			
			User user = (User) dao.getbyId(session, order.getUserId(), User.class);
			
			if(user == null)
				return JSONUtils.getFailJson("User not found");
			
			Map<String, String> result = dooService.woodoodata(order.getUserId(),session,user);
			
			JSONObject doodata = new JSONObject();
			doodata.put("pending_action_doo_cash", result.get("pending_action_doo_cash"));
			doodata.put("pending_shopping_doo_cash",result.get("pending_shopping_doo_cash"));
			doodata.put("total_pending_doo_cash", result.get("total_pending_doo_cash"));
			doodata.put("total_confirmed_doo_cash", result.get("total_confirmed_doo_cash"));
			doodata.put("available_for_instant_redemption", result.get("available_for_instant_redemption"));
			doodata.put("available_for_my_choice_redemption", result.get("available_for_my_choice_redemption"));
			
			JSONObject woodata = new JSONObject();
			
			woodata.put("lock_screen_woo_cash", result.get("lock_screen_woo_cash"));
			woodata.put("bonus_woo_cash", result.get("bonus_woo_cash"));
			woodata.put("total_woo_cash", result.get("total_woo_cash"));
			
			JSONObject totalEarned = new JSONObject();
			totalEarned.put("total_earned", result.get("total_confirmed"));
			totalEarned.put("total_encash", "NA");
			totalEarned.put("gap_from_value_back_target", ""+result.get("gap_from_value_back_target"));
			totalEarned.put("available_for_redemption", "NA");
			
			String receptNo;
			try {
				
				RedemptionBrand brand = (RedemptionBrand) dao.getbyId(session, order.getBrand_id(), RedemptionBrand.class);
				
				order.setBrand_name(brand.getName());
				order.setType(brand.getType());
				order.setWebsite(brand.getWebsite());
				
				String orderId = null;
				Order orderOld = null;
//				System.out.println("swdihwgduihewd");
				orderOld = (Order) dao.getbyUniqueColumn(session, "clientOrderId", orderId, Order.class);
				do{
					orderId = "woodoo"+generateRandomString(8);
//					System.out.println("swdihwgduihewd");
					orderOld = (Order) dao.getbyUniqueColumn(session, "clientOrderId", orderId, Order.class);
					
				}while(orderOld != null);
				order.setClientOrderId(orderId);
				receptNo = orderApiCaller(session, user, order,brand);
			} catch (OrderFailException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "error";
			} 
//			System.out.println("Recept No : "+receptNo);
			String couponApi = Constants.couponGeneratorAPI+Constants.accessToken;
			couponApi = couponApi.replace("{receptNumber}", receptNo);
			
			Map<String, String> map = new HashMap<>();
			JSONObject json = new JSONObject();
			Order orderOld = null;
			String couponCode = null;
			
			do{
				couponCode = "woodoo"+generateRandomString(4);
				orderOld = (Order) dao.getbyUniqueColumn(session, "coupon", couponCode, Order.class);
				
			}while(orderOld != null);
			
			 
	        //order.setCoupon(couponCode);
			json.put("code", couponCode);
			json.put("value",order.getAmount());
			
			JSONArray jsonArray = new JSONArray();
			jsonArray.put(json);
			map.put("accessToken", Constants.accessToken);
			map.put("receipt", receptNo);
			
			String couponCreationStatus = HttpCaller.post(couponApi, map);
			
//			System.out.println("Kuch bhii  "+couponCreationStatus);
			JSONArray couponR = new JSONArray(couponCreationStatus.toString());
			com.amazonaws.util.json.JSONObject couponResponse=(com.amazonaws.util.json.JSONObject) couponR.get(0);
			if(!couponResponse.isNull("voucherNumber"))
			{       Date date = new Date();
					SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					formatter.setTimeZone(TimeZone.getTimeZone("IST"));		 	  
					String strDate = formatter.format(date);
				    order.setCoupon(couponResponse.get("voucherNumber").toString());
				    order.setTaxes(couponResponse.get("voucherPin").toString());
				    order.setOrderDate(strDate);
					dao.insert(session, order);
					if(order.getSkuId().equalsIgnoreCase("bmb000100"))
					{
					 String message = "ACTGV<"+ order.getCoupon() +">";
					 OtpEmailSender.verifyPhone("9239895050", message);
					}
					return "success";
				
			}
		}
		finally {
			session.close();
		}
		return null;
	}

	private static String generateRandomString(long size) {
		final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder builder = new StringBuilder();
		long count = size;
		
		while (count-- != 0) {
		int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
		builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	private String orderApiCaller(Session session, User user, Order order,RedemptionBrand brand) throws IOException, JSONException, ParseException, OrderFailException {
		brandList.generateAccessToken();
		String url = Constants.orderPlace+Constants.accessToken;
		Map<String, String> headers = new HashMap<>();
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date(Timings.getCurrentTime());
		String currentDate = dateFormat.format(date);
		//[{"brandHash":"95b93fce0173a0d468b17d27db4e67dc","denomination":101,"productType":"digital","quantity":"1"}]
		/*JSONObject jos = new JSONObject(brand.getDenominations());
		  
		JSONObject jo = new JSONObject();
		jo.put("brandHash", brand.getHash());
		jo.put("denomination", jos.get("denomination_value"));
		jo.put("productType", jos.get("type"));
		jo.put("quantity", "1");
		JSONArray ja = new JSONArray();
		ja.put(jo);*/
		headers.put("skuId", order.getSkuId());
		//headers.put("skuId", "als000500");
		
		headers.put("userMessage", "happy");
		headers.put("clientOrderId", order.getClientOrderId());
		headers.put("orderDate", currentDate);
		headers.put("billingName", user.getFirst_name()+" "+user.getLast_name());
		headers.put("billingEmail", user.getEmail());
		headers.put("billingAddressLine1", user.getAddress_one());
		headers.put("billingAddressLine2", user.getAddress_two());
		headers.put("billingCompany", "WooDoo");
		headers.put("billingCity", user.getCity());
		headers.put("billingState", user.getState());
		headers.put("billingCountry", "INDIA");
		headers.put("billingZip", user.getPin_code());
		headers.put("billingMobile", ""+user.getMobile_no());
        headers.put("billingPhone", ""+user.getMobile_no());
		headers.put("paymentMode", "NEFT");
		headers.put("paymentStatus", "Awaiting Payment");
		headers.put("lineItemQuantity", "1");
		headers.put("receiversName", user.getFirst_name());
		headers.put("receiversEmail", user.getEmail());
		headers.put("shippingAddressLine1", user.getAddress_one());
		
		if(user.getAddress_two()!= null)
			headers.put("shippingAddressLine2", "");
		
		headers.put("shippingCity", user.getCity());
		headers.put("shippingState", user.getState());
		headers.put("shippingCountry", "INDIA");
		headers.put("shippingZip", user.getPin_code());
		headers.put("shippingMobile", ""+user.getMobile_no());
		
		String response = null;
			try {
				response = HttpCaller.post(url, headers);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//		System.out.println(response);
//		
		JSONObject json = new JSONObject(response.toString());
//		System.out.println("JSON  "+json.toString());
		
		if(json.isNull("receipt"))
			throw new OrderFailException();
		else{
			String recept_id = json.getString("receipt");
			return recept_id;
		}
	}

	public String getOrdersOfUser(Long user_id) throws Exception {
		Session session = getSession();
		try	{
			Criteria c = session.createCriteria(Order.class);
			c.add(Restrictions.eq("userId", user_id));
			c.addOrder(org.hibernate.criterion.Order.desc("id"));
			ArrayList<Order> orders = (ArrayList<Order>) c.list();
			return JSONUtils.getSuccessJson(orders);
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}
	
	@Scheduled(cron = "0 23 12 * * *") // 23:12 it will run on
	public void scheduler()
	{
		Session session = getSession();
		try
		{
//			EmailSender email = new EmailSender("23:12 scheduller run", "Scheduller report", "sumit.verma@codeyeti.in");
//			email.start();
			
			fillBrandListInDataBase(session);
			
			Constants.flipkartData = null;
			Constants.flipkartData = CouponCaller.campaignApiCaller();
			
			Constants.offers = null;
			Constants.offers = CouponCaller.actionApiCaller();
			
			Constants.shoppingOffers = null;
			Constants.shoppingOffers = CouponCaller.shopingApiCaller();
	
			Constants.APICALLTIME = null;
			
			Constants.APICALLTIME = Timings.getCurrentTime();
			
			//Constants.espnNews = null;
			
			//Constants.espnNews = NewsApiCaller.newsApiCallerESPN();
			
			//Constants.ngNews = null;
			
			//Constants.ngNews = NewsApiCaller.newsApiCallerNGC();
			
			//Constants.toiNews = null;
			
			//Constants.toiNews = NewsApiCaller.newsApiCallerTOI();
			
			dooService.updateConversionData(session);
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			
			session.close();
		}
	}
		
//	@Scheduled(cron = "0 0/60 * * * ?") //  it will run hourly
	public void schedulerEveryHour()
	{
		Session session = getSession();
		try
		{
//			EmailSender email = new EmailSender("Hourly scheduler run", "Scheduller report", "sumit.verma@codeyeti.in");
//			email.start();
//			fillBrandListInDataBase(session);
			Constants.flipkartData = null;
			Constants.flipkartData = CouponCaller.campaignApiCaller();
			Constants.offers = null;
			Constants.offers = CouponCaller.actionApiCaller();
			Constants.shoppingOffers = null;
			Constants.shoppingOffers = CouponCaller.shopingApiCaller();
			
			Constants.APICALLTIME = null;
			
			Constants.APICALLTIME = Timings.getCurrentTime();
			
			//Constants.espnNews = null;
			
			//Constants.espnNews = NewsApiCaller.newsApiCallerESPN();
			
			//Constants.ngNews = null;
			
			//Constants.ngNews = NewsApiCaller.newsApiCallerNGC();
			
			//Constants.toiNews = null;
			
			//Constants.toiNews = NewsApiCaller.newsApiCallerTOI();
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			session.close();
		}
	}
	
	//@Scheduled(cron="0 0 11 * * *", zone = "IST")
	public void schedulerAtSevenAM()
	{
		Session session = getSession();
		
		Map<String, String> map1 = new HashMap<>();
		
		map1.put("message", "Rise & Shine! Check settings in WooDoo to ensure that it is \"ON\" for earning Woo Cash.");
		map1.put("type", "general");
		map1.put("title", "WooDoo");
		
		ArrayList<User> users1 = dao.getall(session, User.class);
		
		for(User user : users1)
		{
			List<String> gcm_ids = new ArrayList<>();
			gcm_ids.add(user.getGcm_id());
			Content content = new Content(gcm_ids, map1);			
			String a=PostGCM.post2(Constants.FCM_API_KEY, content);
			//String a=AndroidNotifications.generalNotificationToUser(user, map1);
		}
		
		try
		{
			for(int i=0;i<6;i++)
			{
				Map<String, String> map = new HashMap<>();
				map.put("type", "start_lock_screen_add");
				System.out.println("Chal vgaya");
				ArrayList<User> users = dao.getall(session, User.class);
				for(User user : users)
				{
					AndroidNotifications.generalNotificationToUser(user, map);
				}
				Thread.sleep(10 * 60 * 1000);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			session.close();
		}
	}
	@Scheduled(cron="0 0 22 * * *", zone = "IST")
	public void schedulerAtFourPM()
	{
		Session session = getSession();
		
		
		
		try
		{
			sendNotificationForNotGetPoint(session);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			session.close();
		}
	}
	
	//@Scheduled(cron="0 15 10 L * ?", zone = "IST")
	public void schedulerAtTwoPM()
	{
		Session session = getSession();

		try
		{
			Criteria c = session.createCriteria(Lsimpression.class);				
			c.add(Restrictions.eq("status", "PROCESSED"));
			ArrayList<Lsimpression> wootime = (ArrayList<Lsimpression>) c.list();
			for (Lsimpression obj:wootime) {
				Lsimpression wootemp=obj;				
				dao.delete(session, wootemp);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			session.close();
		}
	}
	
	
	
	@Scheduled(cron = "0 0 * * * *", zone = "IST") //  it will run hourly
	public void schedulerEverySevenMinute()
	{
//		EmailSender email = new EmailSender("Five minute scheduler run", "Scheduller report", "sumit.verma@codeyeti.in");
//		email.start();
//		
		Session session = getSession();
		try
		{
			Constants.APICALLTIME = null;
			
			Constants.APICALLTIME = Timings.getCurrentTime();
			
			Constants.espnNews = null;
			
			Constants.espnNews = NewsApiCaller.newsApiCallerESPN();
			
		    Constants.ngNews = null;
			
			Constants.ngNews = NewsApiCaller.newsApiCallerNGC();
			
		    Constants.toiNews = null;
			
			Constants.toiNews = NewsApiCaller.newsApiCallerTOI();
			sendNotificationAdmin();
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			session.close();
		}
	}

	public void sendNotificationAdmin() {
		Session session = getSession();
		try {
			Criteria c = session.createCriteria(User.class);
			c.add(Restrictions.eq("nstatus", true));
			ArrayList<User> allUsers = (ArrayList<User>) c.list();

			ArrayList<NotificationW> allnotification = dao.getall(session, NotificationW.class);
			for (NotificationW noti : allnotification) {
				Map<String, String> map = new HashMap<>();
				map.put("message", noti.getMessage());
				map.put("type", noti.getType());
				map.put("title", noti.getTitle());
				map.put("url", noti.getUrl());
				if (istrue(noti.getStartDate(), noti.getEndDate(), noti.getTime()) && noti.getStatus() == true) {
					List<String> gcm_ids = new ArrayList<>();
					if (noti.getIds().length() > 1) {
						String idss = noti.getIds().replace(" ", "");
						String[] ids = idss.split(",");
						for (int i = 0; i < ids.length; i++) {
							String string = ids[i];
							if (isInteger(string)) {
								User Users = (User) dao.getbyId(session, Integer.parseInt(string), User.class);
								if(Users.getGcm_id()!=null && Users.getGcm_id()!="" )
								{
								 gcm_ids.add(Users.getGcm_id());
								
								}
								Notifications notification = new Notifications();
								notification.setMessage(noti.getMessage());
								notification.setStatus(true);
								notification.setUser_id(Users.getUser_id());
								notification.setDatetime(Timings.getCurrentTime());
								dao.insert(session, notification);
							}
						}

					} else {
						for (User user : allUsers) {
							if(user.getGcm_id()!=null && user.getGcm_id()!="" )
							{
							 gcm_ids.add(user.getGcm_id());
							
							}
							// User user=(User) dao.getbyId(session, 9106, User.class);
							Notifications notification = new Notifications();
							notification.setMessage(noti.getMessage());
							notification.setStatus(true);
							notification.setUser_id(user.getUser_id());
							notification.setDatetime(Timings.getCurrentTime());
							dao.insert(session, notification);
						}
					}
					
					if(gcm_ids.size()<999) {
						AndroidNotifications.generalNotificationToUsers(gcm_ids, map);
						
					}else
					{
						int j=gcm_ids.size()/1000;
						for (int i=0;i<j;i++)
						{
							if((1000*i)+1000<gcm_ids.size())
							{
							List<String> sublist = gcm_ids.subList(1000*i, (1000*i)+1000);
							AndroidNotifications.generalNotificationToUsers(sublist, map);
							}else
							{
								List<String> sublist = gcm_ids.subList(1000*i, gcm_ids.size());
								AndroidNotifications.generalNotificationToUsers(sublist, map);
							}
						}
					}
					
					
				}
			}

		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}
	}
	public static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    } catch(NullPointerException e) {
	        return false;
	    }
	    // only got here if we didn't return false
	    return true;
	}
   public boolean istrue(String statdate,String enddate,String time)
   {
	   try {
	   
	Date date = new Date();
	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	formatter.setTimeZone(TimeZone.getTimeZone("IST"));		 	  
	String strDate = formatter.format(date);
	SimpleDateFormat formatter2 = new SimpleDateFormat("HH");
	formatter2.setTimeZone(TimeZone.getTimeZone("IST"));
	String hours = formatter2.format(date);
	   SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = sdf.parse(statdate);       
		Date date2 = sdf.parse(enddate);
		Date date3 = sdf.parse(strDate);
		if(date3.compareTo(date1) >= 0 && date2.compareTo(date3) >= 0)
		{
			if(time.contains(""+hours))
			{
				return true;
			}
		}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return false;
	}

	   return false;
   }
   
   
   
   public void sendNotificationForNotGetPoint(Session session) {
		try 
		{   
			Criteria c = session.createCriteria(User.class);
			c.add(Restrictions.eq("nstatus", true));
			ArrayList<User> allUsers = (ArrayList<User>) c.list();
			
			
			Map<String, String> map = new HashMap<>();
			map.put("message", "WooDoo Greetings! We noticed that you are missing on earning daily 50 Woo Cash & ASSURED Daily & Weekly Paytm Cash.For WooDoo's awesome Lock Screen & Rewards expereince Select ON in SETTINGS & SAVE.TCA.");
			map.put("type", "setting");
			map.put("title", "WooDoo");
			map.put("url", "");
			ArrayList<WooLog> att=getPointUser(session);
           	
			List<String> gcm_ids = new ArrayList<>();
			
			for(User user : allUsers)
			{
				
				boolean flag=true;
				for(WooLog log:att)					
				{
					if(log.getUser_id().equals(user.getUser_id()))
						
					{
						flag=false;
						break;
					}
					
				}
				if(flag)
				{
					map.put("userId", ""+user.getUser_id());	
					if(user.getGcm_id()!=null && user.getGcm_id()!="" )
					{
					 gcm_ids.add(user.getGcm_id());
					
					}
					
				}
				
			}
			if(gcm_ids.size()<999) {
				AndroidNotifications.generalNotificationToUsers(gcm_ids, map);
				
			}else
			{
				int j=gcm_ids.size()/1000;
				for (int i=0;i<j;i++)
				{
					if((1000*i)+1000<gcm_ids.size())
					{
					List<String> sublist = gcm_ids.subList(1000*i, (1000*i)+1000);
					AndroidNotifications.generalNotificationToUsers(sublist, map);
					}else
					{
						List<String> sublist = gcm_ids.subList(1000*i, gcm_ids.size());
						AndroidNotifications.generalNotificationToUsers(sublist, map);
					}
				}
			}
			
		} catch (Exception e) {
			throw e;
		}
		
	}
  
  public ArrayList<WooLog> getPointUser(Session session)  {
	  
		Criteria c = session.createCriteria(WooLog.class);
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));		 	  
		String strDate = formatter.format(date);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		String dateString = strDate+" 00:00:32";
		  Date date2;
		try {
			date2 = sdf.parse(dateString);
			c.add(Restrictions.gt("date_time", date2.getTime()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return (ArrayList<WooLog>) c.list();
	}
	
	public void updateDb() 
	{
		Session session = getSession();
		try
		{
//			ArrayList<WooLog> olds = dao.getall(session, WooLog.class);
//			ArrayList<Conversion> olds1 = dao.getall(session, Conversion.class);
//			for(WooLog log : olds)
//			{
//				if(log.getExtra_point() == null)
//					log.setLeft_points(log.getWoo_point());
//				else
//					log.setLeft_points(log.getWoo_point()+log.getExtra_point());
//				dao.update(session, log);
//			}
//			for(Conversion c : olds1)
//			{
//				c.setLeft_points(c.getUsers_payout());
//				dao.update(session, c);
//			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			session.close();
		}
	}
	
		

}
