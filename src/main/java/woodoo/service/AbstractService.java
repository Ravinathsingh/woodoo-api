package woodoo.service;


import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.google.gson.Gson;

import woodoo.configuration.AppConfig;
import woodoo.dao.DaoFactory;


public class AbstractService {
	Gson gson;
	DaoFactory dao;
	SessionFactory sessionFactory;
	AppConfig config;
	
	public AbstractService(DaoFactory dao, AppConfig config){
		this.gson = config.getGson();
		this.dao = dao;
		this.sessionFactory = config.getSessionFactory();
		this.config  = config;
	}
	
	public Session getSession(){
		Session session = sessionFactory.openSession();
		return session;
	}
	
	public void close(Session session){
		session.close();
	}
}
