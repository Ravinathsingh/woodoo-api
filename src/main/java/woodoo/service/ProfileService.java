package woodoo.service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional.TxType;

import org.apache.http.util.TextUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonAnyFormatVisitor;
import com.opencsv.CSVWriter;

import woodoo.configuration.AppConfig;
import woodoo.dao.DaoFactory;
import woodoo.dao.WooDooDao;
import woodoo.enums.Logintype;
import woodoo.enums.OtpType;
import woodoo.enums.WooStatus;
import woodoo.model.Admin;
import woodoo.model.City;
import woodoo.model.Conversion;
import woodoo.model.Notifications;
import woodoo.model.OTPInfo;
import woodoo.model.Order;
import woodoo.model.State;
import woodoo.model.User;
import woodoo.model.WooLog;
import woodoo.model.WooPromotions;
import woodoo.network.OtpEmailSender;
import woodoo.network.notification.AndroidNotifications;
import woodoo.utils.Constants;
import woodoo.utils.JSONUtils;
import woodoo.utils.NoSuchEntityException;
import woodoo.utils.StringUtils;
import woodoo.utils.Timings;

@Service
public class ProfileService extends AbstractService{

	@Autowired
	public ProfileService(DaoFactory dao, AppConfig config) {
		super(dao, config);
	}

	@Autowired
	DooService dooService;
	
	public String signUp(User user) throws Exception {
		Session session = getSession();
		try
		{
//			if((user.getEmail() == null && user.getFacebook_id() == null) || (user.getEmail().isEmpty() && user.getFacebook_id().isEmpty()))
//			{
//				return JSONUtils.getFailJson("Please send value of atleast one of these email or facebook_id");
//			}
//			System.out.println(user);
			User rUSER = null;
			if(user.getDob() == null)
				return JSONUtils.getFailJson("Please enter dob.");

			if(user.getUsed_referral_code() != null && !(user.getUsed_referral_code().trim().isEmpty()))
			{	
				rUSER = (User) dao.getbyUniqueColumn(session, "my_referral_code", user.getUsed_referral_code(), User.class);

				if(rUSER == null)
				{
					return JSONUtils.getFailJson("Referral code wrong.");
				}
			}

			if(user.getEmail() != null && !user.getEmail().isEmpty()){
				User checkEmail = (User) dao.getbyUniqueColumn(session, "email", user.getEmail(), User.class);
				if(checkEmail != null)
					return JSONUtils.getFailJson("email already used.");
			}

			if(user.getFacebook_id() != null && !user.getFacebook_id().isEmpty()){
				User checkfb = (User) dao.getbyUniqueColumn(session, "facebook_id", user.getFacebook_id(), User.class);
				if(checkfb != null)
					return JSONUtils.getFailJson("fb id already registered.");
			}

			if(user.getMobile_no() != null){
				User checkfb = (User) dao.getbyUniqueColumn(session, "mobile_no", user.getMobile_no(), User.class);
				if(checkfb != null)
					return JSONUtils.getFailJson("Mobile no. used by different user.");
			}
			String rCode = "";
			for(;;)
			{
				rCode = ""+Constants.generateRandomString(5);
				User userOlda = (User) dao.getbyUniqueColumn(session, "my_referral_code", rCode, User.class);
				if(userOlda == null){
					break;
				}
			}
			//			System.out.println("Refer : "+rCode);

			if(user.getDevice_id() != null)
			{

				Map<String, Object> map = new HashMap<>();
				map.put("device_id", user.getDevice_id());
				map.put("mobile", user.getMobile_no());
				
				OTPInfo old =  (OTPInfo) dao.getbyUniqueColumns(session, map , OTPInfo.class);
				if(old == null || !old.getIsOtpUsed())
				{	
					System.out.println("Returned from old check");
					return JSONUtils.getFailJson();
				}
			}
//			else
//			{
//				return JSONUtils.getFailJson();
//			}
//			
			user.setMy_referral_code(rCode);
			user.setAge(Timings.getAge(user.getDob()));
			user.setStatus(true);
			user.setSignUpDate(Timings.getCurrentTime());		
			user.setNstatus(true);
			dao.insert(session, user);
			

			if(user.getUsed_referral_code() != null && !user.getUsed_referral_code().isEmpty())
			{
				WooLog log = new WooLog();
				
				log.setDate_time(Timings.getCurrentTime());
				log.setExtra_point(0l);
				log.setIsBonus(true);
				log.setLeft_points(500l);
				log.setUser_id(user.getUser_id());
				log.setWoo_point(500l);
				
				dao.insert(session, log);
				
				WooLog log1 = new WooLog();
				
				log1.setDate_time(Timings.getCurrentTime());
				log1.setExtra_point(0l);
				log1.setIsBonus(true);
				log1.setLeft_points(500l);
				log1.setUser_id(rUSER.getUser_id());
				log1.setWoo_point(500l);
				
				dao.insert(session, log1);
				
				
				
//				// Fill action doo cash table here
//				Conversion conversion = new Conversion();
//				//				conversion.setApproved_payout(Constants.referralAmount);
//				conversion.setConversion_status(true);
//				conversion.setConversion_time(Timings.getCurrentTime());
//				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
//				Date date = new Date(Timings.getCurrentTime());
//				conversion.setDate_time(format.parse(format.format(date)));
//				conversion.setStatus(WooStatus.MYCHOICE.toString());
//				conversion.setType("REFERRAL");
//				conversion.setUser_id(user.getUser_id());
//				conversion.setUsers_payout(Constants.referralAmount);
//				conversion.setOfferName("Referral");
//				conversion.setLeft_points(conversion.getUsers_payout());
//
//				dao.insert(session, conversion);
			}
			return JSONUtils.getSuccessJson(user);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String userLogin(String login_type, String email, String facebook_id, String password, String fcm_token) throws Exception {
		Session session = getSession();
		try
		{
			if(Enum.valueOf(Logintype.class, login_type) == null){
				return JSONUtils.getFailJson("Login type is not valid. options are (EMAILPASSWORD,GOOGLE,FACEBOOK)");
			}

			if(login_type.equalsIgnoreCase(Logintype.EMAILPASSWORD.toString()))
			{
				if(email == null || email.isEmpty() || password == null || password.isEmpty())
					return JSONUtils.getFailJson("please enter both email and password.");

				Map<String , Object> map = new HashMap<>();
				map.put("email", email);
				//				map.put("password", password);

				User emailpassworduser = (User) dao.getbyUniqueColumns(session, map, User.class); 
				if(emailpassworduser != null && !emailpassworduser.getPassword().equalsIgnoreCase(password))
				{
					return JSONUtils.getFailJson("Incorrect Password");
				}

				if(emailpassworduser != null && emailpassworduser.getStatus().equals(false))
				{
					return JSONUtils.getFailJson(Constants.ACCOUNT_DEACTIVATION_MESSAGE);
				}

				
				if(emailpassworduser == null)
					return JSONUtils.getFailJson("Incorrect email or password");
				else
				{
					emailpassworduser.setGcm_id(fcm_token);
					dao.update(session, emailpassworduser);
					return JSONUtils.getSuccessJsonUserLogin(emailpassworduser,true);
				}
			}

			if(login_type.equalsIgnoreCase(Logintype.PHONEPASSWORD.toString()))
			{
				if(email == null || email.isEmpty() || password == null || password.isEmpty())
					return JSONUtils.getFailJson("please enter both email and password.");

				Map<String , Object> map = new HashMap<>();
				map.put("mobile_no", email);
				//				map.put("password", password);

				User emailpassworduser = (User) dao.getbyUniqueColumns(session, map, User.class); 

				if(emailpassworduser != null && ! emailpassworduser.getPassword().equalsIgnoreCase(password))
				{
					return JSONUtils.getFailJson("Incorrect Password");
				}

				if(emailpassworduser == null)
					return JSONUtils.getFailJson("Incorrect phone number or password");
				else
				{
					
					if(emailpassworduser.getStatus().equals(false))
					{
						return JSONUtils.getFailJson(Constants.ACCOUNT_DEACTIVATION_MESSAGE);
					}
					
					emailpassworduser.setGcm_id(fcm_token);
					dao.update(session, emailpassworduser);
					return JSONUtils.getSuccessJsonUserLogin(emailpassworduser,true);
				}
			}

			if(login_type.equalsIgnoreCase(Logintype.FACEBOOK.toString()))
			{
				if(facebook_id == null || facebook_id.isEmpty())
					return JSONUtils.getFailJson("please sent facebook_id.");

				Map<String , Object> map = new HashMap<>();
				map.put("facebook_id", facebook_id);
				User facbookUser = (User) dao.getbyUniqueColumns(session, map, User.class); 
				if(facbookUser == null && !TextUtils.isEmpty(email))
				{
					facbookUser = (User) dao.getbyUniqueColumn(session, "email", email, User.class);
				}
				if(facbookUser == null)
					return JSONUtils.getSuccessJsonUserLogin(null,false);
				else
				{
					if(facbookUser.getStatus().equals(false))
					{
						return JSONUtils.getFailJson(Constants.ACCOUNT_DEACTIVATION_MESSAGE);
					}

					
					facbookUser.setGcm_id(fcm_token);
					dao.update(session, facbookUser);
					return JSONUtils.getSuccessJsonUserLogin(facbookUser,true);
				}
			}

			if(login_type.equalsIgnoreCase(Logintype.GOOGLE.toString()))
			{
				if(email == null)
					return JSONUtils.getFailJson("please send email.");

				Map<String , Object> map = new HashMap<>();
				map.put("email", email);

				User googleUser = (User) dao.getbyUniqueColumns(session, map, User.class); 

				if(googleUser == null)
					return JSONUtils.getSuccessJsonUserLogin(null,false);
				else
				{
					if(googleUser.getStatus().equals(false))
					{
						return JSONUtils.getFailJson(Constants.ACCOUNT_DEACTIVATION_MESSAGE);
					}
					googleUser.setGcm_id(fcm_token);
					dao.update(session, googleUser);
					return JSONUtils.getSuccessJsonUserLogin(googleUser,true);
				}
			}
			return JSONUtils.getFailJson("Unknown error at backend");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String userProfileUpdate(User user) throws Exception {
		Session session = getSession();
		try
		{
			User checkEmail = (User) dao.getbyUniqueColumn(session, "email", user.getEmail(), User.class);
			if(checkEmail != null)
				if(! checkEmail.getUser_id().equals(user.getUser_id()))
					return JSONUtils.getFailJson("This email is used by any other user.");

			User oldUser = (User) dao.getbyId(session, user.getUser_id(), User.class);

			if(oldUser == null)
			{
				return JSONUtils.getFailJson("User not found with this id.");
			}
			
			if(user.getGcm_id() != null)
				oldUser.setGcm_id(user.getGcm_id());
			
			oldUser.setAddress_one(user.getAddress_one());
			oldUser.setAddress_two(user.getAddress_two());
			oldUser.setCity(user.getCity());
			oldUser.setDob(user.getDob());
			oldUser.setOtherCityName(user.getOtherCityName());    
			//			oldUser.setEmail(user.getEmail());

			//			if(user.getFacebook_id() != null)
			//				oldUser.setFacebook_id(user.getFacebook_id());

			oldUser.setFirst_name(user.getFirst_name());
			oldUser.setGender(user.getGender());
			oldUser.setLast_name(user.getLast_name());
//			oldUser.setMembership_plan(user.getMembership_plan());
			//			oldUser.setMobile_no(user.getMobile_no());
			oldUser.setPassword(user.getPassword());
			//			oldUser.setPhone_value(user.getPhone_value());
			oldUser.setPin_code(user.getPin_code());
			oldUser.setState(user.getState());
			oldUser.setAge(Timings.getAge(oldUser.getDob()));
			dao.update(session, oldUser);
			//			dao.insert(session, user);
			return JSONUtils.getSuccessJson(oldUser);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String otpsender(String type, String email, String mobile_no) throws Exception {
		Session session = getSession();
		try
		{
			if(type.equalsIgnoreCase(OtpType.FORGET_EMAIL.toString())){
				if(email == null || email.isEmpty()){
					return JSONUtils.getFailJson("Please enter email.");
				}
				User checkEmail = (User) dao.getbyUniqueColumn(session, "email", email, User.class);
				if(checkEmail == null){
					return JSONUtils.getFailJson("Account not found.");
				}
				JSONObject jobj = new JSONObject();
				String otp = String.valueOf(new Random().nextInt(9000) + 1000);

				String message = "Hi. "+otp+" is your login OTP. \n you are one step away from WooDoo to begin. Cheers! Team WooDoo.";
				OtpEmailSender sender = new OtpEmailSender(message, "WooDoo OTP", email);
				sender.run();		
				jobj.put("otp", otp);
				jobj.put("user_id", checkEmail.getUser_id());
				jobj.put("password", checkEmail.getPassword());
				jobj.put("status", "success");
				System.out.println("Response : "+jobj.toString());
				return jobj.toString();
			}

			if(type.equalsIgnoreCase(OtpType.FORGET_MOBILE.toString())){
				if(mobile_no == null || mobile_no.isEmpty()){
					return JSONUtils.getFailJson("Please enter mobile no.");
				}
				User checkMobile = (User) dao.getbyUniqueColumn(session, "mobile_no", mobile_no, User.class);
				if(checkMobile == null){
					return JSONUtils.getFailJson("Account not found.");
				}
				JSONObject jobj = new JSONObject();
				String otp = String.valueOf(new Random().nextInt(9000) + 1000);
				String message = "Hi. "+otp+" is your login OTP. \n you are one step away from WooDoo to begin. Cheers! Team WooDoo.";
				OtpEmailSender.verifyPhone(""+mobile_no, message);
				jobj.put("otp", otp);
				jobj.put("user_id", checkMobile.getUser_id());
				jobj.put("password", checkMobile.getPassword());
				jobj.put("status", "success");
				System.out.println("Response : "+jobj.toString());
				return jobj.toString();
			}


			if(type.equalsIgnoreCase(OtpType.EMAIL.toString())){
				if(email == null || email.isEmpty()){
					return JSONUtils.getFailJson("Please enter email.");
				}
				User checkEmail = (User) dao.getbyUniqueColumn(session, "email", email, User.class);
				if(checkEmail != null){
					return JSONUtils.getFailJson("Email already used.");
				}
				JSONObject jobj = new JSONObject();
				String otp = String.valueOf(new Random().nextInt(9000) + 1000);
				//				OtpEmailSender.send(email.trim(), otp);
				String message = "Hi. "+otp+" is your login OTP. \n you are one step away from WooDoo to begin. Cheers! Team WooDoo.";
				OtpEmailSender sender = new OtpEmailSender(message, "WooDoo OTP", email);
				sender.run();		
				jobj.put("otp", otp);
				jobj.put("status", "success");
				System.out.println("Response : "+jobj.toString());
				return jobj.toString();
			}

			if(type.equalsIgnoreCase(OtpType.MOBILE.toString())){
				if(mobile_no == null || mobile_no.isEmpty()){
					return JSONUtils.getFailJson("Please enter mobile no.");
				}
				User checkMobile = (User) dao.getbyUniqueColumn(session, "mobile_no", mobile_no, User.class);
				if(checkMobile != null){
					return JSONUtils.getFailJson("Mobile no. already used.");
				}
				JSONObject jobj = new JSONObject();
				String otp = String.valueOf(new Random().nextInt(9000) + 1000);
				String message = "Hi. "+otp+" is your login OTP. \n you are one step away from WooDoo to begin. Cheers! Team WooDoo.";
				OtpEmailSender.verifyPhone(""+mobile_no, message);
				jobj.put("otp", otp);
				jobj.put("status", "success");
				System.out.println("Response : "+jobj.toString());
				return jobj.toString();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
		return JSONUtils.getFailJson();
	}

	public String forgetPassword(String type, String email, String mobile_no) throws Exception {
		Session session = getSession();
		try
		{
			return JSONUtils.getSuccessJson();
			//			if(type.equalsIgnoreCase(OtpType.EMAIL.toString())){
			//				if(email == null || email.isEmpty()){
			//					return JSONUtils.getFailJson("Please enter email.");
			//				}
			//				
			//				User checkEmail = (User) dao.getbyUniqueColumn(session, "email", email, User.class);
			//				if(checkEmail != null){
			//					return JSONUtils.getFailJson("Email already used.");
			//				}
			//				JSONObject jobj = new JSONObject();
			//
			//				String otp = String.valueOf(new Random().nextInt(9000) + 1000);
			//				OtpEmailSender.send(email.trim(), otp);
			//				jobj.put("otp", otp);
			//				jobj.put("status", "success");
			//				return jobj.toString();
			//			}
			//			
			//			if(type.equalsIgnoreCase(OtpType.MOBILE.toString())){
			//				if(mobile_no == null || mobile_no.isEmpty()){
			//					return JSONUtils.getFailJson("Please enter mobile no.");
			//				}
			//				Long no = Long.parseLong(mobile_no);
			//				User checkMobile = (User) dao.getbyUniqueColumn(session, "mobile_no", no, User.class);
			//				if(checkMobile != null){
			//					return JSONUtils.getFailJson("Mobile no. already used.");
			//				}
			//				JSONObject jobj = new JSONObject();
			//
			//				String otp = String.valueOf(new Random().nextInt(9000) + 1000);
			////				OtpEmailSender.send(email.trim(), otp);
			//				jobj.put("otp", otp);
			//				jobj.put("status", "success");
			//				return jobj.toString();
			//			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
		//		return JSONUtils.getFailJson();

	}

	public String getStates() throws Exception {
		Session session = getSession();
		try
		{
			List<State> state = new ArrayList<>();
			Long country_id = 106l; // i.e INDIA
			state.addAll(dao.getWooDoodao().getSortedStates(session, "country_id", country_id, State.class));

			return JSONUtils.getSuccessJson(state);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String getCities(Long state_id) throws Exception {
		Session session = getSession();
		try
		{
			ArrayList<City> cities = new ArrayList<>();
			cities.addAll(dao.getWooDoodao().getSortedCity(session, "state_id", state_id, City.class));
			City city = new City();
			city.setCity_name("Other");
			city.setState_id(0L);
			city.setId(0L);     
			cities.add(city);    
			return JSONUtils.getSuccessJson(cities);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
		//		return JSONUtils.getFailJson();
	}

	public String logout(Long my_user_id) throws Exception {
		Session session = getSession();
		try
		{
			User user = (User) dao.getbyId(session, my_user_id, User.class);
			if(user == null)
			{
				return JSONUtils.getFailJson("User doesn't exist.");
			}
			user.setGcm_id(null);
			dao.update(session, user);
			return JSONUtils.getSuccessJson("Successfully logout.");
		}
		catch (Exception e )
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String changePassword(Long user_id, String old_password, String new_password, String gcm) throws Exception {
		Session session = getSession();
		try
		{
			User user = (User) dao.getbyId(session, user_id, User.class);
			
			if(user == null)
				return JSONUtils.getFailJson(Constants.ACCOUNT_DELETION_MESSAGE);
			
			if(user.getStatus().equals(false))
			{
				return JSONUtils.getFailJson(Constants.ACCOUNT_DEACTIVATION_MESSAGE);
			}
			
			if(!user.getPassword().equals(old_password))
			{
				return JSONUtils.getFailJson("Old password is wrong");
			}

			if(user.getPassword().equals(new_password))
			{
				return JSONUtils.getFailJson("Old and new password are same.");
			}

			if(gcm != null && !gcm.isEmpty())
				user.setGcm_id(gcm);

			user.setPassword(new_password);  
			dao.update(session, user);
			
			JSONObject obj = new JSONObject();
			obj.put("status", "success");
			obj.put("message", "Password changed successfully.");
			obj.put("data",  new JSONObject(user));

			return obj.toString();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String updateGCM(Long my_user_id, String gcm_id) throws Exception {
		Session session = getSession();
		try
		{
			User user = (User) dao.getbyId(session, my_user_id, User.class);
			
			if(user == null)
				return JSONUtils.getFailJson(Constants.ACCOUNT_DELETION_MESSAGE);
		
			user.setGcm_id(gcm_id);
			dao.update(session, user);
			
			if(user.getStatus().equals(false))
			{
				user.setGcm_id(null);
				dao.update(session, user);
				
				return JSONUtils.getFailJson(Constants.ACCOUNT_DEACTIVATION_MESSAGE);
			}
			
			return JSONUtils.getSuccessJson("GCM updated successfully.");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}
	public String InsertUsrData(String device_id ,String gcm_id,String mob ,String email) throws Exception {
		Session session = getSession();
		try
		{
			User user = new User();
			user.setGcm_id(gcm_id);
			user.setMobile_no(mob);
			user.setEmail(email);
			user.setDevice_id(device_id);
			dao.insert(session, user);
			
			
			
			return JSONUtils.getSuccessJson("User Insert successfully.");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String searchUser(String email, Long pageNo) throws Exception {
		Session session = getSession();
		if(pageNo > 0)
			pageNo = pageNo - 1;
		try
		{
			int maxSize = 10;
			String users = dao.getWooDoodao().searchUser(session, maxSize, email,pageNo);
			return users;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String getWooLog(Long user_id, HttpServletResponse response) throws IOException {
		Session session = getSession();
		TimeZone tz = TimeZone.getTimeZone("IST");

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(Timings.getCurrentTime());
		calendar.setTimeZone(tz);
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		User user = (User) dao.getbyId(session, user_id, User.class);
		if(user == null)
			return JSONUtils.getFailJson("User not found.");

		String csvFileName = "Name-"+user.getFirst_name()+" "+user.getLast_name()+"-"+ dateFormat.format(calendar.getTime()) + ".csv";
		response.setContentType("text/csv");
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		response.setHeader(headerKey, headerValue);

		BufferedWriter writer = new BufferedWriter(response.getWriter());
		writer.append(
				"Sr. No.,Date Time,Woo Point,Extra Points\n");

		JSONArray jarray = new JSONArray();
		try {
			List<WooLog> wooLog = dao.getlistbyUniqueColumn(session, "user_id", user_id, WooLog.class);

			System.out.println("Size "+wooLog.size());

			if (wooLog.size() > 0) {
				int i = 0;
				for (Object obj : wooLog) {
					i++;
					WooLog us = (WooLog) obj;
					jarray.put(i);
					Date dateL = new Date(us.getDate_time());
					dateTimeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
					jarray.put(dateTimeFormat.format(dateL));
					if(us.getWoo_point() == null)
						jarray.put(0);
					else
						jarray.put(us.getWoo_point());

					if(us.getExtra_point() == null)
						jarray.put(0);
					else
						jarray.put(us.getExtra_point());
				}
			}
		} finally {
			session.close();
		}
		//		System.out.println(jarray);
		for (int i = 0; i < jarray.length(); i = i + 4) 
		{
			//			System.out.println(String.format("%s,%s,%s,%s\n", jarray.get(i + 0), jarray.get(i + 1),
			//					jarray.get(i + 2), jarray.get(i + 3)));
			writer.append(String.format("%s,%s,%s,%s\n", jarray.get(i + 0), jarray.get(i + 1),
					jarray.get(i + 2), jarray.get(i + 3)));
		}
		writer.flush();
		writer.close();
		return jarray.toString();
	}

	public String getDooLog(Long user_id, HttpServletResponse response) throws IOException {
		Session session = getSession();
		TimeZone tz = TimeZone.getTimeZone("IST");

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(Timings.getCurrentTime());
		calendar.setTimeZone(tz);
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		User user = (User) dao.getbyId(session, user_id, User.class);
		if(user == null)
			return JSONUtils.getFailJson("User not found.");

		String csvFileName = "Name-"+user.getFirst_name()+" "+user.getLast_name()+"-"+ dateFormat.format(calendar.getTime()) + ".csv";
		response.setContentType("text/csv");
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		response.setHeader(headerKey, headerValue);

		BufferedWriter writer = new BufferedWriter(response.getWriter());
		writer.append(
				"Sr. No.,Date Time,Offer Name,Type,Approved Payout,User's Payout,Conversion Status,Our Status,Left Points \n");

		JSONArray jarray = new JSONArray();
		try {
			List<Conversion> wooLog = dao.getlistbyUniqueColumn(session, "user_id", user_id, Conversion.class);

			if (wooLog.size() > 0) {
				int i = 0;
				for (Conversion us : wooLog)
				{
					i++;
					jarray.put(i);
					if(us.getConversion_time() == null) {
						jarray.put(0);
					}
					else {
						Date dateL = new Date(us.getConversion_time());
						dateTimeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
						jarray.put(dateTimeFormat.format(dateL));
					}
					if(us.getOfferName() == null) {
						jarray.put(0);
					}
					else {
						jarray.put(us.getOfferName());
					}
					if(us.getType() == null) {
						jarray.put(0);
					}
					else {
						jarray.put(us.getType());
					}
					if(us.getApproved_payout() == null)
						jarray.put(0);
					else
						jarray.put(us.getApproved_payout());
					if(us.getUsers_payout() == null) {
						jarray.put(0);
					}
					else {
						jarray.put(us.getUsers_payout());
					}
					if(us.getConversion_status() == null) {
						jarray.put(0);
					}
					else {
						jarray.put(us.getConversion_status());
					}
					if(us.getStatus() == null)
						jarray.put(0);
					else
						jarray.put(us.getStatus());
					if(us.getLeft_points() == null) {
						jarray.put(0);
					}
					else {
						jarray.put(us.getLeft_points());
					}

				}
			}
		} 
		finally {
			session.close();
		}
		//		System.out.println(jarray.toString());
		for (int i = 0; i < jarray.length(); i = i + 9) {
			writer.append(String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s\n", jarray.get(i + 0), jarray.get(i + 1),
					jarray.get(i + 2), jarray.get(i + 3), jarray.get(i + 4), jarray.get(i + 5), jarray.get(i + 6)
					, jarray.get(i + 7), jarray.get(i + 8)));
		}
		writer.flush(); 
		writer.close();
		return jarray.toString();
	}

	public String adminLogin(String username, String password) throws Exception {
		Session session = getSession();
		try
		{
			Map<String, Object> map = new HashMap<>();
			map.put("username", username);
			map.put("password", password);

			Admin admin = (Admin) dao.getbyUniqueColumns(session, map, Admin.class);

			if(admin != null)
				return JSONUtils.getSuccessJson(admin);
			else
				return JSONUtils.getFailJson("Wrong credential.");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String getUserById(Long user_id) throws Exception {
		Session session = getSession();
		try
		{
			User user = (User) dao.getbyId(session, user_id, User.class);
			
			if(user != null)
			{
				JSONObject dooData = new JSONObject(dooService.wooDooBox(user_id));

//				String data =  gson.toJson(dooData.get("data")).toString();
				
//				JSONObject result = new JSONObject();
//				result.put("action_do_data", dooData.get("data"));

				
				JSONObject obj = new JSONObject();
				obj.put("status", "success");
				obj.put("data", new JSONObject(gson.toJson(user)));
				

				obj.put("doo_cash", dooData.get("doo_cash"));
				obj.put("woo_cash", dooData.get("woo_cash"));
				obj.put("total_earned", dooData.get("total_earned"));

				return obj.toString();

			}
			else
				return JSONUtils.getFailJson("Wrong user_id.");
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}

	}

	public String downloadUserCsv(long admin_id, HttpServletResponse response,String statdate,String enddate,String report,String starttime,String endtime) throws Exception {
		
		Session session = getSession();
		
		try {   
			Admin admin = (Admin) dao.getbyId(session, admin_id, Admin.class);
			if(admin == null) {
				return JSONUtils.getFailWithReasonJson("Admin does not exist");
			}
			
			Criteria c = session.createCriteria(User.class);
			 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		        Date date1 = sdf.parse(statdate+" "+starttime);       
				Date date2 = sdf.parse(enddate+" "+endtime);
			Date date = new Date();
			
			SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
			String dateString = date1+" 00:00:32";
			String dateString2 = date2+" 00:00:32";
			  Date date3;
			  Date date4;
			  if(report.equalsIgnoreCase("1")) {
			try {
				//date3 = sdf2.parse(dateString);
				//date4 = sdf2.parse(dateString2);
				c.add(Restrictions.gt("signUpDate", date1.getTime()));
				c.add(Restrictions.lt("signUpDate", date2.getTime()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  }
			List<User> users =(ArrayList<User>)c.list();
			
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
			String csvFileName = dateFormat.format(date)+"_USER.csv";
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
			System.out.println("Header value "+headerValue);  
			response.setHeader(headerKey, headerValue);
			//response.setCharacterEncoding("UTF-8");
			response.setContentType("text/csv"); 
			CSVWriter csvWriter = new CSVWriter(response.getWriter());
			
			String []header = new String[]{"USER ID.","USER FIRST NAME","USER LAST NAME","AGE","GENDER","EMAIL ID","MOBILE NUMBER","MY REFERRAL CODE","USED REFERRAL CODE","STATE","CITY","PINCODE","PHONE VALUE",
			"PENDING ACTION DOO CASH ","PENDING SHOPPING DOO CASH","TOTAL PENDING DOO CASH","TOTAL CONFIRMED DOO CASH"
			,"AVAILABLE FOR INSTANT REDEMPTION","AVAILABLE FOR MY CHOICE REDEMPTION","LOCK SCREEN WOO CASH","BONUS WOO CASH",
			"TOTAL WOO CASH","TOTAL EARNED","TOTAL ENCASHED","GAP FROM VALUE BACK TARGET","MEMBERSHIP PLAN","SIGNUP DATE"
			};
			csvWriter.writeNext(header); 
			
			String[]row = null;
			long i = 1;
			
//			DateFormat format = new SimpleDateFormat("dd mm yyyy");
			
			for(User user : users) {
				Map<String, String> result = dooService.woodoodata(user.getUser_id(),session,user,date1.getTime(),date2.getTime(),report);
				
				String lastName = user.getLast_name();
				if(lastName == null)
					lastName = "";
				String age = Integer.toString(WooDooDao.calculateAgeInYear(user.getDob()));
				
				String memberShipPlan = user.getMembership_plan();
				if(memberShipPlan == null) 
					memberShipPlan = "";
				String phoneValue = Long.toString(user.getPhone_value()); 
				
				String suDate = "";
				
				if(user.getSignUpDate() != null)
				{
					Date sidate = new Date(user.getSignUpDate());
					dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
					suDate = dateFormat.format(sidate);
				}
				else
				{
					Date sidate = new Date(Timings.getCurrentTime());
					dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
					suDate = dateFormat.format(sidate);	
				}
					
				row = new String[]{user.getUser_id()+"",user.getFirst_name(),lastName,""+user.getAge(),user.getGender(),
						user.getEmail(),user.getMobile_no(),user.getMy_referral_code(),user.getUsed_referral_code(),user.getState(),user.getCity(),user.getPin_code(),phoneValue,
						result.get("pending_action_doo_cash"),
						result.get("pending_shopping_doo_cash"),
						result.get("total_pending_doo_cash"),
						result.get("total_confirmed_doo_cash"),
						result.get("available_for_instant_redemption"),
						result.get("available_for_my_choice_redemption"),
						result.get("lock_screen_woo_cash"),
						result.get("bonus_woo_cash"),
						result.get("total_woo_cash"),
						result.get("total_confirmed"),
						result.get("total_encash"),
						result.get("gap_from_value_back_target"),"BRONZE",suDate
				};
				csvWriter.writeNext(row);
				i++; 
			}
			csvWriter.flush();      
			csvWriter.close();   
			return JSONUtils.getSuccessJson(); 
		} catch (Exception e) {
			throw e;
		}
		finally {
			session.close();
		}
	}
	
public String downloadPaytmCsvR(long admin_id, HttpServletResponse response) throws Exception {
		
		Session session = getSession();
		
		try {   
			Admin admin = (Admin) dao.getbyId(session, admin_id, Admin.class);
			if(admin == null) {
				return JSONUtils.getFailWithReasonJson("Admin does not exist");
			}		
			
			Criteria c1 = session.createCriteria(Order.class);
			c1.add(Restrictions.eq("type", "PENDING"));
			ArrayList<Order> add = (ArrayList<Order>) c1.list();
			Date date = new Date();		
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
			String csvFileName = dateFormat.format(date)+"_PAYTM_REQUEST.csv";
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
			System.out.println("Header value "+headerValue);  
			response.setHeader(headerKey, headerValue);
			//response.setCharacterEncoding("UTF-8");
			response.setContentType("text/csv"); 
			CSVWriter csvWriter = new CSVWriter(response.getWriter());
			
			String []header = new String[]{"User ID","User Name","Order ID","Mobile No","Requested Redemption Value","Date of Redemption Request"};
			csvWriter.writeNext(header); 
			
			String[]row = null;
			long i = 1;
			
//			DateFormat format = new SimpleDateFormat("dd mm yyyy");
			
			for(Order order : add) {			
					
				row = new String[]{order.getUserId()+"",order.getBillingName(),order.getClientOrderId(),""+order.getBillingMobile(),order.getAmount(),order.getOrderDate()};
				csvWriter.writeNext(row);
				i++; 
			}
			csvWriter.flush();      
			csvWriter.close();   
			return JSONUtils.getSuccessJson(); 
		} catch (Exception e) {
			throw e;
		}
		finally {
			session.close();
		}
	}
public String downloadPayTMCsvC(long admin_id, HttpServletResponse response,String statdate,String enddate) throws Exception {
	
	Session session = getSession();
	
	try {   
		Admin admin = (Admin) dao.getbyId(session, admin_id, Admin.class);
		if(admin == null) {
			return JSONUtils.getFailWithReasonJson("Admin does not exist");
		}
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        Date date1 = sdf.parse(statdate);       
			Date date2 = sdf.parse(enddate);
		Criteria c = session.createCriteria(User.class);
		
		Date date = new Date();
	
		Criteria c1 = session.createCriteria(Order.class);
		c1.add(Restrictions.eq("type", "CONFIRM"));				
		ArrayList<Order> addon = (ArrayList<Order>) c1.list();
		ArrayList<Order> tmp=new ArrayList<Order>();
		for (int i = 0; i < addon.size(); i++) {
			try {
				SimpleDateFormat sdfmt1 = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat sdfmt2= new SimpleDateFormat("yyyy-MM-dd");
				java.util.Date dDate = sdfmt1.parse( addon.get(i).getShippingAddressLine1() );
				String strOutput = sdfmt2.format( dDate );
				Date datetemp=new SimpleDateFormat("yyyy-MM-dd").parse(strOutput);	
				if(datetemp.after(date1) && datetemp.before(date2)) {
					tmp.add(addon.get(i));
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
					
			
		}
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
		String csvFileName = dateFormat.format(date)+"_PAYTM_CONFIRM.csv";
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
		System.out.println("Header value "+headerValue);  
		response.setHeader(headerKey, headerValue);
		//response.setCharacterEncoding("UTF-8");
		response.setContentType("text/csv"); 
		CSVWriter csvWriter = new CSVWriter(response.getWriter());
		
		String []header = new String[]{"User ID","User Name","Order ID","Mobile No","Requested Redemption Value","Date of Redemption Request","Date of Redemption Processed"};
		csvWriter.writeNext(header); 
		
		String[]row = null;
		long i = 1;
		
//		DateFormat format = new SimpleDateFormat("dd mm yyyy");
		
		for(Order order : tmp) {			
				
			row = new String[]{order.getUserId()+"",order.getBillingName(),order.getClientOrderId(),""+order.getBillingMobile(),order.getAmount(),order.getOrderDate(),order.getShippingAddressLine1()};
			csvWriter.writeNext(row);
			i++; 
		}
		csvWriter.flush();      
		csvWriter.close();   
		return JSONUtils.getSuccessJson(); 
	} catch (Exception e) {
		throw e;
	}
	finally {
		session.close();
	}
}
	
//	/* Need to change these code position */
//	
//	public Map<String, String> woodoodata(Long my_user_id, Session session, User user)  {
//
//		ArrayList<Conversion> conversions = dao.getWooDoodao().getUnusedDoos(session, my_user_id);
//
//		//		System.out.println("here i m");
//		Double pending_action_doo_cash = 0.0; //
//		Double pending_shopping_doo_cash = 0.0;//
//
//		Double total_pending_doo_cash = 0.0;//
//		Double total_confirmed_doo_cash = 0.0;
//
//		Double available_for_instant_redemption = 0.0;
//		Double available_for_my_choice_redemption = 0.0;
//
//		DecimalFormat df = new DecimalFormat("##0.0");      
//		//		time = Double.valueOf(df.format(time));
//
//		Long total_woo_cash = 0l;
//		for( Conversion conversion : conversions )
//		{
//			String status = conversion.getStatus();
//			if(status.equalsIgnoreCase(WooStatus.MYCHOICE.toString()))
//			{
//				available_for_my_choice_redemption = available_for_my_choice_redemption + conversion.getLeft_points();
//			}
//			else if(status.equalsIgnoreCase(WooStatus.CONFIRMED.toString()))
//			{
//				total_confirmed_doo_cash = total_confirmed_doo_cash + conversion.getLeft_points();
//			}
//			if(status.equalsIgnoreCase(WooStatus.PENDING.toString()))
//			{
//				if(conversion.getType().equalsIgnoreCase("ACTION"))
//				{
//					pending_action_doo_cash = pending_action_doo_cash + conversion.getLeft_points();
//				}
//				if(conversion.getType().equalsIgnoreCase("SHOPPING"))
//				{
//					pending_shopping_doo_cash = pending_shopping_doo_cash + conversion.getLeft_points();
//				}
//			}
//		}
//
//		total_pending_doo_cash = pending_action_doo_cash + pending_shopping_doo_cash;
//
//		Map<String , String> result = new HashMap<>();
//
//		System.out.println(df.format(pending_action_doo_cash));
//		System.out.println(df.format(pending_shopping_doo_cash));
//		System.out.println(df.format(total_pending_doo_cash));
//		System.out.println(df.format(total_confirmed_doo_cash));
//		System.out.println(df.format(available_for_my_choice_redemption));
//
//
//		result.put("pending_action_doo_cash", ""+pending_action_doo_cash.longValue());
//		result.put("pending_shopping_doo_cash", ""+pending_shopping_doo_cash.longValue());
//		result.put("total_pending_doo_cash", ""+total_pending_doo_cash.longValue());
//		result.put("total_confirmed_doo_cash", ""+total_confirmed_doo_cash.longValue());
//		result.put("available_for_my_choice_redemption", ""+available_for_my_choice_redemption.longValue());
//		result.put("available_for_instant_redemption", "0");
//		Long lockMoney = dao.getWooDoodao().getLockScreenCashOfUser(session, my_user_id,"lock");
//		Long extraMoney = dao.getWooDoodao().getLockScreenCashOfUser(session, my_user_id,"extra");
//
//		//		System.out.println("Lock Money : "+lockMoney);
//		//		System.out.println("Extra Money : "+extraMoney);
//		System.out.println("kojch "+extraMoney.longValue()+lockMoney.longValue());
//		result.put("lock_screen_woo_cash", ""+lockMoney.longValue());
//		result.put("bonus_woo_cash", ""+extraMoney.longValue());
//		result.put("total_woo_cash", ""+(extraMoney.longValue()+lockMoney.longValue()));
//
//		//finding total woo cash
//		total_woo_cash = (extraMoney+lockMoney);
//
//		result.put("total_encash", getTotalEncashed(session, user));
//
//		if(total_confirmed_doo_cash > total_woo_cash)
//		{
//			result.put("total_confirmed", ""+total_woo_cash.longValue());
//			result.put("gap_from_value_back_target", ""+(user.getPhone_value() - total_woo_cash.longValue()));
//		}
//		else
//		{
//			result.put("total_confirmed", ""+total_confirmed_doo_cash.longValue());
//			result.put("gap_from_value_back_target", ""+(user.getPhone_value() - total_confirmed_doo_cash.longValue()));
//		}
//		return result;
//	}
	
	private String getTotalEncashed(Session session, User user) {

		ArrayList<Conversion> doos = dao.getWooDoodao().getUsedDoos(session, user.getUser_id());
		//		System.out.println("Here");
		//		System.out.println(doos);

		Double used=0.0;
		for(Conversion con : doos)
		{
			Double left = con.getUsers_payout() - con.getLeft_points();
			used = used+left;
		}
		//		System.out.println("Used : "+used);
		return ""+used.longValue();
	}

	public String sendNotification(String message, String title) {
		Session session = getSession();
		try 
		{   
			ArrayList<User> allUsers = dao.getall(session, User.class);
			Map<String, String> map = new HashMap<>();
			
			map.put("message", message);
			map.put("type", "general");
			map.put("title", title);

			for(User user : allUsers)
			{
				map.put("userId", ""+user.getUser_id());
				
				Notifications notification = new Notifications();
				notification.setMessage(message);
				notification.setStatus(true);
				notification.setUser_id(user.getUser_id());
				notification.setDatetime(Timings.getCurrentTime());
				dao.insert(session, notification);

				AndroidNotifications.generalNotificationToUser(user, map);

			}
			
			return JSONUtils.getSuccessJson(); 
		} catch (Exception e) {
			throw e;
		}
		finally {
			session.close();
		}
	}

	public String changeUserStatus(Long user_id) {
		Session session = getSession();
		try 
		{   
			User user = (User) dao.getbyId(session, user_id, User.class);
			if(user.getStatus().equals(true))
			{
				user.setStatus(false);
				Map<String, String> map = new HashMap<>();
				
				String message = Constants.ACCOUNT_DEACTIVATION_MESSAGE;
				map.put("userId", ""+user_id);
				map.put("message", message);
				map.put("type", "account_deactivation");
				map.put("title", "Account Deactivated");
				
				Notifications notification = new Notifications();
				notification.setMessage(message);
				notification.setStatus(true);
				notification.setUser_id(user.getUser_id());
				notification.setDatetime(Timings.getCurrentTime());
				dao.insert(session, notification);

				AndroidNotifications.generalNotificationToUser(user, map);
				
				user.setGcm_id(null);
				user.setNstatus(false);
				dao.update(session, user);

			}
			else
			{
				user.setStatus(true);
			}
			dao.update(session, user);
			return JSONUtils.getSuccessJson(user); 
		} catch (Exception e) {
			throw e;
		}
		finally {
			session.close();
		}

	}
	
	public String changeUsernStatus(Long user_id) {
		Session session = getSession();
		try 
		{   
			User user = (User) dao.getbyId(session, user_id, User.class);
			
				user.setNstatus(true);
				
				dao.update(session, user);

			
			return JSONUtils.getSuccessJson(user); 
		} catch (Exception e) {
			throw e;
		}
		finally {
			session.close();
		}

	}

	public String deleteUser(Long user_id) {
		Session session = getSession();
		try
		{   
			User user = (User) dao.getbyId(session, user_id, User.class);
			Map<String, String> map = new HashMap<>();
			
			String message = Constants.ACCOUNT_DELETION_MESSAGE;
			map.put("userId", ""+user_id);
			map.put("message", message);
			map.put("type", "account_deleted");
			map.put("title", "Account Deleted");

			AndroidNotifications.generalNotificationToUser(user, map);
			
			dao.delete(session, user);
			return JSONUtils.getSuccessJson(); 
		} catch (Exception e) {
			throw e;
		}
		finally {
			session.close();
		}

	}

	public String script() {
		Session session = getSession();
		try
		{   

			ArrayList<User> users = dao.getall(session, User.class);
			
			for(User user : users)
			{
				if(user.getAge() == null)
				{
					user.setAge(Timings.getAge(user.getDob()));
					dao.update(session, user);
				}
			}
			
			return JSONUtils.getSuccessJson(); 
		} catch (Exception e) {
			throw e;
		}
		finally {
			session.close();
		}
	}

	public String otpsenderV2(Map<String, Object> request) throws Exception {
		// TODO Auto-generated method stub
		
		Session session = getSession();
		
		try {
			String brand  = (String) request.get("brand");
			String device_id  = (String) request.get("device_id");
			String device_model  = (String) request.get("device_model");
			String mobile_no  = (String) request.get("mobile");
			String os_version  = (String) request.get("os_version");
			String ram  = (String) request.get("ram");
			String screen_height  = (String) request.get("screen_height");
			String screen_width  = (String) request.get("screen_width");
			String source  = (String) request.get("source");
			
			
			if( brand.isEmpty()) {
				return JSONUtils.getFailJson("brand empty");
			}
			if(device_id.isEmpty()) {
				return JSONUtils.getFailJson("device_id empty");
				
			}
			if(device_model.isEmpty()) {

				return JSONUtils.getFailJson("device_model empty");
			}
			if(mobile_no.isEmpty()) {

				return JSONUtils.getFailJson("moblile empty");
				
			}
			if(os_version.isEmpty()) {
				return JSONUtils.getFailJson("os_version empty");
		
			}
			if(ram.isEmpty()) {
				return JSONUtils.getFailJson("ram empty");

			}
			if(screen_height.isEmpty()) {
				return JSONUtils.getFailJson("screen_height empty");

			}
			if(screen_width.isEmpty()) {
				return JSONUtils.getFailJson("screen_width empty");

			}
			if(source.isEmpty()) {
				return JSONUtils.getFailJson("source empty");
			}
			
			if(mobile_no == null || mobile_no.isEmpty()){
				return JSONUtils.getFailJson("Please enter mobile no.");
			}
			
			
			User checkMobile = (User) dao.getbyUniqueColumn(session, "mobile_no", mobile_no, User.class);
			if(checkMobile != null){
				return JSONUtils.getFailJson("Mobile no. already used.");
			}
			
			Map<String, Object> map = new HashMap<>();
			map.put("device_id", device_id);
			map.put("mobile", mobile_no);
			
			OTPInfo old =  (OTPInfo) dao.getbyUniqueColumns(session, map , OTPInfo.class);
		
			String otp = String.valueOf(new Random().nextInt(9000) + 1000);
			String message = "Hi. "+otp+" is your login OTP. \n you are one step away from WooDoo to begin. Cheers! Team WooDoo.";
			OtpEmailSender.verifyPhone(""+mobile_no, message);
			
			OTPInfo info = new OTPInfo(brand, device_id, device_model, mobile_no, os_version, ram, screen_height, screen_width, source,otp, false);
			
			if(old == null)
			{
				dao.insert(session, info);
			}
			else
			{
				old.setOtp(otp);
				old.setIsOtpUsed(false);
				dao.update(session, old);
			}
			
			return JSONUtils.getSuccessJson();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return JSONUtils.getFailJson();
	}

	public String otpVerify(Map<String, Object> request) {
		Session session = getSession();

		try
		{
			String device_id  = (String) request.get("a");	
			String mobileNo = (String) request.get("c");
			String otp  = (String) request.get("b");

			if(device_id == null || device_id.isEmpty()) {
				return JSONUtils.getFailJson("device empty");
			}
			if(otp  == null || otp.isEmpty()) {
				return JSONUtils.getFailJson("otp empty");
			}
			if(mobileNo == null ||mobileNo.isEmpty()) {
				return JSONUtils.getFailJson("mobile no empty");
			}
			
			Map<String, Object> map = new HashMap<>();
			map.put("device_id", device_id);
			map.put("mobile", mobileNo);
			System.out.println(map.toString());
			OTPInfo old =  (OTPInfo) dao.getbyUniqueColumns(session, map , OTPInfo.class);
			
			System.out.println(gson.toJson(old));
			
			if(old == null || ! otp.equals(old.getOtp()))
			{
				System.out.println("Old otp check fail.");
				return JSONUtils.getFailJson("Otp Wrong.");
			}
			if(old.getIsOtpUsed().equals(false) && otp.equals(old.getOtp()))
			{
				old.setIsOtpUsed(true);
				dao.update(session, old);
				return JSONUtils.getSuccessJson();
			}
			
			return JSONUtils.getFailWithReasonJson("Your phone is already registered. Please mail us contact@woodoomobility.com");

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally {
			session.close();
		}	
		System.out.println("Returned from last.");
		return JSONUtils.getFailJson();
		
	}

	public String appVersion(String version) {

		Map<String, Object> map = new HashMap<>();
		
		try{
			System.out.println("Current version : "+version);
			
			Double v = Double.parseDouble(version);
			
			if(v.compareTo(1.6) < 0){
				map.put("isForceUpdate", false);
			}
			else{
				map.put("isForceUpdate", false);
			}
			
			
		}
		catch (Exception e) {
			map.put("isForceUpdate", false);
			e.printStackTrace();
		}
		
		map.put("currentVersion", "");
		map.put("message", "Update now for a Super Easy Unlock Button & Improved Experience!");
		map.put("forced_version", "11");
		return JSONUtils.getSuccessJson(map);
	}
	
	
	
}
