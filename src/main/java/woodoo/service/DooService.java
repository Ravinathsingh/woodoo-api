package woodoo.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

import woodoo.configuration.AppConfig;
import woodoo.dao.DaoFactory;
import woodoo.enums.WooStatus;
import woodoo.model.Conversion;
import woodoo.model.Lsimpression;
import woodoo.model.Lsimpressionnew;
import woodoo.model.ManualAdds;
import woodoo.model.NotificationW;
import woodoo.model.Notifications;
import woodoo.model.Order;
import woodoo.model.Shopping_Action_Url;
import woodoo.model.Shopping_url;
import woodoo.model.User;
import woodoo.model.WooLog;
import woodoo.model.WooLogTemp;
import woodoo.model.WooPromotions;
import woodoo.network.ConversionApiCaller;
import woodoo.network.CouponCaller;
import woodoo.network.EmailSender;
import woodoo.network.HttpCaller;
import woodoo.network.NewsApiCaller;
import woodoo.network.notification.AndroidNotifications;
import woodoo.network.notification.sender.Content;
import woodoo.network.notification.sender.PostGCM;
import woodoo.utils.AwsS3Utils;
import woodoo.utils.Constants;
import woodoo.utils.JSONUtils;
import woodoo.utils.Timings;
import woodoo.views.ConversionView;
import woodoo.views.FlipkartOutput;
import woodoo.views.FlipkartView;
import woodoo.views.NewsView;
import woodoo.views.OffersJSONView;
import woodoo.views.OffersView;
import woodoo.views.ShoppingDooView;

@Service
public class DooService extends AbstractService{

	@Autowired
	public DooService(DaoFactory dao, AppConfig config) {
		super(dao, config);
	}
	
	public ArrayList<Shopping_url> getShopigData() throws Exception {
		Session session = getSession();
		try
		{
			
				ArrayList<Shopping_url> add = (ArrayList<Shopping_url>) dao.getall(session, Shopping_url.class);
				
				return add;
			
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}	
	}
	public String getData() throws Exception {
		Session session = getSession();
		try
		{
			
				ArrayList<Shopping_url> add = (ArrayList<Shopping_url>) dao.getall(session, Shopping_url.class);
				
				return JSONUtils.getSuccessJson(add);
			
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}	
	}
	public String getpaytmpendingData() throws Exception {
		Session session = getSession();
		try
		{
			Criteria c1 = session.createCriteria(Order.class);
			c1.add(Restrictions.eq("type", "PENDING"));
			ArrayList<Order> add = (ArrayList<Order>) c1.list();
				
				return JSONUtils.getSuccessJson(add);
			
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}	
	}
	public String getpaytmconfirmData() throws Exception {
		Session session = getSession();
		try
		{
			
			Criteria c1 = session.createCriteria(Order.class);
			c1.add(Restrictions.eq("type", "CONFIRM"));			
			ArrayList<Order> add = (ArrayList<Order>) c1.list();				
				return JSONUtils.getSuccessJson(add);
			
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}	
	}
	
	public String updatestatuspaytm(String id) throws Exception {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		formatter.setTimeZone(TimeZone.getTimeZone("IST"));		 	  
		String strDate = formatter.format(date);
		Session session = getSession();
		String[] temp=id.split(",");
		
		try
		{
			for(int i=0;i<temp.length;i++)
			{
			Order no = (Order) dao.getbyId(session, Integer.parseInt(temp[i]), Order.class);
			
				no.setType("CONFIRM");
				no.setShippingAddressLine1(strDate);
			dao.update(session, no);			
			}
			return JSONUtils.getSuccessJson("success");
			
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		
		finally {
			session.close();
		}	
	}
	
	public String InsertData(String url) throws Exception {
		Session session = getSession();
		try
		{
			Shopping_url obj=new Shopping_url();
			obj.setUrl(url);
			dao.insert(session, obj);				
			return JSONUtils.getSuccessJson(obj);
			
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}	
	}
	public String DeleteData(int id) throws Exception {
		Session session = getSession();
		try
		{
			
			Shopping_url obj=new Shopping_url();
			obj.setUrl_id(id);
			dao.delete(session, obj);				
			return JSONUtils.getSuccessJson(obj);
			
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}	
	}
	public ArrayList<Shopping_Action_Url> getShopActionData() throws Exception {
		Session session = getSession();
		try
		{
			
				ArrayList<Shopping_Action_Url> add = (ArrayList<Shopping_Action_Url>) dao.getall(session, Shopping_Action_Url.class);
				
				return add;
			
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}	
	}
	public String getActionData() throws Exception {
		Session session = getSession();
		try
		{
			
				ArrayList<Shopping_Action_Url> add = (ArrayList<Shopping_Action_Url>) dao.getall(session, Shopping_Action_Url.class);
				
				return JSONUtils.getSuccessJson(add);
			
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}	
	}
	
	public String getNotificationData() throws Exception {
		Session session = getSession();
		try
		{
			
				ArrayList<NotificationW> add = (ArrayList<NotificationW>) dao.getall(session, NotificationW.class);
				
				return JSONUtils.getSuccessJson(add);
			
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}	
	}
	
	public String InsertActionData(String url) throws Exception {
		Session session = getSession();
		try
		{
			Shopping_Action_Url obj=new Shopping_Action_Url();
			obj.setUrl(url);
			dao.insert(session, obj);				
			return JSONUtils.getSuccessJson(obj);
			
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}	
	}
	
	public String InsertNotifiData(String id,String title, String startDate, String endDate,String time , String message, String addToNotification,String url,String type,String ids) throws Exception {
		Session session = getSession();
		try
		{
			if(isInteger(id))
			{
				NotificationW obj = (NotificationW) dao.getbyId(session, Integer.parseInt(id), NotificationW.class);
				obj.setTitle(title);
				obj.setStartDate(startDate);
				obj.setEndDate(endDate);
				obj.setTime(time);
				obj.setMessage(message);
				obj.setStatus(Boolean.valueOf(addToNotification));
				obj.setUrl(url);
				Date date = new Date();
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				String strDate = formatter.format(date);
				obj.setCreationTime(strDate);
				obj.setType(type);
				obj.setIds(ids);
				dao.update(session, obj);				
				return JSONUtils.getSuccessJson(obj);
			}else
			{
			
			NotificationW obj=new NotificationW();
			obj.setTitle(title);
			obj.setStartDate(startDate);
			obj.setEndDate(endDate);
			obj.setTime(time);
			obj.setUrl(url);
			obj.setMessage(message);
			obj.setStatus(Boolean.valueOf(addToNotification));
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			String strDate = formatter.format(date);
			obj.setCreationTime(strDate);
			obj.setType(type);
			obj.setIds(ids);
			dao.insert(session, obj);				
			return JSONUtils.getSuccessJson(obj);
			}
			
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}	
	}
	public String SendNotifiData(String title,String type,String url,String userid,String message) throws Exception {
		Session session = getSession();
		try
		{
			userid=userid.replace(" ", "");
			String[] ids=userid.split(",");
			List<String> gcm_ids = new ArrayList<>();
			for (int i = 0; i < ids.length; i++) {
				String string = ids[i];
				
			
			if(isInteger(string))
			{
			   User Users = (User) dao.getbyId(session,Integer.parseInt(string), User.class);
			   gcm_ids.add(Users.getGcm_id());
				
					//System.out.println("chanadn kumar"+string);
				
			}else
			{			
					
			return JSONUtils.getSuccessJson("Userid should be integer!");
			}
			}
			Map<String, String> map = new HashMap<>();
			
			map.put("message", message);
			map.put("type", type);
			map.put("title", title);
			map.put("url", url);
		
			
				
				/*Notifications notification = new Notifications();
				notification.setMessage(message);
				notification.setStatus(true);
				notification.setUser_id(Long.parseLong(string));
				notification.setDatetime(Timings.getCurrentTime());
				dao.insert(session, notification);*/

			if(gcm_ids.size()<999) {
				AndroidNotifications.generalNotificationToUsers(gcm_ids, map);
				
			}else
			{
				int j=gcm_ids.size()/1000;
				for (int i=0;i<j;i++)
				{
					if((1000*i)+1000<gcm_ids.size())
					{
					List<String> sublist = gcm_ids.subList(1000*i, (1000*i)+1000);
					AndroidNotifications.generalNotificationToUsers(sublist, map);
					}else
					{
						List<String> sublist = gcm_ids.subList(1000*i, gcm_ids.size());
						AndroidNotifications.generalNotificationToUsers(sublist, map);
					}
				}
			}

			
		}
		catch (Exception e){
			e.printStackTrace();
			
			throw new Exception();
		}
		finally {
			session.close();
		}
		return JSONUtils.getSuccessJson();	
	}
	public static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    } catch(NullPointerException e) {
	        return false;
	    }
	    // only got here if we didn't return false
	    return true;
	}
	
	public void sendNotificationAdmin() {
		Session session = getSession();
		try 
		{   
			//ArrayList<User> allUsers = dao.getall(session, User.class);
			
			/*ArrayList<NotificationW> add = (ArrayList<NotificationW>) dao.getall(session, NotificationW.class);
			for(NotificationW noti : add)
			{
			Map<String, String> map = new HashMap<>();
			map.put("message", noti.getMessage());
			map.put("type", "setting");
			map.put("title", noti.getTitle());
			map.put("url", noti.getUrl());
            if(istrue(noti.getStartDate(),noti.getEndDate(),noti.getTime())&& noti.getStatus()==true)
            {
            	
			//for(User user : allUsers)
			//{
            User user=(User) dao.getbyId(session, 9106, User.class);	
			map.put("userId", ""+user.getUser_id());
			
            */
			Map<String, String> map1 = new HashMap<>();
			
			map1.put("message", "Rise & Shine! Check settings in WooDoo to ensure that it is \"ON\" for earning Woo Cash.");
			map1.put("type", "general");
			map1.put("title", "WooDoo");
			Criteria c = session.createCriteria(User.class);
			c.add(Restrictions.eq("nstatus", false));
			ArrayList<User> allUsers = (ArrayList<User>) c.list();
			//User user=(User) dao.getbyId(session, 9106, User.class);
			for(User user : allUsers)
			{
			List<String> gcm_ids = new ArrayList<>();
			gcm_ids.add(user.getGcm_id());
			Content content = new Content(gcm_ids, map1);			
			String a=PostGCM.post2(Constants.FCM_API_KEY, content);
			if(a.equalsIgnoreCase("1"))
			{
				user.setNstatus(true);
				dao.update(session, user);
			}
			}
			//AndroidNotifications.generalNotificationToUser(user, map);
            


//}
			//}
			//}
			
		} catch (Exception e) {
			try {
				throw e;
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		finally {
			session.close();
		}
	}
   public boolean istrue(String statdate,String enddate,String time)
   {
	   try {
	   Date date = new Date();
	   DateTime dt = new DateTime();
	   int hours = dt.getHourOfDay();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = formatter.format(date);
	   SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date date1 = sdf.parse(statdate);       
		Date date2 = sdf.parse(enddate);
		Date date3 = sdf.parse(strDate);
		if(date3.compareTo(date1) >= 0 && date2.compareTo(date3) >= 0)
		{
			if(time.contains(""+hours))
			{
				return true;
			}
		}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return false;
	}

	   return false;
   }
	
	public String DeleteActionData(int id) throws Exception {
		Session session = getSession();
		try
		{
			
			Shopping_Action_Url obj=new Shopping_Action_Url();
			obj.setId(id);
			dao.delete(session, obj);				
			return JSONUtils.getSuccessJson(obj);
			
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}	
	}

	public String DeleteNotifiData(int id) throws Exception {
		Session session = getSession();
		try
		{
			
			NotificationW obj=new NotificationW();
			obj.setId((long) id);
			dao.delete(session, obj);				
			return JSONUtils.getSuccessJson(obj);
			
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}	
	}

	public String updatestatusnoData(int id) throws Exception {
		Session session = getSession();
		try
		{
			NotificationW no = (NotificationW) dao.getbyId(session, id, NotificationW.class);
			if(no.getStatus())
			{
			no.setStatus(false);
			}else
			{
				no.setStatus(true);
			}
			dao.update(session, no);				
			return JSONUtils.getSuccessJson(no);
			
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}	
	}


	public String getDooData(String my_user_id, Integer current_page_no, Integer maxResults) throws Exception {
		Session session = getSession();
		try
		{
			Boolean decision = false;

			int startIndex = 0;

//			if(current_page_no == 0)
//				startIndex = ((maxResults * current_page_no));
//			else
//				startIndex = ((maxResults * current_page_no)+1);

			Boolean isMoreLeft = false;
			int last = 0;

			if(Constants.shoppingOffers == null)
			{
				Constants.shoppingOffers = CouponCaller.shopingApiCaller();
			}
			//No data left 
			
			if(startIndex > Constants.shoppingOffers.size())
			{
				JSONObject json = new JSONObject();
				json.put("status", "success");
				json.put("is_more_data_left", isMoreLeft);
				json.put("data", new JSONArray(gson.toJson(new ArrayList<>())));
				json.put("current_page_no", current_page_no+1);
				return json.toString();
			}

			//setting isMoreDataAvailable
//			if(Constants.shoppingOffers.size() > (startIndex+maxResults)){
//				isMoreLeft = true;
//			}

			ArrayList<ShoppingDooView> views = new ArrayList<>();
			last = startIndex+maxResults;

//			if(last > Constants.shoppingOffers.size())
				last = Constants.shoppingOffers.size();

			for(int i = startIndex ; i < last ; i++){
				
				/*if(Constants.shoppingOffers.get(i).getName().contains("Wadi.com") ||
						Constants.shoppingOffers.get(i).getName().contains("Aliexpress.com") ||
						Constants.shoppingOffers.get(i).getName().contains("Almosafer.com") ||
						Constants.shoppingOffers.get(i).getName().contains("Jumia.com") ||
						Constants.shoppingOffers.get(i).getName().contains("Rehlat.com") ||
						Constants.shoppingOffers.get(i).getName().contains("Souq.com") ||
						Constants.shoppingOffers.get(i).getName().contains("Tajawal.com") ||
						Constants.shoppingOffers.get(i).getName().contains("1and1.com") ||
						Constants.shoppingOffers.get(i).getName().contains("Dresslily.com") ||
						Constants.shoppingOffers.get(i).getName().contains("Gamiss.com") ||
						Constants.shoppingOffers.get(i).getName().contains("Liquidweb.com") ||
						Constants.shoppingOffers.get(i).getName().contains("Rosegal.com") ||
						Constants.shoppingOffers.get(i).getName().contains("SheIn.com") ||
						Constants.shoppingOffers.get(i).getName().contains("Gearbest.com") ||
						Constants.shoppingOffers.get(i).getName().contains("Zaful.com") ||
						Constants.shoppingOffers.get(i).getName().contains("Gap.ae")	||
						Constants.shoppingOffers.get(i).getName().contains("Ounass.com")	||
						Constants.shoppingOffers.get(i).getName().contains("Nass.com")	||
						Constants.shoppingOffers.get(i).getName().contains("Park")	||
						Constants.shoppingOffers.get(i).getName().contains("Radisson")	||
						Constants.shoppingOffers.get(i).getName().contains("Hostinger.in")	||						
						Constants.shoppingOffers.get(i).getName().contains("Mamasandpapas.com.sa")	||
						Constants.shoppingOffers.get(i).getName().contains("WebHostingPad.com")||						
						Constants.shoppingOffers.get(i).getName().contains("Avenue.com")||
						Constants.shoppingOffers.get(i).getName().contains("Boscovs.com")||
						Constants.shoppingOffers.get(i).getName().contains("DHgate.com")||
						Constants.shoppingOffers.get(i).getName().contains("Depositphotos.com")||
						Constants.shoppingOffers.get(i).getName().contains("FatCow.com")||
						Constants.shoppingOffers.get(i).getName().contains("Fiverr.com")||					
						Constants.shoppingOffers.get(i).getName().contains("HeavenGifts.com")||
						Constants.shoppingOffers.get(i).getName().contains("InMotionHosting.com")||
						Constants.shoppingOffers.get(i).getName().contains("Ipage.com")||
						Constants.shoppingOffers.get(i).getName().contains("Kiwi.com")||
						Constants.shoppingOffers.get(i).getName().contains("LivingSocial.com")||
						Constants.shoppingOffers.get(i).getName().contains("LunarPages.com")||
						Constants.shoppingOffers.get(i).getName().contains("TurnkeyInternet.net")||
						Constants.shoppingOffers.get(i).getName().contains("Worldmarket.com")	||
						Constants.shoppingOffers.get(i).getName().contains("Autoanything.com")	||
						Constants.shoppingOffers.get(i).getName().contains("Barnes&Noble.com")	||
						Constants.shoppingOffers.get(i).getName().contains("Billabong.com")	||
						Constants.shoppingOffers.get(i).getName().contains("Burkededecor.com")||
						Constants.shoppingOffers.get(i).getName().contains("Framesdirect.com")||
						Constants.shoppingOffers.get(i).getName().contains("NordVPN.net")||
						Constants.shoppingOffers.get(i).getName().contains("Officedepot.com")||
						Constants.shoppingOffers.get(i).getName().contains("Overland.com")||
						Constants.shoppingOffers.get(i).getName().contains("Sonos.com")||
						Constants.shoppingOffers.get(i).getName().contains("TigerDirect.com")||
						Constants.shoppingOffers.get(i).getName().contains("Woot.com "))
				{
					
				}else
				{
					views.add(couponToShoppingView(my_user_id,Constants.shoppingOffers.get(i)));
				}*/
				
               if(!containsAKeyword(Constants.shoppingOffers.get(i).getName(),getShopigData()))
               {
            	   views.add(couponToShoppingView(my_user_id,Constants.shoppingOffers.get(i)));
               }
				
			}

			JSONObject json = new JSONObject();
			json.put("status", "success");
			json.put("is_more_data_left", isMoreLeft);
			json.put("data", new JSONArray(gson.toJson(views)));
			json.put("current_page_no", current_page_no+1);

			System.out.println("response : "+json.toString());

			return json.toString();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String actionDooListData(Long my_user_id, Integer current_page_no, Integer maxResults) throws Exception {
		Session session = getSession();
		try
		{
			Boolean decision = false;
			int startIndex = 0;
//
//			if(current_page_no == 0)
//				startIndex = ((maxResults * current_page_no));
//			else
//				startIndex = ((maxResults * current_page_no)+1);

			Boolean isMoreLeft = false;
			int last = 0;

			if(Constants.offers == null)
			{
				Constants.offers = CouponCaller.actionApiCaller();
			}

			//No data left 
			if(startIndex > Constants.offers.size())
			{
				JSONObject json = new JSONObject();
				json.put("status", "success");
				json.put("is_more_data_left", isMoreLeft);
				json.put("data", new JSONArray(gson.toJson(new ArrayList<>())));
				json.put("current_page_no", current_page_no+1);
				return json.toString();
			}

			//setting isMoreDataAvailable
//			if(Constants.offers.size() > (startIndex+maxResults))
//			{
//				isMoreLeft = true;
//			}

			ArrayList<OffersJSONView> views = new ArrayList<>();
			last = startIndex+maxResults;
			//			System.out.println(last);
//			if(last > Constants.offers.size())
				last = Constants.offers.size();

//			for(int i = startIndex ; i < last ; i++)
//			{
//				views.add(actionToShoppingView(Constants.offers.get(i), my_user_id));
//			} 
//			Spuul
			for(int i = startIndex ; i < last ; i++)
			{
				/*if(
						Constants.offers.get(i).getName().contains("Vodafone") ||
						Constants.offers.get(i).getName().contains("Idea") ||
						Constants.offers.get(i).getName().contains("Games") ||									
						Constants.offers.get(i).getName().contains("Video") ||
						Constants.offers.get(i).getName().contains("Airtel") ||
						Constants.offers.get(i).getName().contains("Academy") ||
						Constants.offers.get(i).getName().contains("Kindzone") ||
						Constants.offers.get(i).getName().contains("FunZone") ||						
						Constants.offers.get(i).getName().contains("Zomato.com") ||
						Constants.offers.get(i).getName().contains("Shotformats") ||
						Constants.offers.get(i).getName().contains("Nnnow.com") ||
						Constants.offers.get(i).getName().contains("Model")||
						Constants.offers.get(i).getName().contains("MrVegasCasino-") ||
						Constants.offers.get(i).getName().contains("GR") ||
						Constants.offers.get(i).getName().contains("Glam") ||
						Constants.offers.get(i).getName().contains("vCommission") ||
						Constants.offers.get(i).getName().contains("Hot Chillies") ||
						Constants.offers.get(i).getName().contains("Eros Now") ||
						Constants.offers.get(i).getName().contains("Gogo Girls") ||
						Constants.offers.get(i).getName().contains("Cut The Rope") ||
						Constants.offers.get(i).getName().contains("RacingChallenge") ||
					Constants.offers.get(i).getName().contains("All In One") ||
					Constants.offers.get(i).getName().contains("Youth Junction") ||
					Constants.offers.get(i).getName().contains("FlashContact")||
					Constants.offers.get(i).getName().contains("Asian") ||
					Constants.offers.get(i).getName().contains("Cute") ||
					Constants.offers.get(i).getName().contains("SlotsDeck.com")
					
		
			
			
						){
					
				}else
				{
					views.add(actionToShoppingView(Constants.offers.get(i), my_user_id));
				}*/
				
				if(!containsAKeyword2(Constants.offers.get(i).getName(),getShopActionData()))
	               {
					views.add(actionToShoppingView(Constants.offers.get(i), my_user_id));
	               }
			}
			
//			Collections.sort(views, Collections.reverseOrder(OffersJSONView.compareByPayout));   

			//			System.out.println("\n\n Offers "+views);
			JSONObject json = new JSONObject();
			json.put("status", "success");
			json.put("is_more_data_left", isMoreLeft); 
			json.put("data", new JSONArray(gson.toJson(views)));
			json.put("current_page_no", current_page_no+1);

			System.out.println("Response : "+json.toString());

			return json.toString();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}
	
	public boolean containsAKeyword(String myString, ArrayList<Shopping_url> keywords){
		   for(Shopping_url keyword : keywords){
		      if(myString.contains(keyword.getUrl())){
		         return true;
		      }
		   }
		   return false; // Never found match.
		}
	public boolean containsAKeyword2(String myString, ArrayList<Shopping_Action_Url> keywords){
		   for(Shopping_Action_Url keyword : keywords){
		      if(myString.contains(keyword.getUrl())){
		         return true;
		      }
		   }
		   return false; // Never found match.
		}

	private OffersJSONView actionToShoppingView(OffersView offersView, Long my_user_id) {
		OffersJSONView view = new OffersJSONView();
		String name = offersView.getName().split(" ")[0];
		view.setDescription(name+": "+offersView.getDescription());
		String payout = offersView.getDefault_payout();

		view.setName(name); 
		
		view.setSpecial_instructions(getConversionFlow(offersView.getDescription()));
		
		if(view.getName().equalsIgnoreCase("Jabong.com"))
		{
			view.setSpecial_instructions(view.getSpecial_instructions().replace("<br><br>\r\n\r\n", "<br>\r\n")+
					"3. Doo Cash will be attributed only if mobile web/browser is used for transaction and NOT the app.<br><br>");
		}
		
		view.setConversion_flow(getConversionFlow(offersView.getDescription()));
		DecimalFormat format = new DecimalFormat("##0.0");
		System.out.println(offersView.getPayout_type()+"  "+offersView.getName()+"  id"+offersView.getId());
		
		if(offersView.getPayout_type().equalsIgnoreCase("cpa_flat") || offersView.getPayout_type().equalsIgnoreCase("cpc"))
		{
			Double pay = offersView.getPay();
			view.setIntPayout(Double.parseDouble(format.format(pay)));
			System.out.println(format.format(pay));
			view.setPayout("DOO & EARN upto "+pay.longValue()+" DOO CASH");
		}
		else
		{
			Double pay =  offersView.getPay();
			view.setIntPayout(Double.parseDouble(format.format(pay)));
			System.out.println(format.format(pay));
			view.setPayout("DOO & EARN upto "+pay.longValue()+"% DOO CASH");
		}

		String link = Constants.TRACKINGURL+"offer_id="+offersView.getId()+"&aff_id=57178&aff_sub="+my_user_id+"&aff_sub2="+Timings.getCurrentTime();
		//		System.out.println("Link genrated : "+link);
		view.setLink(link);
		view.setThumbnail(offersView.getThumbnail());
		view.setOur_instructions(Constants.INSTRUCTIONS_URL_ACTION);
		return view;
	}

	private ShoppingDooView couponToShoppingView(String my_user_id, OffersView offersView) 
	{
		ShoppingDooView view = new ShoppingDooView();
		String offerName = offersView.getName().split(" ")[0];
		view.setName(offerName);

		String link = Constants.TRACKINGURL+"offer_id="+offersView.getId()+"&aff_id=57178&aff_sub="+my_user_id+"&aff_sub2="+Timings.getCurrentTime();
		view.setLink(link);
		DecimalFormat format = new DecimalFormat("##0");
//		System.out.println(offersView);
		if(offersView.getPayout_type().equalsIgnoreCase("cpa_flat"))
		{
			//Double pay = offersView.getPay() *0.8;
			view.setPayout("DOO & EARN upto "+String.valueOf(offersView.getPay()).split("\\.")[0]+" DOO CASH");
		}
		else
		{
			//Double pay = (offersView.getPay() *0.8); 
			view.setPayout("DOO & EARN upto "+String.valueOf(offersView.getPay()).split("\\.")[0]+"% DOO CASH");
		}
		view.setThumbnail(offersView.getThumbnail());
		view.setDescription(offersView.getDescription());
		view.setSpecial_instructions(getConversionFlow(offersView.getDescription()));
		
		if(view.getName().equalsIgnoreCase("Jabong.com"))
		{
			view.setSpecial_instructions(view.getSpecial_instructions().replace("<br><br>\r\n\r\n", "<br>\r\n")+
					"3. Doo Cash will be attributed only if mobile web/browser is used for transaction and NOT the app.<br><br>");
		}
		
		if(view.getName().equalsIgnoreCase("Voonik.com"))
		{
			view.setSpecial_instructions(view.getSpecial_instructions().replace("<br><br>\r\n\r\n", "<br>\r\n")
					+"4. Doo Cash will be attributed only if mobile web/browser is used for transaction and NOT the app.<br><br>");
		}
		
		view.setOur_instructions(Constants.INSTRUCTIONS_URL_SHOPPING);
		return view;
	}

	public String lockScreenData(Long my_user_id, Integer espnNextIndex, Integer netGeoNextIndex, Integer toiNextIndex, Integer flipkartNextIndex) throws Exception {
		String actionDooData = actionDooListData(my_user_id, 0, 60);
		JSONObject dooData = new JSONObject(actionDooData);
		System.out.println(dooData.toString());
//				JSONArray dooArray = new JSONArray(gson.toJson(dooData.get("data")));
//				System.out.println("final data : "+dooArray.toString());

		String data =  gson.toJson(dooData.get("data")).toString();
		JSONObject result = new JSONObject();
		result.put("action_do_data", dooData.get("data"));

		Session session = getSession();
		try
		{
			if(Constants.APICALLTIME == null)
				Constants.APICALLTIME = Timings.getCurrentTime();

			if(Constants.espnNews == null)
				Constants.espnNews = NewsApiCaller.newsApiCallerESPN();
			if(Constants.ngNews == null)
				Constants.ngNews  = NewsApiCaller.newsApiCallerNGC();
			if(Constants.toiNews == null)
				Constants.toiNews = NewsApiCaller.newsApiCallerTOI();

			//			int random = randamNoAccordingToArraySize(Constants.ngNews.size());
			ManualAdds screenTwo = (ManualAdds) dao.getbyUniqueColumn(session, "screen_no", 2, ManualAdds.class);
			ManualAdds screenThree = (ManualAdds) dao.getbyUniqueColumn(session, "screen_no", 3, ManualAdds.class);
			ManualAdds screenFour = (ManualAdds) dao.getbyUniqueColumn(session, "screen_no", 4, ManualAdds.class);
			ManualAdds screenSix = (ManualAdds) dao.getbyUniqueColumn(session, "screen_no", 6, ManualAdds.class);
			
			NewsView espn = null;
			int espnIndex = 0;
			if(screenSix != null && screenSix.getStatus().equals(true) && checkValidity(screenSix.getEndDate(),screenSix.getStartDate()).equals(true) )
			{
				espn = new NewsView();
				if(screenSix.getDescription() != null)
					espn.setDescription(screenSix.getDescription());
				
				if(screenSix.getTitle() != null)
					espn.setTitle(screenSix.getTitle());
				
				if(screenSix.getUrlToImage() != null)
					espn.setUrlToImage(screenSix.getUrlToImage());
				
				if(screenSix.getUrl() != null)
					espn.setUrl(screenSix.getUrl());
				
			espn.setIs_manual(true);
			}
			else
			{
				ArrayList<NewsView> espnNews1 =new ArrayList<>();
				for(int i=espnNextIndex;i<Constants.espnNews.size();i++)
				{
					if(espnNews1.size()<4)
					{
					espn = Constants.espnNews.get(i);					
					espn.setIs_manual(false);
					espnNews1.add(espn);
					}
				}
				//espnIndex = getNextIndex(espnNextIndex, Constants.espnNews.size());
				
				result.put("espn_news", new JSONArray(gson.toJson(espnNews1)));
			}
			
			NewsView ngNews1 = null;
			int netIndex = 0;
			
			if(screenFour != null && screenFour.getStatus().equals(true) && checkValidity(screenFour.getEndDate(), screenFour.getStartDate()).equals(true) )
			{
				ngNews1 = new NewsView();
				if(screenFour.getDescription() != null)
					ngNews1.setDescription(screenFour.getDescription());
				
				if(screenFour.getTitle() != null)
					ngNews1.setTitle(screenFour.getTitle());
				
				if(screenFour.getUrlToImage() != null)
					ngNews1.setUrlToImage(screenFour.getUrlToImage());
				
				if(screenFour.getUrl() != null)
					ngNews1.setUrl(screenFour.getUrl());
				
				ngNews1.setIs_manual(true);
			}
			else
			{
				/*netIndex = getNextIndex(netGeoNextIndex, Constants.ngNews.size());
				ngNews = Constants.ngNews.get(netIndex);
				ngNews.setIs_manual(false);*/
				ArrayList<NewsView> ngNewse =new ArrayList<>();
				for(int i=netGeoNextIndex;i<Constants.ngNews.size();i++)
				{
					if(ngNewse.size()<4)
					{
						ngNews1 = Constants.ngNews.get(i);
						ngNews1.setIs_manual(false);
						ngNewse.add(ngNews1);
					}
				}
				result.put("national_geographic", new JSONArray(gson.toJson(ngNewse)));
			}
			
			NewsView toiNews1 = null;
			int toiIndex = 0;
			if(screenTwo != null && screenTwo.getStatus().equals(true) && checkValidity(screenTwo.getEndDate(),screenTwo.getStartDate()).equals(true) )
			{
				toiNews1 = new NewsView();
				if(screenTwo.getDescription() != null)
					toiNews1.setDescription(screenTwo.getDescription());
				
				if(screenTwo.getTitle() != null)
					toiNews1.setTitle(screenTwo.getTitle());
				
				if(screenTwo.getUrlToImage() != null)
					toiNews1.setUrlToImage(screenTwo.getUrlToImage());
				
				if(screenTwo.getUrl() != null)
					toiNews1.setUrl(screenTwo.getUrl());
				
				toiNews1.setIs_manual(true);
			}
			else
			{
				/*toiIndex = getNextIndex(toiNextIndex, Constants.toiNews.size());
				toiNews = Constants.toiNews.get(toiIndex);
				toiNews.setIs_manual(false);*/
				ArrayList<NewsView> toiNewse =new ArrayList<>();
				for(int i=toiNextIndex;i<Constants.toiNews.size();i++)
				{
					if(toiNewse.size()<4)
					{
						toiNews1 = Constants.toiNews.get(i);
						toiNews1.setIs_manual(false);
						toiNewse.add(toiNews1);
					}
				}
				result.put("times_of_india", new JSONArray(gson.toJson(toiNewse)));
			}
			
			/*JSONObject e = new JSONObject(gson.toJson(espn));
			e.put("nextIndex", espnIndex);
			result.put("espn_news", e);*/
			
			/*JSONObject n = new JSONObject(gson.toJson(ngNews));
			result.put("national_geographic", n);
			n.put("nextIndex", netIndex);
			
			JSONObject t = new JSONObject(gson.toJson(toiNews));
			t.put("nextIndex", toiIndex);
			
			result.put("times_of_india", t);*/
			if((espnNextIndex+4)<Constants.espnNews.size())
			{
			result.put("espnNextIndex", espnNextIndex+4);
			}else
			{
				result.put("espnNextIndex", 0);
			}
			if((netGeoNextIndex+4)<Constants.ngNews.size())
			{
			result.put("netGeoNextIndex", netGeoNextIndex+4);
			}else
			{
				result.put("netGeoNextIndex", 0);
			}
			if((toiNextIndex+4)<Constants.toiNews.size())
			{
			result.put("toiNextIndex", toiNextIndex+4);
			}else
			{
				result.put("toiNextIndex", 0);
			}
			result.put("status", "success");
			// Flipkart api works
			if(Constants.flipkartData == null)
			{
				System.out.println("Constants.flipkartData is empty!!!");
				Constants.flipkartData = CouponCaller.campaignApiCaller();
			}
			int seed = Constants.flipkartData.size();
			if(seed < 0) {
				return JSONUtils.getFailJson("No News to show");
			}

			int randomNum;
			int minimum = 0;
			int maximum = seed;

			Random rand = new Random();

			int one =  minimum + rand.nextInt((maximum - minimum) + 1);

			int two =  minimum + rand.nextInt((maximum - minimum) + 1);
			//			System.out.println("one : "+one+" two "+two);
 
			ArrayList<FlipkartOutput> output = new ArrayList<>();
			int flipkartIndex = 0;
			if(screenThree != null && screenThree.getStatus().equals(true) && checkValidity(screenThree.getEndDate(),screenThree.getStartDate()).equals(true) )
			{
				FlipkartOutput o = new FlipkartOutput();
				o.setUrlToImage(screenThree.getUrlToImage());
				o.setUrl(screenThree.getUrl());
				o.setIs_manual(true);
				
				if(screenThree.getDescription() != null)
					o.setDescription(screenThree.getDescription());
				if(screenThree.getTitle() != null)
					o.setTitle(screenThree.getTitle());
				output.add(o);
			}
			else
			{
				if(Constants.flipkartData != null && Constants.flipkartData.size() > 0 ) 
				{
					flipkartIndex = getNextIndex(flipkartNextIndex, Constants.flipkartData.size());
					FlipkartView v1 = Constants.flipkartData.get(flipkartIndex);
					
					FlipkartOutput o = new FlipkartOutput();
					o.setUrlToImage(v1.getUrl());
					String TR1 = Constants.TRACKINGURL+"offer_id="+v1.getOffer_id()+"&aff_id=57178&aff_sub="+my_user_id+"&aff_sub2="+Timings.getCurrentTime()+"&file_id="+v1.getId();
					o.setUrl(TR1);
					o.setIs_manual(false);
					o.setNextIndex(flipkartIndex);
					
					System.out.println("Flipkart data : "+v1);
					
					output.add(o);   
				}
				if(output.size() == 0)
				{
					FlipkartOutput o = new FlipkartOutput();
					o.setUrlToImage("http://media.vcommission.com/brand/files/vcm/412/FK.300x250.jpg");
					String TR1 = "http://tracking.vcommission.com/aff_c?offer_id=412&aff_id=57178&aff_sub="+my_user_id+"aff_sub2="+Timings.getCurrentTime()+"&file_id=31288";
					o.setUrl(TR1);
					o.setIs_manual(false);
					System.out.println("Flipkart data  is empty : "+TR1);
			
					output.add(o);   
				}
				
			}		
			
			
//			if(Constants.flipkartData != null && Constants.flipkartData.size() > two) { 
//				FlipkartView v2 = Constants.flipkartData.get(two);
//
//				FlipkartOutput o1 = new FlipkartOutput();
//				String TR2 = Constants.TRACKINGURL+"offer_id="+v2.getOffer_id()+"&aff_id=57178&aff_sub="+my_user_id+"&file_id="+v2.getId();
//				o1.setTrackingUrl(TR2);
//				o1.setThumbnail(v2.getUrl());
//				
//				output.add(o1);
//			}    

			result.put("flipkart", new JSONArray(gson.toJson(output)));

			System.out.println("Response : "+result.toString());

			return result.toString();
		}
		catch (Exception e) { 
			e.printStackTrace();
			throw e;    
		}    
		finally {
			session.close();
		}

	}

	private Boolean checkValidity(Long endDate, Long starttime) {
		
		System.out.println("Current Time : "+Timings.getCurrentTime()+"End Date : "+endDate+" Start Time : "+starttime);
		System.out.println(endDate > Timings.getCurrentTime());
		System.out.println( starttime < Timings.getCurrentTime() );
		System.out.println(endDate-Timings.getCurrentTime());
		if(endDate > Timings.getCurrentTime() && starttime < Timings.getCurrentTime() )
		{
			System.out.println(" returning true");
			return true;
		}
		System.out.println(" returning false");
		return false;
	}

	private int getNextIndex(Integer espnNextIndex, int size) {
		if(size == 0)
			return 0;
		if(espnNextIndex == 0) {
			return size-1;
		} 
		else {
			if(size > espnNextIndex)
				return espnNextIndex-1;
			else {
				return size-1; 
			}
		}
		/*if(size <= espnNextIndex)
			return 0;
		int index =  (espnNextIndex+1);
		if(index >= size)
			index=0;*/
	}

	public  static String getConversionFlow(String html)
	{
		//		String html = "Install QuackQuack Android App from Google Play Store.<br><br>\r\n\r\n<strong>Converts on:</strong> Install & Register.<br><br>\r\n\r\n<strong>Conversion Flow:</strong><br>\r\n1. User clicks on Install button in Google Play Store.<br>\r\n2. User signup via Facebook.<br>\r\n3. User select some personal information 1st page and 2nd page of the application.<br>\r\n4. Registration is counted and credited.<br><br>\r\n\r\n<strong>Allowed Media:</strong> Mobile Banners, Text Links, Offer Wall & Incent.<br><br>\r\n\r\n<strong>Disallowed Media:</strong> Email/Newsletter, Facebook PPC, PPC, Social Media & Pop Traffic.<br><br>\r\n\r\n<strong>Special Instructions:</strong> Only for Female.<br>";
		//		String html = "Best deals in Delhi/NCR restaurrants, Spa & Salon, Fitness, Movies, Activities and Travel.<br><br>\r\n\r\n<strong>Converts on:</strong> Sale<br><br>\r\n\r\n<strong>Conversion Flow:</strong><br>\r\n1. User makes any purchase.<br>\r\n2. Sale is counted & credited.<br><br>\r\n\r\n<strong>Tracking Information:</strong><br>\r\nFrequency - Real time<br>\r\nCookie Duration - 7 Days<br>\r\nWeb - <font color=\"green\">Available</font><br>\r\nMobile Web - <font color=\"green\">Available</font><br>\r\nApp - <font color=\"red\">Not Available</font><br>\r\nDeeplink - <font color=\"green\">Available</font> (ONLY through Deeplink Tool <a href=\"http://partners.vcommission.com/publisher/#!/page/25\">here</a>)<br>\r\nMultiple Conversions - <font color=\"green\">Available</font><br><br>\r\n\r\n<strong>Promotion Methods:</strong><br>\r\nText Link - <font color=\"green\">Allowed</font><br>\r\nBanner - <font color=\"green\">Allowed</font><br>\r\nDeals - <font color=\"green\">Allowed</font> (Generic & the ones uploaded <a href=\"http://partners.vcommission.com/publisher/#!/page/6\">here</a>)<br>\r\nCoupons - <font color=\"green\">Allowed</font><br>\r\nCashback - <font color=\"green\"> Allowed</font><br>\r\nEmail (Text) - <font color=\"green\">Allowed</font><br>\r\nCustom Email (Text) - <font color=\"red\">Not Allowed</font><br>\r\nEmail (HTML) - <font color=\"green\">Allowed</font><br>\r\nCustom Email (HTML) - <font color=\"red\">Not Allowed</font><br>\r\nPOP Traffic - <font color=\"red\">Not Allowed</font><br>\r\nNative Ads - <font color=\"red\"> Not Allowed</font><br>\r\nSocial Media - <font color=\"red\">Not Allowed</font><br>\r\nFacebook Ads - <font color=\"red\">Not Allowed</font><br>\r\nSEM - Brand Keyword(s) - <font color=\"red\">Not Allowed</font><br>\r\nSEM - Generic Keyword(s) - <font color=\"green\">Allowed</font><br>\r\nSEM - Brand + Generic Keyword(s) - <font color=\"red\">Not Allowed</font>";
		//		String html = "Best deals in Delhi/NCR restaurrants, Spa & Salon, Fitness, Movies, Activities and Travel.<br><br>\r\n\r\n<strong>Converts on:</strong> Sale<br><br>\r\n\r\n<strong>Conversion Flow:</strong><br>\r\n1. User makes any purchase.<br>\r\n2. Sale is counted & credited.<br><br>\r\n\r\n<strong>Tracking Information:</strong><br>\r\nFrequency - Real time<br>\r\nCookie Duration - 7 Days<br>\r\nWeb - <font color=\"green\">Available</font><br>\r\nMobile Web - <font color=\"green\">Available</font><br>\r\nApp - <font color=\"red\">Not Available</font><br>\r\nDeeplink - <font color=\"green\">Available</font> (ONLY through Deeplink Tool <a href=\"http://partners.vcommission.com/publisher/#!/page/25\">here</a>)<br>\r\nMultiple Conversions - <font color=\"green\">Available</font><br><br>\r\n\r\n<strong>Promotion Methods:</strong><br>\r\nText Link - <font color=\"green\">Allowed</font><br>\r\nBanner - <font color=\"green\">Allowed</font><br>\r\nDeals - <font color=\"green\">Allowed</font> (Generic & the ones uploaded <a href=\"http://partners.vcommission.com/publisher/#!/page/6\">here</a>)<br>\r\nCoupons - <font color=\"green\">Allowed</font><br>\r\nCashback - <font color=\"green\"> Allowed</font><br>\r\nEmail (Text) - <font color=\"green\">Allowed</font><br>\r\nCustom Email (Text) - <font color=\"red\">Not Allowed</font><br>\r\nEmail (HTML) - <font color=\"green\">Allowed</font><br>\r\nCustom Email (HTML) - <font color=\"red\">Not Allowed</font><br>\r\nPOP Traffic - <font color=\"red\">Not Allowed</font><br>\r\nNative Ads - <font color=\"red\"> Not Allowed</font><br>\r\nSocial Media - <font color=\"red\">Not Allowed</font><br>\r\nFacebook Ads - <font color=\"red\">Not Allowed</font><br>\r\nSEM - Brand Keyword(s) - <font color=\"red\">Not Allowed</font><br>\r\nSEM - Generic Keyword(s) - <font color=\"green\">Allowed</font><br>\r\nSEM - Brand + Generic Keyword(s) - <font color=\"red\">Not Allowed</font>";
		//		String html = "Install Amazon Android App from Google Play Store.<br><br>\r\n\r\n<strong>Converts on:</strong> Install & First Launch<br><br>\r\n<strong><u>URL to be used as: http://tracking.vcommission.com/aff_c?offer_id=2866&aff_id={affiliate_id}&aff_sub=SUB<font color=\"red\">&google_aid=PASS_GOOGLE_AID</font>\r\n<br>\r\n<i>Please note: If Google AID is not passed properly, the campaign will not track\r\n</strong></u>\r\n<br><br>\r\n<strong>Conversion Flow:</strong><br>\r\n1. User clicks on Install button in Google Play store<br>\r\n2. User opens the app for the first time<br>\r\n3. Install is counted and credited.<br><br>\r\n\r\n<strong>Allowed Media:</strong> Mobile Banners & Text Links<br><br>\r\n\r\n<strong>Disallowed Media:</strong> Email/Newsletter, Facebook PPC, PPC, Social Media, Pop Traffic,  Incent, Offer-wall, Virtual Currency & Cashback <br><br>\r\n\r\n<strong>Special Instructions:</strong> \r\nIt’s mandatory to pass the GAID – post backs without a GAID can result in invalidation of the install.<br>\r\n\r\nPartners are encouraged to drive two main KPIs: <br>\r\n1) First Sign In <br>\r\n2) First Purchases";		
		try{
		String[] htmlArr = html.split("<strong>Conversion Flow:</strong>");
		String[] arr1 = htmlArr[1].split("<strong>");
		return arr1[0];
		}
		catch(Exception e)
		{
			return "";
		}
	}

	public  static String getSpecialInstructio(String html)
	{
		//		String html = "Install QuackQuack Android App from Google Play Store.<br><br>\r\n\r\n<strong>Converts on:</strong> Install & Register.<br><br>\r\n\r\n<strong>Conversion Flow:</strong><br>\r\n1. User clicks on Install button in Google Play Store.<br>\r\n2. User signup via Facebook.<br>\r\n3. User select some personal information 1st page and 2nd page of the application.<br>\r\n4. Registration is counted and credited.<br><br>\r\n\r\n<strong>Allowed Media:</strong> Mobile Banners, Text Links, Offer Wall & Incent.<br><br>\r\n\r\n<strong>Disallowed Media:</strong> Email/Newsletter, Facebook PPC, PPC, Social Media & Pop Traffic.<br><br>\r\n\r\n<strong>Special Instructions:</strong> Only for Female.<br>";
		//		String html = "Best deals in Delhi/NCR restaurrants, Spa & Salon, Fitness, Movies, Activities and Travel.<br><br>\r\n\r\n<strong>Converts on:</strong> Sale<br><br>\r\n\r\n<strong>Conversion Flow:</strong><br>\r\n1. User makes any purchase.<br>\r\n2. Sale is counted & credited.<br><br>\r\n\r\n<strong>Tracking Information:</strong><br>\r\nFrequency - Real time<br>\r\nCookie Duration - 7 Days<br>\r\nWeb - <font color=\"green\">Available</font><br>\r\nMobile Web - <font color=\"green\">Available</font><br>\r\nApp - <font color=\"red\">Not Available</font><br>\r\nDeeplink - <font color=\"green\">Available</font> (ONLY through Deeplink Tool <a href=\"http://partners.vcommission.com/publisher/#!/page/25\">here</a>)<br>\r\nMultiple Conversions - <font color=\"green\">Available</font><br><br>\r\n\r\n<strong>Promotion Methods:</strong><br>\r\nText Link - <font color=\"green\">Allowed</font><br>\r\nBanner - <font color=\"green\">Allowed</font><br>\r\nDeals - <font color=\"green\">Allowed</font> (Generic & the ones uploaded <a href=\"http://partners.vcommission.com/publisher/#!/page/6\">here</a>)<br>\r\nCoupons - <font color=\"green\">Allowed</font><br>\r\nCashback - <font color=\"green\"> Allowed</font><br>\r\nEmail (Text) - <font color=\"green\">Allowed</font><br>\r\nCustom Email (Text) - <font color=\"red\">Not Allowed</font><br>\r\nEmail (HTML) - <font color=\"green\">Allowed</font><br>\r\nCustom Email (HTML) - <font color=\"red\">Not Allowed</font><br>\r\nPOP Traffic - <font color=\"red\">Not Allowed</font><br>\r\nNative Ads - <font color=\"red\"> Not Allowed</font><br>\r\nSocial Media - <font color=\"red\">Not Allowed</font><br>\r\nFacebook Ads - <font color=\"red\">Not Allowed</font><br>\r\nSEM - Brand Keyword(s) - <font color=\"red\">Not Allowed</font><br>\r\nSEM - Generic Keyword(s) - <font color=\"green\">Allowed</font><br>\r\nSEM - Brand + Generic Keyword(s) - <font color=\"red\">Not Allowed</font>";
		//		String html = "Install Amazon Android App from Google Play Store.<br><br>\r\n\r\n<strong>Converts on:</strong> Install & First Launch<br><br>\r\n<strong><u>URL to be used as: http://tracking.vcommission.com/aff_c?offer_id=2866&aff_id={affiliate_id}&aff_sub=SUB<font color=\"red\">&google_aid=PASS_GOOGLE_AID</font>\r\n<br>\r\n<i>Please note: If Google AID is not passed properly, the campaign will not track\r\n</strong></u>\r\n<br><br>\r\n<strong>Conversion Flow:</strong><br>\r\n1. User clicks on Install button in Google Play store<br>\r\n2. User opens the app for the first time<br>\r\n3. Install is counted and credited.<br><br>\r\n\r\n<strong>Allowed Media:</strong> Mobile Banners & Text Links<br><br>\r\n\r\n<strong>Disallowed Media:</strong> Email/Newsletter, Facebook PPC, PPC, Social Media, Pop Traffic,  Incent, Offer-wall, Virtual Currency & Cashback <br><br>\r\n\r\n<strong>Special Instructions:</strong> \r\nIt’s mandatory to pass the GAID – post backs without a GAID can result in invalidation of the install.<br>\r\n\r\nPartners are encouraged to drive two main KPIs: <br>\r\n1) First Sign In <br>\r\n2) First Purchases";		
		String[] htmlArr = html.split("<strong>Special Instructions:</strong>");
		if(htmlArr.length > 1)
		{
			return htmlArr[1];
		}
		else
		{
			return "No Special Instruction.";
		}
	}

	private static int randamNoAccordingToArraySize(int size) {
		Random random = new Random();
		int rand = random.nextInt(size-1);
		return rand;
	}



	public String wooDooBox(Long my_user_id) throws Exception {

		Session session = getSession();
		try
		{
			User user = (User) dao.getbyId(session, my_user_id, User.class);
			if(user == null)
				return JSONUtils.getFailJson("User not found with this id.");
			JSONObject response = new JSONObject();
			response.put("status", "success");
			
//			Boolean decision = true;
//
//			if(decision)
//			{
//				//				System.out.println("calling api....");
//				Constants.APICALLTIME = Timings.getCurrentTime();
//				try
//				{
//					boolean updateStatus = updateConversionData(session);
//				}
//				catch (IOException | ParseException e) 
//				{
//					e.printStackTrace();
//					return JSONUtils.getFailJson("Conversion api has changed the json format please inform us.");
//				}
//			}

			Map<String, String> result = woodoodata(my_user_id,session,user);
						System.out.println("here i m  "+result);
			JSONObject doodata = new JSONObject();
			doodata.put("pending_action_doo_cash", result.get("pending_action_doo_cash"));
			doodata.put("pending_shopping_doo_cash",result.get("pending_shopping_doo_cash"));
			doodata.put("total_pending_doo_cash", result.get("total_pending_doo_cash"));
			doodata.put("total_confirmed_doo_cash", result.get("total_confirmed_doo_cash"));
			doodata.put("available_for_instant_redemption", result.get("available_for_instant_redemption"));
			doodata.put("available_for_my_choice_redemption", result.get("available_for_my_choice_redemption"));

			JSONObject woodata = new JSONObject();

			woodata.put("lock_screen_woo_cash", result.get("lock_screen_woo_cash"));
			woodata.put("bonus_woo_cash", result.get("bonus_woo_cash"));
			woodata.put("total_woo_cash", result.get("total_woo_cash"));

			JSONObject totalEarned = new JSONObject();
			totalEarned.put("total_earned", result.get("total_confirmed"));
			totalEarned.put("total_encash", result.get("total_encash"));
			totalEarned.put("gap_from_value_back_target", ""+result.get("gap_from_value_back_target"));
			totalEarned.put("available_for_redemption", ""+result.get("available_for_redemption"));
			totalEarned.put("redeem", result.get("redeem"));
			
			response.put("doo_cash", doodata);
			response.put("woo_cash", woodata);
			response.put("total_earned", totalEarned);

			System.out.println("Response : "+response.toString());

			return response.toString();
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}



	public Map<String, String> woodoodata(Long my_user_id, Session session, User user)  {

		ArrayList<Conversion> conversions = dao.getWooDoodao().getUnusedDoosOfUser(session, my_user_id);

		//		System.out.println("here i m");
		Double pending_action_doo_cash = 0.0; //
		Double pending_shopping_doo_cash = 0.0;//

		Double total_pending_doo_cash = 0.0;//
		Double total_confirmed_doo_cash = 0.0;

		Double available_for_instant_redemption = 0.0;
		Double available_for_my_choice_redemption = 0.0;

		DecimalFormat df = new DecimalFormat("##0.0");
		//		time = Double.valueOf(df.format(time));

		for( Conversion conversion : conversions )
		{
			System.out.println("LOg : "+conversion);
			String status = conversion.getStatus();
			if(status.equalsIgnoreCase(WooStatus.MYCHOICE.toString()))
			{
				available_for_my_choice_redemption = available_for_my_choice_redemption + conversion.getLeft_points();
			}
			else if(status.equalsIgnoreCase(WooStatus.CONFIRMED.toString()))
			{
				total_confirmed_doo_cash = total_confirmed_doo_cash + conversion.getLeft_points();
			}
			if(status.equalsIgnoreCase(WooStatus.PENDING.toString()))
			{
				if(conversion.getType().equalsIgnoreCase("ACTION"))
				{
					pending_action_doo_cash = pending_action_doo_cash + conversion.getLeft_points();
				}
				
				if( conversion.getType().equalsIgnoreCase("SHOPPING") )
				{
					pending_shopping_doo_cash = pending_shopping_doo_cash + conversion.getLeft_points();
				}
			}
		}

		total_pending_doo_cash = pending_action_doo_cash + pending_shopping_doo_cash;

		Map<String , String> result = new HashMap<>();

		System.out.println("pending_action_doo_cash : "+df.format(pending_action_doo_cash));
		System.out.println("pending_shopping_doo_cash : "+df.format(pending_shopping_doo_cash));
		System.out.println("total_pending_doo_cash : "+df.format(total_pending_doo_cash));
		System.out.println("total_confirmed_doo_cash : "+df.format(total_confirmed_doo_cash));
		System.out.println("available_for_my_choice_redemption : "+df.format(available_for_my_choice_redemption));

		result.put("pending_action_doo_cash", ""+pending_action_doo_cash.longValue());
		result.put("pending_shopping_doo_cash", ""+pending_shopping_doo_cash.longValue());
		result.put("total_pending_doo_cash", ""+total_pending_doo_cash.longValue());
		result.put("total_confirmed_doo_cash", ""+total_confirmed_doo_cash.longValue());
		result.put("available_for_my_choice_redemption", ""+available_for_my_choice_redemption.longValue());
		result.put("available_for_instant_redemption", "0");
		
		Long lockMoney = dao.getWooDoodao().getLockScreenCashOfUser(session, my_user_id,"lock");
		Long extraMoney = dao.getWooDoodao().getLockScreenCashOfUser(session, my_user_id,"extra");

		//		System.out.println("Lock Money : "+lockMoney);
		//		System.out.println("Extra Money : "+extraMoney);
		
		Long totalWoo = (extraMoney.longValue()+lockMoney.longValue());
		Long redeemAble = 0l;
		
		System.out.println("kojch "+extraMoney.longValue() + lockMoney.longValue());
		result.put("lock_screen_woo_cash", ""+lockMoney.longValue()); 
		result.put("bonus_woo_cash", ""+extraMoney.longValue());
		result.put("total_woo_cash", ""+totalWoo);
		
		if(totalWoo > available_for_my_choice_redemption.longValue())
			redeemAble = available_for_my_choice_redemption.longValue();
		else
			redeemAble = totalWoo;
		
		result.put("available_for_redemption", ""+redeemAble);
		
		Long encashed = getTotalEncashed(session, user);
		
		result.put("total_encash", ""+encashed);

		result.put("gap_from_value_back_target", ""+(user.getPhone_value() - redeemAble));
		
		result.put("total_confirmed", ""+(redeemAble+encashed));
		
		result.put("redeem", redeemAble+"");
		return result;
	}
	
	public Map<String, String> woodoodata(Long my_user_id, Session session, User user,Long date1,Long date2,String report)  {

		ArrayList<Conversion> conversions = dao.getWooDoodao().getUnusedDoosOfUser(session, my_user_id,date1,date2);

		//		System.out.println("here i m");
		Double pending_action_doo_cash = 0.0; //
		Double pending_shopping_doo_cash = 0.0;//

		Double total_pending_doo_cash = 0.0;//
		Double total_confirmed_doo_cash = 0.0;

		Double available_for_instant_redemption = 0.0;
		Double available_for_my_choice_redemption = 0.0;

		DecimalFormat df = new DecimalFormat("##0.0");
		//		time = Double.valueOf(df.format(time));

		for( Conversion conversion : conversions )
		{
			System.out.println("LOg : "+conversion);
			String status = conversion.getStatus();
			if(status.equalsIgnoreCase(WooStatus.MYCHOICE.toString()))
			{
				available_for_my_choice_redemption = available_for_my_choice_redemption + conversion.getLeft_points();
			}
			else if(status.equalsIgnoreCase(WooStatus.CONFIRMED.toString()))
			{
				total_confirmed_doo_cash = total_confirmed_doo_cash + conversion.getLeft_points();
			}
			if(status.equalsIgnoreCase(WooStatus.PENDING.toString()))
			{
				if(conversion.getType().equalsIgnoreCase("ACTION"))
				{
					pending_action_doo_cash = pending_action_doo_cash + conversion.getLeft_points();
				}
				
				if( conversion.getType().equalsIgnoreCase("SHOPPING") )
				{
					pending_shopping_doo_cash = pending_shopping_doo_cash + conversion.getLeft_points();
				}
			}
		}

		total_pending_doo_cash = pending_action_doo_cash + pending_shopping_doo_cash;

		Map<String , String> result = new HashMap<>();

		System.out.println("pending_action_doo_cash : "+df.format(pending_action_doo_cash));
		System.out.println("pending_shopping_doo_cash : "+df.format(pending_shopping_doo_cash));
		System.out.println("total_pending_doo_cash : "+df.format(total_pending_doo_cash));
		System.out.println("total_confirmed_doo_cash : "+df.format(total_confirmed_doo_cash));
		System.out.println("available_for_my_choice_redemption : "+df.format(available_for_my_choice_redemption));

		result.put("pending_action_doo_cash", ""+pending_action_doo_cash.longValue());
		result.put("pending_shopping_doo_cash", ""+pending_shopping_doo_cash.longValue());
		result.put("total_pending_doo_cash", ""+total_pending_doo_cash.longValue());
		result.put("total_confirmed_doo_cash", ""+total_confirmed_doo_cash.longValue());
		result.put("available_for_my_choice_redemption", ""+available_for_my_choice_redemption.longValue());
		result.put("available_for_instant_redemption", "0");
		
		Long lockMoney = dao.getWooDoodao().getLockScreenCashOfUser(session, my_user_id,"lock",date1,date2,report);
		Long extraMoney = dao.getWooDoodao().getLockScreenCashOfUser(session, my_user_id,"extra",date1,date2,report);

		//		System.out.println("Lock Money : "+lockMoney);
		//		System.out.println("Extra Money : "+extraMoney);
		
		Long totalWoo = (extraMoney.longValue()+lockMoney.longValue());
		Long redeemAble = 0l;
		
		System.out.println("kojch "+extraMoney.longValue() + lockMoney.longValue());
		result.put("lock_screen_woo_cash", ""+lockMoney.longValue()); 
		result.put("bonus_woo_cash", ""+extraMoney.longValue());
		result.put("total_woo_cash", ""+totalWoo);
		
		if(totalWoo > available_for_my_choice_redemption.longValue())
			redeemAble = available_for_my_choice_redemption.longValue();
		else
			redeemAble = totalWoo;
		
		result.put("available_for_redemption", ""+redeemAble);
		
		Long encashed = getTotalEncashed(session, user);
		
		result.put("total_encash", ""+encashed);

		result.put("gap_from_value_back_target", ""+(user.getPhone_value() - redeemAble));
		
		result.put("total_confirmed", ""+(redeemAble+encashed));
		
		result.put("redeem", redeemAble+"");
		return result;
	}

	private Long getTotalEncashed(Session session, User user) {

		ArrayList<Conversion> doos = dao.getWooDoodao().getUsedDoos(session, user.getUser_id());
		//		System.out.println("Here");
		//		System.out.println(doos);

		Double used=0.0;
		for(Conversion con : doos)
		{
			Double left = con.getUsers_payout() - con.getLeft_points();
			used = used+left;
		}
		//		System.out.println("Used : "+used);
		return used.longValue();
	}

	@SuppressWarnings("unchecked")
	public boolean updateConversionData(Session session) throws Exception {

		String output = HttpCaller.getCall(Constants.vCommissionConversion);
		JSONObject obj = new JSONObject(output);
		Integer pageCount = obj.getJSONObject("response").getJSONObject("data").getInt("pageCount");
		System.out.println("Total Count " + obj.getJSONObject("response").getJSONObject("data").getInt("count") );
		
		int k = 0;
		int lm = 0;
		
//		dao.truncate(session, Conversion.class);
		
		for(int j = 1; j <= pageCount; j++)
		{
			ArrayList<ConversionView> conversionviews = ConversionApiCaller.conversionApiCaller(j);
			
			int i=0;
			
			for(ConversionView view : conversionviews)
			{
//				System.out.println("Viewdcxcc  "+view);
				if(view.getAffiliate_info() != null && !view.getAffiliate_info().equalsIgnoreCase(""))
				{
//					System.out.println("dsfdsfdsfdsfsfsdfsfffdsfdsfdsfdsfdsfsdf " + ++k );
				}
				
				if(view.getAffiliate_info() != null && !view.getAffiliate_info().equalsIgnoreCase(""))
				{
					if(i == 10)
					{
						session.clear();
						System.out.println("Session cleared.");
						i=0;
					}
//					System.out.println("Fore test "+view);
					Map<String, Object> map1 = new HashMap<>();
					try {
						map1.put("user_id", Long.parseLong(view.getAffiliate_info()));
					} catch (Exception e) {
						System.out.println("sdfdsfds " + view.getAffiliate_info());
						continue;
					}
					
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					Date date = format.parse(view.getDate());
					map1.put("conversion_time", date.getTime());
					map1.put("hour", view.getHour());
					map1.put("offer_id", view.getOffer_id());
					map1.put("uniqueId",view.getUniqueId());
					
					DecimalFormat formatq = new DecimalFormat("##0.00");
					
//					map1.put("approved_payout", Double.parseDouble(formatq.format(Double.parseDouble(view.getPayout()))));

					System.out.println("Appro  "+Double.parseDouble(formatq.format(Double.parseDouble(view.getPayout()))));
					
					Conversion old = (Conversion) dao.getbyUniqueColumns(session, map1, Conversion.class);
					
					if(old != null)
					{
						Double newMoney = Double.parseDouble(view.getPayout());
						
						if(old.getApproved_payout().equals(0.0) && !newMoney.equals(0.0) )
						{
							old.setApproved_payout(Double.parseDouble(view.getPayout()));
							Double pay = (old.getApproved_payout() * 0.8);
							Long l = pay.longValue();
							old.setUsers_payout(l.doubleValue());
							old.setLeft_points(old.getUsers_payout());
						}
						
						if(view.getConversion().longValue() > 0)
						{
							Long myChoice = (old.getConversion_time()+Constants.myChoiceDuration);
							Long confirmed = (old.getConversion_time()+Constants.confirmedRedumptionDuration);

							if(myChoice < Timings.getCurrentTime())
							{
								if(!old.getStatus().equalsIgnoreCase(WooStatus.MYCHOICE.toString()))
								{
									//							Send notification of mychoice
									Map<String, String> map = new HashMap<>();
									User oldUser = (User) dao.getbyId(session, old.getUser_id(), User.class);

									String message = old.getUsers_payout()+" Doo-Cash added to your MyChoice money.";
									map.put("userId", ""+old.getId());
									map.put("message", message);
									map.put("type", "newDooCash");
									map.put("title", "New Doo Cash");

									Notifications notification = new Notifications();
									notification.setMessage(message);
									notification.setStatus(true);
									notification.setUser_id(old.getUser_id());
									notification.setDatetime(Timings.getCurrentTime());
									dao.insert(session, notification);

									if(oldUser != null)
										AndroidNotifications.generalNotificationToUser(oldUser, map);
								}
								old.setStatus(WooStatus.MYCHOICE.toString());
							}
							else if(confirmed < Timings.getCurrentTime())
							{
								if(!old.getStatus().equalsIgnoreCase(WooStatus.CONFIRMED.toString()))
								{
									Map<String, String> map = new HashMap<>();
									User oldUser = (User) dao.getbyId(session, old.getUser_id(), User.class);
									//							Send notification confirmed
									String message = "Your "+old.getApproved_payout()+" Doo-Cash has been confirmed.";
									map.put("userId", ""+old.getId());
									map.put("message", message);
									map.put("type", "newDooCash");
									map.put("title", "New Doo Cash");
									
									Notifications notification = new Notifications();
									notification.setMessage(message);
									notification.setStatus(true);
									notification.setUser_id(old.getId());
									notification.setDatetime(Timings.getCurrentTime());

									dao.insert(session, notification);
									if(oldUser != null)
										AndroidNotifications.generalNotificationToUser(oldUser, map);
								}
								old.setStatus(WooStatus.CONFIRMED.toString());
							}
							else
							{
								old.setStatus(WooStatus.PENDING.toString());
							}
						}
						else
						{
							old.setStatus(WooStatus.PENDING.toString());
						}
						dao.update(session, old);
					}
					else
					{
						System.out.println("here");
						Conversion newConversion = new Conversion();
						newConversion.setApproved_payout(Double.parseDouble(view.getPayout()));
						Double pay = (newConversion.getApproved_payout() * 0.8);
						Long l = pay.longValue();
						newConversion.setUsers_payout(l.doubleValue());
						newConversion.setLeft_points(newConversion.getUsers_payout());
						
						newConversion.setStatus(WooStatus.PENDING.toString());
						
						newConversion.setUniqueId(view.getUniqueId());
						newConversion.setConversion_status(true);
//						DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
						newConversion.setConversion_time(view.getDatetime());
						newConversion.setDate_time(new Date(view.getDatetime()));
						newConversion.setOffer_id(view.getOffer_id());
						newConversion.setOfferName(view.getOfferName());
						newConversion.setHour(view.getHour());
						if(view.getOfferName().contains("CPS"))
						{
							newConversion.setType("SHOPPING");
						}
						else
						{
							newConversion.setType("ACTION");
						}
						newConversion.setUser_id(Long.parseLong(view.getAffiliate_info()));
						System.out.println("inserting " + ++lm );
						dao.insert(session, newConversion);
					}
					old = null;
				}


			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public boolean updateConversionData1(Session session) throws Exception {

		String output = HttpCaller.getCall(Constants.vCommissionConversion);
		JSONObject obj = new JSONObject(output);
		Integer pageCount = obj.getJSONObject("response").getJSONObject("data").getInt("pageCount");
		System.out.println("Total Count " + obj.getJSONObject("response").getJSONObject("data").getInt("count") );
		
		int k = 0;
		int lm = 0;
		
		dao.truncate(session, Conversion.class);
		
		for(int j = 1; j <= pageCount; j++)
		{

			ArrayList<ConversionView> conversionviews = ConversionApiCaller.conversionApiCaller(j);
			
			int i=0;
			
			for(ConversionView view : conversionviews)
			{
//				System.out.println("Viewdcxcc  "+view);
				if(view.getAffiliate_info() != null && !view.getAffiliate_info().equalsIgnoreCase(""))
				{
//					System.out.println("dsfdsfdsfdsfsfsdfsfffdsfdsfdsfdsfdsfsdf " + ++k );
				}
				
				if(view.getAffiliate_info() != null && !view.getAffiliate_info().equalsIgnoreCase(""))
				{
					if(i == 10)
					{
						session.clear();
						System.out.println("Session cleared.");
						i=0;
					}
//					System.out.println("Fore test "+view);
					Map<String, Object> map1 = new HashMap<>();
					try {
						map1.put("user_id", Long.parseLong(view.getAffiliate_info()));
					} catch (Exception e) {
						System.out.println("sdfdsfds " + view.getAffiliate_info());
						continue;
					}
					map1.put("offer_id", view.getOffer_id());
					DecimalFormat formatq = new DecimalFormat("##0.00");
					
					map1.put("approved_payout", Double.parseDouble(formatq.format(Double.parseDouble(view.getPayout()))));

					System.out.println("Appro  "+Double.parseDouble(formatq.format(Double.parseDouble(view.getPayout()))));
					
					Conversion old = (Conversion) dao.getbyUniqueColumns(session, map1, Conversion.class);
					
					if(old != null)
					{
//						System.out.println("updating " +  old.getOffer_id() + " " + old.getUser_id());
//						System.out.println("O  "+old); 
						
						if(view.getConversion().longValue() > 0)
						{
//							System.out.println("Con "+old.getConversion_time()+"  \n sec :"+Constants.myChoiceDuration+"  \n Total"+old.getConversion_time()+Constants.myChoiceDuration);
							Long myChoice = (old.getConversion_time()+Constants.myChoiceDuration);
							Long confirmed = (old.getConversion_time()+Constants.confirmedRedumptionDuration);

//							System.out.println("My "+myChoice);
							
//							if(!old.getUsers_payout().equals(view.getPayout()))
//							{
//								System.out.println(view.getPayout()+"I m not matching : "+view);
//								
//								Double used = old.getUsers_payout() - old.getLeft_points();
//								old.setApproved_payout(Double.parseDouble(view.getPayout()));
//								old.setUsers_payout(old.getApproved_payout() * 0.8);
//								old.setLeft_points( old.getUsers_payout() - used );
//							}

							if(myChoice < Timings.getCurrentTime())
							{
								if(!old.getStatus().equalsIgnoreCase(WooStatus.MYCHOICE.toString()))
								{
									//							Send notification of mychoice
									Map<String, String> map = new HashMap<>();
									User oldUser = (User) dao.getbyId(session, old.getUser_id(), User.class);

									String message = old.getUsers_payout()+" Doo-Cash added to your MyChoice money.";
									map.put("userId", ""+old.getId());
									map.put("message", message);
									map.put("type", "newDooCash");
									map.put("title", "New Doo Cash");

									Notifications notification = new Notifications();
									notification.setMessage(message);
									notification.setStatus(true);
									notification.setUser_id(old.getUser_id());
									notification.setDatetime(Timings.getCurrentTime());
									dao.insert(session, notification);

									if(oldUser != null)
										AndroidNotifications.generalNotificationToUser(oldUser, map);
								}
								old.setStatus(WooStatus.MYCHOICE.toString());
							}
							else if(confirmed < Timings.getCurrentTime())
							{
								if(!old.getStatus().equalsIgnoreCase(WooStatus.CONFIRMED.toString()))
								{
									Map<String, String> map = new HashMap<>();
									User oldUser = (User) dao.getbyId(session, old.getUser_id(), User.class);
									//							Send notification confirmed
									String message = "Your "+old.getApproved_payout()+" Doo-Cash has been confirmed.";
									map.put("userId", ""+old.getId());
									map.put("message", message);
									map.put("type", "newDooCash");
									map.put("title", "New Doo Cash");
									
									Notifications notification = new Notifications();
									notification.setMessage(message);
									notification.setStatus(true);
									notification.setUser_id(old.getId());
									notification.setDatetime(Timings.getCurrentTime());

									dao.insert(session, notification);
									if(oldUser != null)
										AndroidNotifications.generalNotificationToUser(oldUser, map);
								}
								old.setStatus(WooStatus.CONFIRMED.toString());
							}
							else
							{
								old.setStatus(WooStatus.PENDING.toString());
							}
						}
						else
						{
							old.setStatus(WooStatus.PENDING.toString());
						}
//						System.out.println("Updated :  "+old);
						dao.update(session, old);
					}
					else
					{
						System.out.println("here");
						Conversion newConversion = new Conversion();
						newConversion.setApproved_payout(Double.parseDouble(view.getPayout()));
//						System.out.println("Appp : "+newConversion.getApproved_payout()+"  res : "+newConversion.getApproved_payout() * 0.8);
						Double pay = (newConversion.getApproved_payout() * 0.8);
						Long l = pay.longValue();
						newConversion.setUsers_payout(l.doubleValue());
						newConversion.setStatus(WooStatus.PENDING.toString());
						newConversion.setLeft_points(newConversion.getUsers_payout());
						
						newConversion.setConversion_status(true);
						
						DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
						newConversion.setConversion_time(view.getDatetime());
						newConversion.setDate_time(new Date(view.getDatetime()));
						newConversion.setOffer_id(view.getOffer_id());
						newConversion.setOfferName(view.getOfferName());

						if(view.getOfferName().contains("CPS"))
						{
							newConversion.setType("SHOPPING");
						}
						else
						{
							newConversion.setType("ACTION");
						}
						newConversion.setUser_id(Long.parseLong(view.getAffiliate_info()));
//						System.out.println("Inserting : "+newConversion);
						System.out.println("inserting " + ++lm );
						dao.insert(session, newConversion);
					}
					old = null;
				}


			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public String dooDaysCalculatorNew(Long my_user_id, Long time,Long countdata) throws Exception {
		Session session = getSession();
		try
		{
		    
		            Lsimpressionnew obj=new Lsimpressionnew();
		            obj.setUser_id(my_user_id);
		            obj.setPhone_time(time);
		            obj.setTime_count(countdata);
		            obj.setStatus("PENDING");
		            dao.insert(session, obj);
			
			JSONObject response = new JSONObject();
			response.put("status", "success");
			
			Long oneDooValue = 1800l;
			Long pointsPerUnit = 50l;
			Long timeConsumed = 0l;
			Long timeLeft = 0l;
			int numDoosGot= 0;
//			Long dooGot = (time / oneDooValue) * pointsPerUnit;
//			System.out.println("Doo Got: "+dooGot);

			Long dooGot = 0l;
			Criteria c = session.createCriteria(Lsimpressionnew.class);
			c.add(Restrictions.eq("user_id", my_user_id));			
			c.add(Restrictions.eq("status", "PENDING"));
			ArrayList<Lsimpressionnew> wootime = (ArrayList<Lsimpressionnew>) c.list();
			
			for (int i = 0; i < wootime.size(); i++) {
				
				timeConsumed=timeConsumed+wootime.get(i).getTime_count();
			}
			
			if(timeConsumed > oneDooValue)
			{
				numDoosGot=(int) (timeConsumed/oneDooValue);
				dooGot=numDoosGot*50l;
				timeLeft=timeConsumed%oneDooValue;
			}
			if(timeConsumed < oneDooValue)
			{
				response.put("timecount", (int)(((float)timeConsumed/oneDooValue)*100));
			}else
			{
				Long rValue=timeConsumed%oneDooValue;
				response.put("timecount", (int)(((float)rValue/oneDooValue)*100));
			}
			
			if(dooGot > 0 )
			{
				WooLog log = new WooLog();
				log.setUser_id(my_user_id);
				log.setWoo_point(dooGot);
				log.setDate_time(Timings.getCurrentTime());
				List<WooPromotions> promotions = dao.getWooDoodao().checkForPromotions(session);
				Long extraPoint = 0l;
				for(WooPromotions p : promotions)
				{
					extraPoint = (extraPoint + (p.getExtra_percentage() / 100) * log.getWoo_point());
				}
				log.setExtra_point(extraPoint);
				log.setLeft_points(log.getExtra_point()+log.getWoo_point());
				log.setIsBonus(false);
				//				System.out.println("Log Filled :");
				dao.insert(session, log);
				for (int i = 0; i < wootime.size(); i++) {
					Lsimpressionnew wootemp=wootime.get(i);
					wootemp.setStatus("PROCESSED");
					dao.update(session, wootemp);
				}
				    obj=new Lsimpressionnew();
		            obj.setUser_id(my_user_id);
		            obj.setPhone_time(time);
		            obj.setTime_count(timeLeft);
		            obj.setStatus("PENDING");
		            dao.insert(session, obj);
				//timeConsumed = (dooGot/pointsPerUnit) * oneDooValue;
				//System.out.println("Woo got: "+dooGot+"Time consumed : "+timeConsumed);
    			
				 User Users = (User) dao.getbyId(session,my_user_id, User.class);
					Map<String, String> map = new HashMap<>();
					
					map.put("message", "Hi. Congratulations! You have earned "+ dooGot +" Lock Screen Woo Cash! Make sure to select ON in SETTINGS & SAVE to continue to earn ASSURED Daily & Weekly Paytm Cash & 100% Valueback for your smartphone. TCA.");
					map.put("type", "setting");
					map.put("title", "WooDoo");
					map.put("url", "");			
					map.put("userId", ""+my_user_id);				
                    AndroidNotifications.generalNotificationToUser(Users, map);
			}
			

			//response.put("time_consumed", timeConsumed);
			//response.put("time_left", timeLeft);
			System.out.println("Response : "+response.toString());

			return response.toString();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}

	}
	
	@SuppressWarnings("unchecked")
	public String dooDaysCalculator(Long my_user_id, Long time) throws Exception {
		Session session = getSession();
		try
		{
			JSONObject response = new JSONObject();
			response.put("status", "success");
			Long oneDooValue = Constants.oneDooValue;
			Long pointsPerUnit = 50l;
			Long timeConsumed = 0l;
			Long timeLeft = 0l;

//			Long dooGot = (time / oneDooValue) * pointsPerUnit;
//			System.out.println("Doo Got: "+dooGot);

			Long dooGot = 0l;
			
			if(time > oneDooValue)
			{
				dooGot=50l;
			}
			
			if(dooGot > 0 )
			{
				WooLog log = new WooLog();
				log.setUser_id(my_user_id);
				log.setWoo_point(dooGot);
				log.setDate_time(Timings.getCurrentTime());
				List<WooPromotions> promotions = dao.getWooDoodao().checkForPromotions(session);
				Long extraPoint = 0l;
				for(WooPromotions p : promotions)
				{
					extraPoint = (extraPoint + (p.getExtra_percentage() / 100) * log.getWoo_point());
				}
				log.setExtra_point(extraPoint);
				log.setLeft_points(log.getExtra_point()+log.getWoo_point());
				log.setIsBonus(false);
				//				System.out.println("Log Filled :");
				dao.insert(session, log);

				timeConsumed = (dooGot/pointsPerUnit) * oneDooValue;
				System.out.println("Woo got: "+dooGot+"Time consumed : "+timeConsumed);
    			timeLeft = time % oneDooValue;
				 User Users = (User) dao.getbyId(session,my_user_id, User.class);
					Map<String, String> map = new HashMap<>();
					
					map.put("message", "Hi. Congratulations! You have earned 50 Lock Screen Woo Cash! Make sure to select ON in SETTINGS & SAVE to continue to earn ASSURED Daily & Weekly Paytm Cash & 100% Valueback for your smartphone. TCA.");
					map.put("type", "setting");
					map.put("title", "WooDoo");
					map.put("url", "");			
					map.put("userId", ""+my_user_id);				
                    AndroidNotifications.generalNotificationToUser(Users, map);
			}
			else
			{
				timeLeft = time;
			}

			response.put("time_consumed", timeConsumed);
			response.put("time_left", timeLeft);
			System.out.println("Response : "+response.toString());

			return response.toString();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}


	}
	
	@SuppressWarnings("unchecked")
	public String wooTimeCalculator(JSONObject recoData) throws Exception {
		Session session = getSession();
		try
		{
		        Long maxValue=-1l;
		         Long my_user_id = recoData.getLong("my_user_id");
		         JSONArray arr=(JSONArray) recoData.get("time");
		         Set<Long> timeImpr = new HashSet<>(arr.length());
		         for (int i = 0; i < arr.length(); i++) {
		            Long time = arr.getLong(i);
		            if(!timeImpr.add(time))
		               continue;

		            Lsimpression obj=new Lsimpression();
		            obj.setUser_id(my_user_id);
		            obj.setPhone_time(time);
		            obj.setStatus("PENDING");
		            dao.insert(session, obj);
		            if(maxValue<time)
		            {
		            	maxValue=time;
		            }
		         }

			
			//Gson gson = new Gson();
		    //String json = gson.toJson(time);
			
			
			
			JSONObject response = new JSONObject();
			response.put("status", "success");
			response.put("maxValue", maxValue);
			Long oneDooValue = 1800l;
			Long pointsPerUnit = 50l;
			Long timeConsumed = 0l;
			Long timeLeft = 0l;
			int numDoosGot= 0;
//			Long dooGot = (time / oneDooValue) * pointsPerUnit;
//			System.out.println("Doo Got: "+dooGot);

			Long dooGot = 0l;
			Criteria c = session.createCriteria(Lsimpression.class);
			c.add(Restrictions.eq("user_id", my_user_id));			
			c.add(Restrictions.eq("status", "PENDING"));
			ArrayList<Lsimpression> wootime = (ArrayList<Lsimpression>) c.list();
			
			if(wootime.size() > oneDooValue)
			{
				numDoosGot=(int) (wootime.size()/oneDooValue);
				dooGot=numDoosGot*50l;
			}
			if(wootime.size() < oneDooValue)
			{
				response.put("timecount", (int)(((float)wootime.size()/oneDooValue)*100));
			}else
			{
				Long rValue=wootime.size()%oneDooValue;
				response.put("timecount", (int)(((float)rValue/oneDooValue)*100));
			}
			
			if(dooGot > 0 )
			{
				WooLog log = new WooLog();
				log.setUser_id(my_user_id);
				log.setWoo_point(dooGot);
				log.setDate_time(Timings.getCurrentTime());
				List<WooPromotions> promotions = dao.getWooDoodao().checkForPromotions(session);
				Long extraPoint = 0l;
				for(WooPromotions p : promotions)
				{
					extraPoint = (extraPoint + (p.getExtra_percentage() / 100) * log.getWoo_point());
				}
				log.setExtra_point(extraPoint);
				log.setLeft_points(log.getExtra_point()+log.getWoo_point());
				log.setIsBonus(false);
				//				System.out.println("Log Filled :");
				dao.insert(session, log);
				for (int i = 0; i < (oneDooValue*numDoosGot); i++) {
					Lsimpression wootemp=wootime.get(i);
					wootemp.setStatus("PROCESSED");
					dao.update(session, wootemp);
				}
				//timeConsumed = (dooGot/pointsPerUnit) * oneDooValue;
				//System.out.println("Woo got: "+dooGot+"Time consumed : "+timeConsumed);
    			
				 User Users = (User) dao.getbyId(session,my_user_id, User.class);
					Map<String, String> map = new HashMap<>();
					
					map.put("message", "Hi. Congratulations! You have earned "+ dooGot +" Lock Screen Woo Cash! Make sure to select ON in SETTINGS & SAVE to continue to earn ASSURED Daily & Weekly Paytm Cash & 100% Valueback for your smartphone. TCA.");
					map.put("type", "setting");
					map.put("title", "WooDoo");
					map.put("url", "");			
					map.put("userId", ""+my_user_id);				
                    AndroidNotifications.generalNotificationToUser(Users, map);
			}
			

			//response.put("time_consumed", timeConsumed);
			//response.put("time_left", timeLeft);
			System.out.println("Response : "+response.toString());

			return response.toString();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}


	}

	public String addPromotions(WooPromotions promotion) throws Exception {
		Session session = getSession();
		try{
			Boolean check = dao.getWooDoodao().checkForPromotionsBeetweenDays(session, promotion.getStart_time(), promotion.getEnd_time());

			if(check){
				return JSONUtils.getFailWithReasonJson("Another promo offer is overlapping this promotion.");
			}
			dao.insert(session, promotion);
			System.out.println("Response : "+JSONUtils.getSuccessJson(promotion));

			return JSONUtils.getSuccessJson(promotion);
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}

	}

	public String getAllPromotions() throws Exception 
	{
		Session session = getSession();
		try{
			ArrayList<WooPromotions> promotions = dao.getall(session, WooPromotions.class);
			System.out.println("Response : "+JSONUtils.getSuccessJson(promotions));

			return JSONUtils.getSuccessJson(promotions);
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String getAllPromotionsBetweenBates(Long start_date, Long end_date) throws Exception {
		Session session = getSession();
		try{
			ArrayList<WooPromotions> promotions = dao.getWooDoodao().getPromotionsBetweenDates(session, start_date,end_date);
			System.out.println("Response : "+JSONUtils.getSuccessJson(promotions));

			return JSONUtils.getSuccessJson(promotions);
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String getAllActivePromotions() throws Exception {
		Session session = getSession();
		try{
			List<WooPromotions> promotions = dao.getWooDoodao().checkForPromotions(session);
			System.out.println("Response : "+JSONUtils.getSuccessJson(promotions));
			return JSONUtils.getSuccessJson(promotions);
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}
	public static void main(String[] args) {

		Double d = 7.9;
		System.out.println(d.longValue());
		Long l = d.longValue();

		System.out.println(l.doubleValue());
		//		Double d = 1212121.0;
		//		DecimalFormat df = new DecimalFormat("##0.0");       
		//		d = Double.valueOf(df.format(d));
		//		System.out.println(d);
		//		

		//		randamNoAccordingToArraySize(10);

		//		String html = "Install QuackQuack Android App from Google Play Store.<br><br>\r\n\r\n<strong>Converts on:</strong> Install & Register.<br><br>\r\n\r\n<strong>Conversion Flow:</strong><br>\r\n1. User clicks on Install button in Google Play Store.<br>\r\n2. User signup via Facebook.<br>\r\n3. User select some personal information 1st page and 2nd page of the application.<br>\r\n4. Registration is counted and credited.<br><br>\r\n\r\n<strong>Allowed Media:</strong> Mobile Banners, Text Links, Offer Wall & Incent.<br><br>\r\n\r\n<strong>Disallowed Media:</strong> Email/Newsletter, Facebook PPC, PPC, Social Media & Pop Traffic.<br><br>\r\n\r\n<strong>Special Instructions:</strong> Only for Female.<br>";
		//		String html = "Best deals in Delhi/NCR restaurrants, Spa & Salon, Fitness, Movies, Activities and Travel.<br><br>\r\n\r\n<strong>Converts on:</strong> Sale<br><br>\r\n\r\n<strong>Conversion Flow:</strong><br>\r\n1. User makes any purchase.<br>\r\n2. Sale is counted & credited.<br><br>\r\n\r\n<strong>Tracking Information:</strong><br>\r\nFrequency - Real time<br>\r\nCookie Duration - 7 Days<br>\r\nWeb - <font color=\"green\">Available</font><br>\r\nMobile Web - <font color=\"green\">Available</font><br>\r\nApp - <font color=\"red\">Not Available</font><br>\r\nDeeplink - <font color=\"green\">Available</font> (ONLY through Deeplink Tool <a href=\"http://partners.vcommission.com/publisher/#!/page/25\">here</a>)<br>\r\nMultiple Conversions - <font color=\"green\">Available</font><br><br>\r\n\r\n<strong>Promotion Methods:</strong><br>\r\nText Link - <font color=\"green\">Allowed</font><br>\r\nBanner - <font color=\"green\">Allowed</font><br>\r\nDeals - <font color=\"green\">Allowed</font> (Generic & the ones uploaded <a href=\"http://partners.vcommission.com/publisher/#!/page/6\">here</a>)<br>\r\nCoupons - <font color=\"green\">Allowed</font><br>\r\nCashback - <font color=\"green\"> Allowed</font><br>\r\nEmail (Text) - <font color=\"green\">Allowed</font><br>\r\nCustom Email (Text) - <font color=\"red\">Not Allowed</font><br>\r\nEmail (HTML) - <font color=\"green\">Allowed</font><br>\r\nCustom Email (HTML) - <font color=\"red\">Not Allowed</font><br>\r\nPOP Traffic - <font color=\"red\">Not Allowed</font><br>\r\nNative Ads - <font color=\"red\"> Not Allowed</font><br>\r\nSocial Media - <font color=\"red\">Not Allowed</font><br>\r\nFacebook Ads - <font color=\"red\">Not Allowed</font><br>\r\nSEM - Brand Keyword(s) - <font color=\"red\">Not Allowed</font><br>\r\nSEM - Generic Keyword(s) - <font color=\"green\">Allowed</font><br>\r\nSEM - Brand + Generic Keyword(s) - <font color=\"red\">Not Allowed</font>";
		//		String html = "Install Amazon Android App from Google Play Store.<br><br>\r\n\r\n<strong>Converts on:</strong> Install & First Launch<br><br>\r\n<strong><u>URL to be used as: http://tracking.vcommission.com/aff_c?offer_id=2866&aff_id={affiliate_id}&aff_sub=SUB<font color=\"red\">&google_aid=PASS_GOOGLE_AID</font>\r\n<br>\r\n<i>Please note: If Google AID is not passed properly, the campaign will not track\r\n</strong></u>\r\n<br><br>\r\n<strong>Conversion Flow:</strong><br>\r\n1. User clicks on Install button in Google Play store<br>\r\n2. User opens the app for the first time<br>\r\n3. Install is counted and credited.<br><br>\r\n\r\n<strong>Allowed Media:</strong> Mobile Banners & Text Links<br><br>\r\n\r\n<strong>Disallowed Media:</strong> Email/Newsletter, Facebook PPC, PPC, Social Media, Pop Traffic,  Incent, Offer-wall, Virtual Currency & Cashback <br><br>\r\n\r\n<strong>Special Instructions:</strong> \r\nIt’s mandatory to pass the GAID – post backs without a GAID can result in invalidation of the install.<br>\r\n\r\nPartners are encouraged to drive two main KPIs: <br>\r\n1) First Sign In <br>\r\n2) First Purchases";		
		//		System.out.println("Special Instruction : "+getSpecialInstructio(html));
		//		System.out.println("Conversion flow : "+getConversionFlow(html));
		//		Random random = new Random();
		//		int rand = random.nextInt(10-1);
		//		System.out.println(rand);
		//		Document doc = Jsoup.parse(htmlArr[1]);
		//				Elements links = doc.select("strong");
		//				Element head = doc.select("head").first();
		//				Element title = head.select("title").first();
		//				System.out.println(htmlArr[1]);
		//				System.out.println("H  "+links.html());

	}
	public String reportMissingWooCash(Long user_id, Long fromDateAndTime, Long toDateAndTime, String comments) throws Exception {

		Session session = getSession();
		try{
			System.out.println("From Date "+fromDateAndTime+" To DateTime "+toDateAndTime);
			User user = (User) dao.getbyId(session, user_id, User.class);
			if(user == null) 
				return JSONUtils.getFailJson("User not find.");
			fromDateAndTime = fromDateAndTime+Constants.GMT;
			toDateAndTime = toDateAndTime+Constants.GMT; 
			DateFormat format = new SimpleDateFormat("dd-MMM-YYYY hh:mm a");
			Date date = new Date(fromDateAndTime);     
			String fromDateTime = format.format(date);
			Date toDate = new Date(toDateAndTime);
			String toDateTime = format.format(toDate);
			String rCode = "";
			rCode = ""+Constants.generateRandomString(5);

			String text = "I have not got my Woo points of \""+fromDateTime+" to "+toDateTime+"\"";
			String subject = "Missing Woo Cash of Name : "+user.getFirst_name() +" \n Email : "+user.getEmail()+" Reference No. :"+rCode;

			EmailSender email = new EmailSender(text, subject, Constants.EMAIL);
			email.start();
			System.out.println("Response : "+JSONUtils.getSuccessJson(rCode));

			return JSONUtils.getSuccessJson(rCode);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String reportMissingActionDooCash(String actionTaken, String docash, Long date, String comments, Long user_id) throws Exception {

		Session session = getSession();
		try{
			User user = (User) dao.getbyId(session, user_id, User.class);

			date = date+Constants.GMT;
			if(user == null)
				return JSONUtils.getFailJson("User not find.");  
			DateFormat format = new SimpleDateFormat("dd-MMM-YYYY");
			Date toDate = new Date(date);
			String toDateTime = format.format(toDate);

			String rCode = "";
			rCode = ""+Constants.generateRandomString(5); 

			String text = "I have not got my Action Doo cash of \""+toDateTime+"\""+"\n Action Taken :"+actionTaken+"\n Doo Cash :"+docash+"\n Comments :"+comments;
			String subject = "Missing Doo Cash of \n Name : "+user.getFirst_name() +" \n Email : "+user.getEmail()+" Reference No. :"+rCode;

			EmailSender email = new EmailSender(text, subject, Constants.EMAIL); 
			email.start();  
			System.out.println("Response : "+JSONUtils.getSuccessJson(rCode));

			return JSONUtils.getSuccessJson(rCode);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}


	}

	public String reportMissingShoppingDooCash(Long user_id, Long purchageDate, String docash, String storeName, String orderNo,
			String productName, String productPurchaseValue, String comments) throws Exception {

		Session session = getSession();
		try{
			User user = (User) dao.getbyId(session, user_id, User.class);

			purchageDate = purchageDate+Constants.GMT;   

			if(user == null)
				return JSONUtils.getFailJson("User not find.");
			DateFormat format = new SimpleDateFormat("dd-MMM-YYYY");

			Date toDate = new Date(purchageDate);
			String toDateTime = format.format(toDate);

			String rCode = "";
			rCode = ""+Constants.generateRandomString(5);

			String text = "I have not got my Shopping Doo cash of \""+toDateTime+"\""+"\n Store Name :"+storeName+"\n Doo Cash :"+docash+"\n Comments :"+comments
					+"\n Product Name :"+productName+" \nProduct Purchased vale :"+productPurchaseValue+"\n Order Number :"+orderNo;
			String subject = "Missing Doo cash of \n Name : "+user.getFirst_name() +" \n Email : "+user.getEmail()+" Reference No. :"+rCode;

			EmailSender email = new EmailSender(text, subject, Constants.EMAIL);
			email.start();
			System.out.println("Response : "+JSONUtils.getSuccessJson(rCode));

			return JSONUtils.getSuccessJson(rCode);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String contactUs(String user_id, String comments) {
		String text = new String("Message : "+comments);
		String subject = new String("Woodoo Id : "+user_id);

		EmailSender email = new EmailSender(text, subject, Constants.EMAIL);
		email.start();
		System.out.println("Response : "+JSONUtils.getSuccessJson());  
		return JSONUtils.getSuccessJson();
	} 

	public String getNotificationsOfUser(Long my_user_id) throws Exception {
		Session session = getSession();
		try{
			Map<String, Object> map = new HashMap<>();
			map.put("user_id", my_user_id);
			map.put("status", true);

			List<Notifications> notifications = dao.getbyUniqueColumnsList(session, map, Notifications.class);
			System.out.println("Response : "+JSONUtils.getSuccessJson(notifications));
			return JSONUtils.getSuccessJson(notifications);
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String giveExtraDooPoints(Long user_id, Double points, Long date) throws Exception {
		Session session = getSession();
		try
		{
			User user = (User) dao.getbyId(session, user_id, User.class);
			if(user == null)
				return JSONUtils.getFailJson("User not found.");
			Conversion conversion = new Conversion();
			conversion.setUser_id(user_id);
			conversion.setUsers_payout(points);
			conversion.setConversion_status(true);
			conversion.setConversion_time(date);
			Date date1 = new Date(date);
			conversion.setDate_time(date1);

			conversion.setStatus(WooStatus.CONFIRMED.toString());
			conversion.setOfferName("Bonus Doo Points");
			conversion.setLeft_points(conversion.getUsers_payout());
			System.out.println(conversion);
			dao.insert(session, conversion);
			Map<String, String> map = new HashMap<>();
			
			String message = "Bonus  "+conversion.getUsers_payout()+" Doo Cash added. Enjoy! Team WooDoo.";
			map.put("userId", ""+conversion.getId());
			map.put("message", message);
			map.put("type", "newDooCash");
			map.put("title", "New Doo Cash");

			Notifications notification = new Notifications();
			notification.setMessage(message);
			notification.setStatus(true);
			notification.setUser_id(user.getUser_id());
			notification.setDatetime(Timings.getCurrentTime());
			dao.insert(session, notification);

			AndroidNotifications.generalNotificationToUser(user, map);
			return JSONUtils.getSuccessJson("Extra Doo point given to users.");
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}
	
	public String giveExtraDooPoints2(Long user_id, Double points, Long date) throws Exception {
		Session session = getSession();
		try
		{
			User user = (User) dao.getbyId(session, user_id, User.class);
			if(user == null)
				return JSONUtils.getFailJson("User not found.");
			Conversion conversion = new Conversion();
			conversion.setUser_id(user_id);
			conversion.setUsers_payout(points);
			conversion.setConversion_status(true);
			conversion.setConversion_time(date);
			Date date1 = new Date(date);
			conversion.setDate_time(date1);

			conversion.setStatus(WooStatus.PENDING.toString());
			conversion.setOfferName("Bonus Pending Doo Points");
			conversion.setLeft_points(conversion.getUsers_payout());
			conversion.setType("ACTION");
			System.out.println(conversion);
			dao.insert(session, conversion);
			Map<String, String> map = new HashMap<>();
			
			String message = "Bonus  "+conversion.getUsers_payout()+" Doo Cash added. Enjoy! Team WooDoo.";
			map.put("userId", ""+conversion.getId());
			map.put("message", message);
			map.put("type", "newDooCash");
			map.put("title", "New Doo Cash");

			Notifications notification = new Notifications();
			notification.setMessage(message);
			notification.setStatus(true);
			notification.setUser_id(user.getUser_id());
			notification.setDatetime(Timings.getCurrentTime());
			dao.insert(session, notification);

			AndroidNotifications.generalNotificationToUser(user, map);
			return JSONUtils.getSuccessJson("Extra Doo point given to users.");
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String giveExtraWooPoints(Long user_id, Long points, Long date) throws Exception {
		Session session = getSession();
		try
		{
			User user = (User) dao.getbyId(session, user_id, User.class);
			if(user == null)
				return JSONUtils.getFailJson("User not found.");

			WooLog log = new WooLog();
			log.setUser_id(user_id);
			log.setDate_time(date);
			log.setExtra_point(points);
			log.setLeft_points(points);
			log.setIsBonus(true);
			
 
			dao.insert(session, log);

			Map<String, String> map = new HashMap<>();

			String message = "Bonus "+log.getExtra_point()+" Woo Cash added. Enjoy! Team WooDoo";
			map.put("userId", ""+log.getUser_id());
			map.put("message", message);
			map.put("type", "newWooCash");
			map.put("title", "New Woo Cash");

			Notifications notification = new Notifications();
			notification.setMessage(message);
			notification.setStatus(true);
			notification.setUser_id(user.getUser_id());
			notification.setDatetime(Timings.getCurrentTime());
			dao.insert(session, notification);

			AndroidNotifications.generalNotificationToUser(user, map);
			System.out.println("Response : "+JSONUtils.getSuccessJson(log));
			//return JSONUtils.getSuccessJson(log);
			return JSONUtils.getSuccessJson("Extra Woo point given to users.");
		}      
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}
	
	public String giveExtraWooPoints2(Long user_id, Long points, Long date) throws Exception {
		Session session = getSession();
		try
		{
			User user = (User) dao.getbyId(session, user_id, User.class);
			if(user == null)
				return JSONUtils.getFailJson("User not found.");

			WooLog log = new WooLog();
			log.setUser_id(user_id);
			log.setDate_time(date);
			log.setExtra_point(points);
			log.setLeft_points(points);
			log.setIsBonus(false);		

			dao.insert(session, log);

			Map<String, String> map = new HashMap<>();

			String message = "Bonus "+log.getExtra_point()+" Woo Cash added. Enjoy! Team WooDoo";
			map.put("userId", ""+log.getUser_id());
			map.put("message", message);
			map.put("type", "newWooCash");
			map.put("title", "New Woo Cash");

			Notifications notification = new Notifications();
			notification.setMessage(message);
			notification.setStatus(true);
			notification.setUser_id(user.getUser_id());
			notification.setDatetime(Timings.getCurrentTime());
			dao.insert(session, notification);

			AndroidNotifications.generalNotificationToUser(user, map);
			System.out.println("Response : "+JSONUtils.getSuccessJson(log));
			//return JSONUtils.getSuccessJson(log);
			return JSONUtils.getSuccessJson("Extra Woo point given to users.");
		}      
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String addManualCampain(String url, String description, String urlToImage, String title, Integer screen_no,
			Boolean status, Long endDate, Long startDate) throws Exception {
		Session session = getSession();
		try
		{
			
			if(screen_no == 2 || screen_no == 4 || screen_no == 6 || screen_no == 3 )
			{
				ManualAdds oldAdd = (ManualAdds) dao.getbyUniqueColumn(session, "screen_no", screen_no, ManualAdds.class);
				
				if(oldAdd != null)
					dao.delete(session, oldAdd);

				ManualAdds add = new ManualAdds();
				add.setDescription(description);
				add.setScreen_no(screen_no);
				add.setStatus(status);
				add.setTitle(title);
				add.setUrl(url);
				add.setUrlToImage(urlToImage);
				add.setEndDate(endDate);
				add.setStartDate(startDate);
				
				dao.insert(session, add);
				return JSONUtils.getSuccessJson("Added Successfully.");		
			}
			else
			{
				return JSONUtils.getFailJson("Invalid screen no. Valid no. are 2,3,4,6");
			}
		}      
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String getAllManualCampain(Long id) throws Exception {
		Session session = getSession();
		try
		{
			if(id != null)
			{
				ManualAdds add = (ManualAdds) dao.getbyId(session, id, ManualAdds.class);
				return JSONUtils.getSuccessJson(add);
			}
			else
			{
				ArrayList<ManualAdds> allAdd = dao.getall(session, ManualAdds.class);
				return JSONUtils.getSuccessJson(allAdd);
			}
		}
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}	
	}

	public String changeStatus(Long id) throws Exception {
		Session session = getSession();
		try
		{
			ManualAdds oldAdd = (ManualAdds) dao.getbyId(session, id, ManualAdds.class);
			
			if(oldAdd == null)
				return JSONUtils.getFailJson("Not found");
			
			if(oldAdd.getStatus())
				oldAdd.setStatus(false);
			else
				oldAdd.setStatus(true);
			
			dao.update(session, oldAdd);
			
			return JSONUtils.getSuccessJson(oldAdd);
		}      
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String deleteCampain(Long id) throws Exception {
		Session session = getSession();
		try
		{
			ManualAdds oldAdd = (ManualAdds) dao.getbyId(session, id, ManualAdds.class);
			
			if(oldAdd == null)
				return JSONUtils.getFailJson("Not found");
			
			dao.delete(session, oldAdd);
			
			return JSONUtils.getSuccessJson("Deleted Successfully.");
		}      
		catch (Exception e){
			e.printStackTrace();
			throw new Exception();
		}
		finally {
			session.close();
		}
	}

	public String fileUpload(MultipartFile file, String extension) {
			try 
			{
				woodoo.model.Tuple<String, String> tuple = AwsS3Utils.uploadFile(file,extension);
				if(tuple != null)
				{
					JSONObject json = new JSONObject();
					json.put("status", "success");
					json.put("url", tuple.o2);
					return json.toString();
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				JSONObject json = new JSONObject();
				json.put("status", "fail");
				json.put("url", e.toString());
				
				return json.toString();
			}
			return JSONUtils.getFailJson();
	}
}
