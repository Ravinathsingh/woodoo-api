package woodoo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MANUAL_ADDS")
public class ManualAdds {
	
	@GeneratedValue
	@Id
	Long id;
	String url;
	String description;
	String urlToImage;
	String title;
	Integer screen_no;
	Boolean status;
	Long endDate;
	Long startDate;
	
	
	public Long getStartDate() {
		return startDate;
	}
	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}
	public Long getEndDate() {
		return endDate;
	}
	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}
	public void setScreen_no(Integer screen_no) {
		this.screen_no = screen_no;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrlToImage() {
		return urlToImage;
	}
	public void setUrlToImage(String urlToImage) {
		this.urlToImage = urlToImage;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getScreen_no() {
		return screen_no;
	}
	public void setScreen_no(int screen_no) {
		this.screen_no = screen_no;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	
	@Override
	public String toString() {
		return "ManualAdds [id=" + id + ", url=" + url + ", description=" + description + ", urlToImage=" + urlToImage
				+ ", title=" + title + ", screen_no=" + screen_no + ", status=" + status + "]";
	}
	
}
