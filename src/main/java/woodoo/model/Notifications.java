package woodoo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author codeyeti
 *
 */
@Entity
@Table(name="NOTIFICATION")
public class Notifications {
	@Id
	@GeneratedValue
	Long id;
	Long user_id;
	Boolean status;
	String message;
	Long datetime;
	
	
	public Long getDatetime() {
		return datetime;
	}
	public void setDatetime(Long datetime) {
		this.datetime = datetime;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUser_id() {
		return user_id;
	}
	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Notifications [id=" + id + ", user_id=" + user_id + ", status=" + status + ", message=" + message + "]";
	}
}
