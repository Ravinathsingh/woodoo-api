package woodoo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="OTPInfo")
public class OTPInfo {

	@Id
	@GeneratedValue
	Long id;
	String brand;
	String device_id;
	String device_model;
	String mobile;
	String os_version;
	String ram;
	String screen_height;
	String screen_width;
	String source;
	String otp;
	Boolean isOtpUsed;
	
	
	
	
	
	public OTPInfo() {
		super();
	}
	public OTPInfo(String brand, String device_id, String device_model, String mobile, String os_version,
			String ram, String screen_height, String screen_width, String source, String otp, Boolean isOtpUsed) {
		super();
		this.id = id;
		this.brand = brand;
		this.device_id = device_id;
		this.device_model = device_model;
		this.mobile = mobile;
		this.os_version = os_version;
		this.ram = ram;
		this.screen_height = screen_height;
		this.screen_width = screen_width;
		this.source = source;
		this.otp = otp;
		this.isOtpUsed = isOtpUsed;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public Boolean getIsOtpUsed() {
		return isOtpUsed;
	}
	public void setIsOtpUsed(Boolean isOtpUsed) {
		this.isOtpUsed = isOtpUsed;
	}
	@Override
	public String toString() {
		return "OTPInfo [id=" + id + ", brand=" + brand + ", device_id=" + device_id + ", device_model=" + device_model
				+ ", mobile=" + mobile + ", os_version=" + os_version + ", ram=" + ram + ", screen_height="
				+ screen_height + ", screen_width=" + screen_width + ", source=" + source + "]";
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public String getDevice_model() {
		return device_model;
	}
	public void setDevice_model(String device_model) {
		this.device_model = device_model;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getOs_version() {
		return os_version;
	}
	public void setOs_version(String os_version) {
		this.os_version = os_version;
	}
	public String getRam() {
		return ram;
	}
	public void setRam(String ram) {
		this.ram = ram;
	}
	public String getScreen_height() {
		return screen_height;
	}
	public void setScreen_height(String screen_height) {
		this.screen_height = screen_height;
	}
	public String getScreen_width() {
		return screen_width;
	}
	public void setScreen_width(String screen_width) {
		this.screen_width = screen_width;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
	
	
}
