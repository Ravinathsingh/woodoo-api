package woodoo.model;


public class Tuple<T1, T2> {

	public T1 o1;
	public T2 o2;

	public Tuple() {
		super();
	}

	public Tuple(T1 o1, T2 o2) {
		super();
		this.o1 = o1;
		this.o2 = o2;
	}

	@Override
	public String toString() {
		return "Tuple [o1=" + o1 + ", o2=" + o2 + "]";
	}

}
