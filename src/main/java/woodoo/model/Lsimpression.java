package woodoo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ls_impressions")
public class Lsimpression 
{
	@GeneratedValue
	@Id
	Long id;
	Long user_id;
	Long phone_time;;
	String status;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUser_id() {
		return user_id;
	}
	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	public Long getPhone_time() {
		return phone_time;
	}
	public void setPhone_time(Long phone_time) {
		this.phone_time = phone_time;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
}
