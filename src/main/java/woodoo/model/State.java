package woodoo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="STATE")
public class State  {

	@Id
	@GeneratedValue
	@Column(name="id")
	@NotNull
	Long id;
	
	Long country_id;
	
	String state_name;
	
	String capital_city;
	
	String largest_city;
	
	String area;
	
	String pin_code;
	
	String abbreviation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCountry_id() {
		return country_id;
	}

	public void setCountry_id(Long country_id) {
		this.country_id = country_id;
	}

	public String getState_name() {
		return state_name;
	}

	public void setState_name(String state_name) {
		this.state_name = state_name;
	}

	public String getCapital_city() {
		return capital_city;
	}

	public void setCapital_city(String capital_city) {
		this.capital_city = capital_city;
	}

	public String getLargest_city() {
		return largest_city;
	}

	public void setLargest_city(String largest_city) {
		this.largest_city = largest_city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getPin_code() {
		return pin_code;
	}

	public void setPin_code(String pin_code) {
		this.pin_code = pin_code;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	
}