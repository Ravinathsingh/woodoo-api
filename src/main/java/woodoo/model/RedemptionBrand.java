package woodoo.model;

import java.util.Arrays;
import java.util.Comparator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="RedemptionBrand")
public class RedemptionBrand {
	@Id
	@GeneratedValue
	Long id;
	String hash;	
	String name;
    String description;
    String terms;
    String image;
    String isActive;
    String isOnline;
    String website;
    String type;
    String denominationsAll;
    Boolean ourStatus;
    String denominations;
    Long amount;
    String skuId;
    
    public RedemptionBrand()
    {
    	
    }
    
    
//	public RedemptionBrand(Long id, String hash, String name, String description, String terms, String image,
//			String isActive, String isOnline, String website, String type, String denominationsAll, Boolean ourStatus,
//			String denominations, Long amount, String skuId) {
//		
//		this.id = id;
//		this.hash = hash;
//		this.name = name;
//		this.description = description;
//		this.terms = terms;
//		this.image = image;
//		this.isActive = isActive;
//		this.isOnline = isOnline;
//		this.website = website;
//		this.type = type;
//		this.denominationsAll = denominationsAll;
//		this.ourStatus = ourStatus;
//		this.denominations = denominations;
//		this.amount = amount;
//		this.skuId = skuId;
//	}
	public String getSkuId() {
		return skuId;
	}
	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getDenominationsAll() {
		return denominationsAll;
	}
	public void setDenominationsAll(String denominationsAll) {
		this.denominationsAll = denominationsAll;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDenominations() {
		return denominations;
	}
	public void setDenominations(String denominations) {
		this.denominations = denominations;
	}
	public Boolean getOurStatus() {
		return ourStatus;
	}
	public void setOurStatus(Boolean ourStatus) {
		this.ourStatus = ourStatus;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTerms() {
		return terms;
	}
	public void setTerms(String terms) {
		this.terms = terms;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getIsOnline() {
		return isOnline;
	}
	public void setIsOnline(String isOnline) {
		this.isOnline = isOnline;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
 	public static Comparator<RedemptionBrand> sortByAmount = new Comparator<RedemptionBrand>() {
		
		@Override
		public int compare(RedemptionBrand o1, RedemptionBrand o2) {
			
			return o1.getAmount().compareTo(o2.getAmount());  
		}
	};
	
}
