package woodoo.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="WOO_LOG")
public class WooLog {
	@Id
	@GeneratedValue
	Long id;
	Long user_id;
	Long woo_point;
	Long date_time;
	Long extra_point;
	Long left_points;
	Boolean isBonus;
	
	
	
	public Boolean getIsBonus() {
		return isBonus;
	}
	public void setIsBonus(Boolean isBonus) {
		this.isBonus = isBonus;
	}
	public Long getLeft_points() {
		return left_points;
	}
	public void setLeft_points(Long left_points) {
		this.left_points = left_points;
	}
	public Long getExtra_point() {
		return extra_point;
	}
	public void setExtra_point(Long extra_point) {
		this.extra_point = extra_point;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUser_id() {
		return user_id;
	}
	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	public Long getWoo_point() {
		return woo_point;
	}
	public void setWoo_point(Long woo_point) {
		this.woo_point = woo_point;
	}
	public Long getDate_time() {
		return date_time;
	}
	public void setDate_time(Long date_time) {
		this.date_time = date_time;
	}
	@Override
	public String toString() {
		return "WooLog [id=" + id + ", user_id=" + user_id + ", woo_point=" + woo_point + ", date_time=" + date_time
				+ ", extra_point=" + extra_point + ", left_points=" + left_points + ", isBonus=" + isBonus + "]";
	}
	
	
}
