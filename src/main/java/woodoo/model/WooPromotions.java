package woodoo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="WOO_PROMOTIONS")
public class WooPromotions
{
	@Id
	@GeneratedValue
	Long id;
	Long start_time;
	Long end_time;
	Long extra_percentage;
	String message;
	
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Long getExtra_percentage() {
		return extra_percentage;
	}
	public void setExtra_percentage(Long extra_percentage) {
		this.extra_percentage = extra_percentage;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getStart_time() {
		return start_time;
	}
	public void setStart_time(Long start_time) {
		this.start_time = start_time;
	}
	public Long getEnd_time() {
		return end_time;
	}
	public void setEnd_time(Long end_time) {
		this.end_time = end_time;
	}
	
	@Override
	public String toString() {
		return "WooPromotions [id=" + id + ", start_time=" + start_time + ", end_time=" + end_time + "]";
	}
}
