package woodoo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="shopping_url")
public class Shopping_url {
	
	@GeneratedValue
	@Id
	Integer url_id;
	String url;
	
	public Integer getUrl_id() {
		return url_id;
	}
	public void setUrl_id(Integer url_id) {
		this.url_id = url_id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

}
