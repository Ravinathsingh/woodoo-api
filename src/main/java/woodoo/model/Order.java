package woodoo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ORDERTABLE")
public class Order {
	@Id
	@GeneratedValue
	Long id;
	Long userId;
	String accessToken;
	String skuId;
	String clientOrderId;
	String orderDate;
	String billingName;
	String billingEmail;
	String billingAddressLine1;
	String billingAddressLine2;
	String billingCompany;
	String billingCity;
	String billingState;
	String billingCountry;
	String billingZip;
	String billingMobile;
	String billingPhone;
	String paymentMode;
	String paymentStatus;
	String lineItemQuantity;
	String shippingCharge;
	String taxes;
	String receiversName;
	String shippingAddressLine1;
	String shippingAddressLine2;
	String shippingCity;
	String shippingState;
	String shippingCountry;
	String shippingZip;
	String shippingMobile;
	String shippingPhone;
	String products;
	String amount;
	String coupon;
	 String image;
//	@Transient
	Long brand_id;
//	@Transient
	String brand_name;
//	@Transient
	String type;
//	@Transient
	String website;
	
	
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getBrand_name() {
		return brand_name;
	}
	public void setBrand_name(String brand_name) {
		this.brand_name = brand_name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public Long getBrand_id() {
		return brand_id;
	}
	public void setBrand_id(Long brand_id) {
		this.brand_id = brand_id;
	}
	public String getCoupon() {
		return coupon;
	}
	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getSkuId() {
		return skuId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}
	public String getClientOrderId() {
		return clientOrderId;
	}
	public void setClientOrderId(String clientOrderId) {
		this.clientOrderId = clientOrderId;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public String getBillingName() {
		return billingName;
	}
	public void setBillingName(String billingName) {
		this.billingName = billingName;
	}
	public String getBillingEmail() {
		return billingEmail;
	}
	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}
	public String getBillingAddressLine1() {
		return billingAddressLine1;
	}
	public void setBillingAddressLine1(String billingAddressLine1) {
		this.billingAddressLine1 = billingAddressLine1;
	}
	public String getBillingAddressLine2() {
		return billingAddressLine2;
	}
	public void setBillingAddressLine2(String billingAddressLine2) {
		this.billingAddressLine2 = billingAddressLine2;
	}
	public String getBillingCompany() {
		return billingCompany;
	}
	public void setBillingCompany(String billingCompany) {
		this.billingCompany = billingCompany;
	}
	public String getBillingCity() {
		return billingCity;
	}
	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}
	public String getBillingState() {
		return billingState;
	}
	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}
	public String getBillingCountry() {
		return billingCountry;
	}
	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}
	public String getBillingZip() {
		return billingZip;
	}
	public void setBillingZip(String billingZip) {
		this.billingZip = billingZip;
	}
	public String getBillingMobile() {
		return billingMobile;
	}
	public void setBillingMobile(String billingMobile) {
		this.billingMobile = billingMobile;
	}
	public String getBillingPhone() {
		return billingPhone;
	}
	public void setBillingPhone(String billingPhone) {
		this.billingPhone = billingPhone;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getLineItemQuantity() {
		return lineItemQuantity;
	}
	public void setLineItemQuantity(String lineItemQuantity) {
		this.lineItemQuantity = lineItemQuantity;
	}
	public String getShippingCharge() {
		return shippingCharge;
	}
	public void setShippingCharge(String shippingCharge) {
		this.shippingCharge = shippingCharge;
	}
	public String getTaxes() {
		return taxes;
	}
	public void setTaxes(String taxes) {
		this.taxes = taxes;
	}
	public String getReceiversName() {
		return receiversName;
	}
	public void setReceiversName(String receiversName) {
		this.receiversName = receiversName;
	}
	public String getShippingAddressLine1() {
		return shippingAddressLine1;
	}
	public void setShippingAddressLine1(String shippingAddressLine1) {
		this.shippingAddressLine1 = shippingAddressLine1;
	}
	public String getShippingAddressLine2() {
		return shippingAddressLine2;
	}
	public void setShippingAddressLine2(String shippingAddressLine2) {
		this.shippingAddressLine2 = shippingAddressLine2;
	}
	public String getShippingCity() {
		return shippingCity;
	}
	public void setShippingCity(String shippingCity) {
		this.shippingCity = shippingCity;
	}
	public String getShippingState() {
		return shippingState;
	}
	public void setShippingState(String shippingState) {
		this.shippingState = shippingState;
	}
	public String getShippingCountry() {
		return shippingCountry;
	}
	public void setShippingCountry(String shippingCountry) {
		this.shippingCountry = shippingCountry;
	}
	public String getShippingZip() {
		return shippingZip;
	}
	public void setShippingZip(String shippingZip) {
		this.shippingZip = shippingZip;
	}
	public String getShippingMobile() {
		return shippingMobile;
	}
	public void setShippingMobile(String shippingMobile) {
		this.shippingMobile = shippingMobile;
	}
	public String getShippingPhone() {
		return shippingPhone;
	}
	public void setShippingPhone(String shippingPhone) {
		this.shippingPhone = shippingPhone;
	}
	public String getProducts() {
		return products;
	}
	public void setProducts(String products) {
		this.products = products;
	}
	@Override
	public String toString() {
		return "Order [id=" + id + ", accessToken=" + accessToken + ", skuId=" + skuId + ", clientOrderId="
				+ clientOrderId + ", orderDate=" + orderDate + ", billingName=" + billingName + ", billingEmail="
				+ billingEmail + ", billingAddressLine1=" + billingAddressLine1 + ", billingAddressLine2="
				+ billingAddressLine2 + ", billingCompany=" + billingCompany + ", billingCity=" + billingCity
				+ ", billingState=" + billingState + ", billingCountry=" + billingCountry + ", billingZip=" + billingZip
				+ ", billingMobile=" + billingMobile + ", billingPhone=" + billingPhone + ", paymentMode=" + paymentMode
				+ ", paymentStatus=" + paymentStatus + ", lineItemQuantity=" + lineItemQuantity + ", shippingCharge="
				+ shippingCharge + ", taxes=" + taxes + ", receiversName=" + receiversName + ", shippingAddressLine1="
				+ shippingAddressLine1 + ", shippingAddressLine2=" + shippingAddressLine2 + ", shippingCity="
				+ shippingCity + ", shippingState=" + shippingState + ", shippingCountry=" + shippingCountry
				+ ", shippingZip=" + shippingZip + ", shippingMobile=" + shippingMobile + ", shippingPhone="
				+ shippingPhone + ", products=" + products + "]";
	}
}
