package woodoo.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author codeyeti
 *
 */
@Entity
@Table(name="CONVERSION_DATA")
public class Conversion {
	@Id
	@GeneratedValue
	Long id;
	String offerName;
	Long user_id;
    @Temporal(TemporalType.TIMESTAMP)
	Date date_time;
    Long offer_id;
    Double approved_payout;
    Boolean conversion_status;
    Long conversion_time;
	Long conversion_id;
	String type;//shopping or action or referral
	Double users_payout;
	String status;
	Double left_points;
	Integer hour;
	Long uniqueId;
    
	
	public Double getLeft_points() {
		return left_points;
	}
	public void setLeft_points(Double left_points) {
		this.left_points = left_points;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Double getUsers_payout() {
		return users_payout;
	}
	public void setUsers_payout(Double users_payout) {
		this.users_payout = users_payout;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getConversion_id() {
		return conversion_id;
	}
	public void setConversion_id(Long conversion_id) {
		this.conversion_id = conversion_id;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOfferName() {
		return offerName;
	}
	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}
	public Long getUser_id() {
		return user_id;
	}
	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	public Date getDate_time() {
		return date_time;
	}
	public void setDate_time(Date date_time) {
		this.date_time = date_time;
	}
	public Long getOffer_id() {
		return offer_id;
	}
	public void setOffer_id(Long offer_id) {
		this.offer_id = offer_id;
	}
	public Double getApproved_payout() {
		return approved_payout;
	}
	public void setApproved_payout(Double approved_payout) {
		this.approved_payout = approved_payout;
	}
	public Boolean getConversion_status() {
		return conversion_status;
	}
	public void setConversion_status(Boolean conversion_status) {
		this.conversion_status = conversion_status;
	}
	public Long getConversion_time() {
		return conversion_time;
	}
	public void setConversion_time(Long conversion_time) {
		this.conversion_time = conversion_time;
	}
	@Override
	public String toString() {
		return "Conversion [id=" + id + ", offerName=" + offerName + ", user_id=" + user_id + ", date_time=" + date_time
				+ ", offer_id=" + offer_id + ", approved_payout=" + approved_payout + ", conversion_status="
				+ conversion_status + ", conversion_time=" + conversion_time + ", conversion_id=" + conversion_id
				+ ", type=" + type + ", users_payout=" + users_payout + ", status=" + status + ", left_points="
				+ left_points + "]";
	}
	public Integer getHour() {
		return hour;
	}
	public void setHour(Integer hour) {
		this.hour = hour;
	}
	public Long getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(Long uniqueId) {
		this.uniqueId = uniqueId;
	}

	
}
