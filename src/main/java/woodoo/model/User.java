package woodoo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="USER")
public class User {
	@Id
	@GeneratedValue
	Long user_id;
	String first_name;
	String last_name;
	Long dob;
	String gender;
	String email;
	String mobile_no;
	String password;
	Long phone_value;
	String state;
	String city;
	String pin_code;
	String address_one;
	String address_two;
	String membership_plan;
	String facebook_id;
	String my_referral_code;
	String used_referral_code;
	String gcm_id;
	
	Long age;
	Long signUpDate;
	Boolean status;
	String otherCityName="";
	
	String device_id;
	Boolean nstatus;
	
	
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Long getAge() {
		return age;
	}
	public void setAge(Long age) {
		this.age = age;
	}
	public Long getSignUpDate() {
		return signUpDate;
	}
	public void setSignUpDate(Long signUpDate) {
		this.signUpDate = signUpDate;
	}
	public String getGcm_id() {
		return gcm_id;
	}
	public void setGcm_id(String gcm_id) {
		this.gcm_id = gcm_id;
	}
	public String getMy_referral_code() {
		return my_referral_code;
	}
	public void setMy_referral_code(String my_referral_code) {
		this.my_referral_code = my_referral_code;
	}
	public String getDevice_id() {
		return device_id;
	}
	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}
	public String getUsed_referral_code() {
		return used_referral_code;
	}
	public void setUsed_referral_code(String used_referral_code) {
		this.used_referral_code = used_referral_code;
	}
	public Long getUser_id() {
		return user_id;
	}
	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public Long getDob() {
		return dob;
	}
	public void setDob(Long dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile_no() {
		return mobile_no;
	}
	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Long getPhone_value() {
		return phone_value;
	}
	public void setPhone_value(Long phone_value) {
		this.phone_value = phone_value;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPin_code() {
		return pin_code;
	}
	public void setPin_code(String pin_code) {
		this.pin_code = pin_code;
	}
	public String getAddress_one() {
		return address_one;
	}
	public void setAddress_one(String address_one) {
		this.address_one = address_one;
	}
	public String getAddress_two() {
		return address_two;
	}
	public void setAddress_two(String address_two) {
		this.address_two = address_two;
	}
	public String getMembership_plan() {
		return membership_plan;
	}
	public void setMembership_plan(String membership_plan) {
		this.membership_plan = membership_plan;
	}
	public String getFacebook_id() {
		return facebook_id;
	}
	public void setFacebook_id(String facebook_id) {
		this.facebook_id = facebook_id;
	}
	
	public String getOtherCityName() {
		return otherCityName;
	}
	public void setOtherCityName(String otherCityName) {
		this.otherCityName = otherCityName;
	}
	public Boolean getNstatus() {
		return nstatus;
	}
	public void setNstatus(Boolean nstatus) {
		this.nstatus = nstatus;
	}
	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", first_name=" + first_name + ", last_name=" + last_name + ", dob=" + dob
				+ ", gender=" + gender + ", email=" + email + ", mobile_no=" + mobile_no + ", password=" + password
				+ ", phone_value=" + phone_value + ", state=" + state + ", city=" + city + ", pin_code=" + pin_code
				+ ", address_one=" + address_one + ", address_two=" + address_two + ", membership_plan="
				+ membership_plan + ", facebook_id=" + facebook_id + ", my_referral_code=" + my_referral_code
				+ ", used_referral_code=" + used_referral_code + "]";
	}
	
	
}
