package woodoo.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import woodoo.service.DooService;

@Controller
public class DemoController
{
	@Autowired
	DooService dooService;
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	@ResponseBody
	public String rootController()
	{
		return "Hello";
	}
	@RequestMapping(value="/get_paytm_pending_data", method=RequestMethod.GET)
	public ResponseEntity<String> createUpdateAdaptive0() throws Exception
	{
		//Logger.geturl();
		System.out.println("chanadn kumar");
		return new ResponseEntity<String>(dooService.getpaytmpendingData(),HttpStatus.OK);
	}
	@RequestMapping(value="/get_paytm_confirm_data", method=RequestMethod.GET)
	public ResponseEntity<String> createUpdateAdaptive01() throws Exception
	{
		//Logger.geturl();
		System.out.println("chanadn kumar");
		return new ResponseEntity<String>(dooService.getpaytmconfirmData(),HttpStatus.OK);
	}
	@RequestMapping(value="/update_paytm_status", method=RequestMethod.GET)
	public ResponseEntity<String> createUpdateAdaptive100(@RequestParam String id) throws Exception
	{
		//Logger.geturl();
		System.out.println("chanadn kumar");
		return new ResponseEntity<String>(dooService.updatestatuspaytm(id),HttpStatus.OK);
	}
	@RequestMapping(value="/url_all", method=RequestMethod.GET)
	public ResponseEntity<String> createUpdateAdaptive() throws Exception
	{
		//Logger.geturl();
		System.out.println("chanadn kumar");
		return new ResponseEntity<String>(dooService.getData(),HttpStatus.OK);
	}
	@RequestMapping(value="/url_insert", method=RequestMethod.GET)
	public ResponseEntity<String> createUpdateAdaptive2(@RequestParam String url) throws Exception
	{
		//Logger.geturl();
		System.out.println("chanadn kumar");
		return new ResponseEntity<String>(dooService.InsertData(url),HttpStatus.OK);
	}
	@RequestMapping(value="/url_delete", method=RequestMethod.GET)
	public ResponseEntity<String> createUpdateAdaptive3(@RequestParam Integer id) throws Exception
	{
		//Logger.geturl();
		System.out.println("chanadn kumar");
		return new ResponseEntity<String>(dooService.DeleteData(id),HttpStatus.OK);
	}
	@RequestMapping(value="/url_action_all", method=RequestMethod.GET)
	public ResponseEntity<String> createUpdateAdaptive4() throws Exception
	{
		//Logger.geturl();
		System.out.println("chanadn kumar");
		return new ResponseEntity<String>(dooService.getActionData(),HttpStatus.OK);
	}
	@RequestMapping(value="/url_action_insert", method=RequestMethod.GET)
	public ResponseEntity<String> createUpdateAdaptive5(@RequestParam String url) throws Exception
	{
		//Logger.geturl();
		System.out.println("chanadn kumar");
		return new ResponseEntity<String>(dooService.InsertActionData(url),HttpStatus.OK);
	}
	@RequestMapping(value="/url_action_delete", method=RequestMethod.GET)
	public ResponseEntity<String> createUpdateAdaptive6(@RequestParam Integer id) throws Exception
	{
		//Logger.geturl();
		System.out.println("chanadn kumar");
		return new ResponseEntity<String>(dooService.DeleteActionData(id),HttpStatus.OK);
	}
	@RequestMapping(value="/get-list-notifications", method=RequestMethod.GET)
	public ResponseEntity<String> createUpdateAdaptive7() throws Exception
	{
		//Logger.geturl();
		System.out.println("chanadn kumar");
		return new ResponseEntity<String>(dooService.getNotificationData(),HttpStatus.OK);
	}
	@RequestMapping(value="/send-notifications-v3", method=RequestMethod.POST)
	public ResponseEntity<String> createUpdateAdaptive11(@RequestParam String title,@RequestParam String type, @RequestParam String url, @RequestParam String userid,@RequestParam String message) throws Exception
	{
		//Logger.geturl();
		System.out.println("chanadn kumar");
		return new ResponseEntity<String>(dooService.SendNotifiData(title,type,url,userid,message),HttpStatus.OK);
	}
	@RequestMapping(value="/send-notifications-v2", method=RequestMethod.POST)
	public ResponseEntity<String> createUpdateAdaptive8(@RequestParam String id,@RequestParam String title, @RequestParam String startDate, @RequestParam String endDate,@RequestParam String time , @RequestParam String message, @RequestParam String addToNotification,@RequestParam String url,@RequestParam String type,@RequestParam String ids) throws Exception
	{
		//Logger.geturl();
		System.out.println("chanadn kumar");
		return new ResponseEntity<String>(dooService.InsertNotifiData(id,title,startDate,endDate,time ,message,addToNotification,url,type,ids),HttpStatus.OK);
	}
	@RequestMapping(value="/delete-deactivate-notifications", method=RequestMethod.GET)
	public ResponseEntity<String> createUpdateAdaptive9(@RequestParam Integer id) throws Exception
	{
		//Logger.geturl();
		System.out.println("chanadn kumar");
		return new ResponseEntity<String>(dooService.DeleteNotifiData(id),HttpStatus.OK);
	}
	@RequestMapping(value="/acivate-deactivate-notifications", method=RequestMethod.GET)
	public ResponseEntity<String> createUpdateAdaptive10(@RequestParam Integer id) throws Exception
	{
		//Logger.geturl();
		System.out.println("chanadn kumar");
		return new ResponseEntity<String>(dooService.updatestatusnoData(id),HttpStatus.OK);
	}
	
	@RequestMapping(value="/send-notifications-v4", method=RequestMethod.GET)
	public ResponseEntity<String> createUpdateAdaptive13() throws Exception
	{
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		formatter.setTimeZone(TimeZone.getTimeZone("IST"));		 	  
			String strDate = formatter.format(date);
			SimpleDateFormat formatter2 = new SimpleDateFormat("HH");
			formatter2.setTimeZone(TimeZone.getTimeZone("IST"));
			String time = formatter2.format(date);
		//Logger.geturl();
		System.out.println("chanadn kumar");
		
		dooService.sendNotificationAdmin();		
		return new ResponseEntity<String>("Updated..."+strDate+"text"+time,HttpStatus.OK);
	}
	
}