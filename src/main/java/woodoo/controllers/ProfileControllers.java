package woodoo.controllers;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.google.gson.Gson;

import woodoo.model.User;
import woodoo.service.ProfileService;
import woodoo.utils.JSONUtils;
import woodoo.utils.Logger;
import woodoo.utils.Timings;

@Controller
public class ProfileControllers {

	@Autowired
	ProfileService profileService;
	
	@RequestMapping(value="/demo", method=RequestMethod.GET)
	public ResponseEntity<String> createUpdateAdaptive()
	{
		//Logger.geturl();
		System.out.println("demo");
		Timings.printCurrentTime();
		return new ResponseEntity<String>("Hellow world",HttpStatus.OK);
	}
	
	@RequestMapping(value="user/signup", method=RequestMethod.POST)
	public ResponseEntity<String> userSignUp(@ModelAttribute User user) throws Exception{
		Logger.geturl();
		System.out.println("user signup");
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.signUp(user),HttpStatus.OK);
	}
	
	@RequestMapping(value="change-user-status", method=RequestMethod.GET)
	public ResponseEntity<String> changeUserStatus(@RequestParam Long user_id) throws Exception{
		Logger.geturl();
		System.out.println("user signup");
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.changeUserStatus(user_id),HttpStatus.OK);
	}
	@RequestMapping(value="change-user-nstatus", method=RequestMethod.GET)
	public ResponseEntity<String> changeUsernStatus(@RequestParam Long user_id) throws Exception{
		Logger.geturl();
		System.out.println("user signup");
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.changeUsernStatus(user_id),HttpStatus.OK);
	}
	
	@RequestMapping(value="delete-user", method=RequestMethod.GET)
	public ResponseEntity<String> deleteUser(@RequestParam Long user_id) throws Exception{
		Logger.geturl();
		System.out.println("user signup");
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.deleteUser(user_id),HttpStatus.OK);
	}
	
	@RequestMapping(value="user/profile-update", method=RequestMethod.POST)
	public ResponseEntity<String> userProfileUpdate(@ModelAttribute User user) throws Exception{
		Logger.geturl();
		System.out.println("user signup");
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.userProfileUpdate(user),HttpStatus.OK);
	}
	
	@RequestMapping(value="get-user-by-id", method=RequestMethod.GET)
	public ResponseEntity<String> getUserById(@RequestParam Long user_id) throws Exception{
		Logger.geturl();
		System.out.println("user signup");
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.getUserById(user_id),HttpStatus.OK);
	}
	
	@RequestMapping(value="user/login/", method=RequestMethod.POST)
	public ResponseEntity<String> userLogin(@RequestParam String login_type, @RequestParam(required=false) String email, @RequestParam String facebook_id, @RequestParam String password, @RequestParam String fcm_token) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("user login "+login_type+" email "+email+" facebook_id "+facebook_id+" password "+password);
		return new ResponseEntity<String>(profileService.userLogin(login_type,email,facebook_id, password,fcm_token),HttpStatus.OK);
	}
	
	@RequestMapping(value="user/logout/", method=RequestMethod.POST)
	public ResponseEntity<String> logout(@RequestParam Long my_user_id) throws Exception
	{
		Logger.geturl();
		System.out.println("user signup");
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.logout(my_user_id),HttpStatus.OK);
	}
	
	@RequestMapping(value="user/update-gcm", method=RequestMethod.POST)
	public ResponseEntity<String> updateGCM(@RequestParam Long my_user_id ,@RequestParam String gcm_id) throws Exception
	{
		Logger.geturl();
		System.out.println("user signup");
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.updateGCM(my_user_id, gcm_id),HttpStatus.OK);
	}
	
	@RequestMapping(value="user/insert-usr-download", method=RequestMethod.POST)
	public ResponseEntity<String> UdateUSer(@RequestParam String device_id ,@RequestParam String gcm_id,@RequestParam String mob ,@RequestParam String email) throws Exception
	{
		Logger.geturl();
		System.out.println("user signup");
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.InsertUsrData(device_id ,gcm_id,mob ,email),HttpStatus.OK);
	}
	
	@RequestMapping(value="otp/sender", method=RequestMethod.POST)
	public ResponseEntity<String> otpSender(@RequestParam String type, @RequestParam(required=false) String email, @RequestParam String mobile_no) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("otp sender "+type+" email : "+email+" mobile_no : "+mobile_no);
		return new ResponseEntity<String>(profileService.otpsender(type,email,mobile_no),HttpStatus.OK);
	}
	
	
	@RequestMapping(value="otp/senderV2", method=RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<String> otpSenderV2(@RequestBody Map<String, Object> request) throws Exception
	{
		
//		{"brand":"samsung","device_id":"","device_model":"","mobile":"","os_version":"","ram":,"screen_height":
//		,"screen_width":720,"source":1}
		
//		Logger.geturl();
		Timings.printCurrentTime();
		
		System.out.println(new Gson().toJson(request));
		return new ResponseEntity<String>(profileService.otpsenderV2(request),HttpStatus.OK);
	}

	@RequestMapping(value="/otp/conform", method=RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<String> otpVerify(@RequestBody Map<String, Object> request) throws Exception
	{
		
//		{"brand":"samsung","device_id":"","device_model":"","mobile":"","os_version":"","ram":,"screen_height":
//		,"screen_width":720,"source":1}
		
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println(new Gson().toJson(request));
		return new ResponseEntity<String>(profileService.otpVerify(request),HttpStatus.OK);
	}
	
	
	
	
	@RequestMapping(value="forget-password", method=RequestMethod.POST)
	public ResponseEntity<String> forgatPassword(@RequestParam String type, @RequestParam(required=false) String email, @RequestParam String mobile_no) throws Exception
	{
		Logger.geturl();
		System.out.println("forget password");
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.forgetPassword(type,email,mobile_no),HttpStatus.OK);
	}
	
	@RequestMapping(value="change-password", method=RequestMethod.POST)
	public ResponseEntity<String> forgatPassword(@RequestParam Long user_id,@RequestParam(required=false) String old_password, @RequestParam(required=false) String new_password, @RequestParam(required=false) String gcm_id) throws Exception
	{
		Logger.geturl();
		System.out.println("forget password"); 
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.changePassword(user_id, old_password,new_password, gcm_id),HttpStatus.OK);
	}
	
	@RequestMapping(value="get-states-of-india", method=RequestMethod.GET)
	public ResponseEntity<String> getState() throws Exception 
	{
		Logger.geturl();
		System.out.println("get states");
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.getStates(),HttpStatus.OK);
	}
	
	@RequestMapping(value="get-cities-of-state", method=RequestMethod.GET)
	public ResponseEntity<String> getCity(@RequestParam Long state_id) throws Exception 
	{
		Logger.geturl();
		System.out.println("get cities");
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.getCities(state_id),HttpStatus.OK);
	}
	
	@RequestMapping(value="search-user", method=RequestMethod.POST)
	public ResponseEntity<String> searchUsers(@RequestParam(required=false) String email, @RequestParam(required=false) Long pageNo) throws Exception
	{
		Logger.geturl();
		System.out.println("Email : "+email+" Next key : "+pageNo);
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.searchUser(email, pageNo), HttpStatus.OK);
	}
	
	@RequestMapping(value="user-woolog",method={RequestMethod.GET}, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<String> downloadWooLog(@RequestParam Long user_id, HttpServletResponse response) throws IOException 
	{
			Timings.printCurrentTime();
			return new ResponseEntity<String>(profileService.getWooLog(user_id,response),HttpStatus.OK);
	}
	
	@RequestMapping(value="user-doo",method={RequestMethod.GET}, produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<String> downloadDooLog(@RequestParam Long user_id, HttpServletResponse response) throws IOException 
	{
		Timings.printCurrentTime();
			return new ResponseEntity<String>(profileService.getDooLog(user_id,response),HttpStatus.OK);
	}
	
	@RequestMapping(value="send-notifications", method={RequestMethod.POST})
	public ResponseEntity<String> sendNotifications(@RequestParam(required=false) String message, @RequestParam(required=false) String title) throws Exception
	{
		Logger.geturl();
		System.out.println("Send Notifications ");
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.sendNotification(message, title), HttpStatus.OK);
	}

	@RequestMapping(value="admin-login", method={RequestMethod.POST})
	public ResponseEntity<String> adminLogin(@RequestParam(required=false) String username, @RequestParam(required=false) String password) throws Exception
	{
		Logger.geturl();
		System.out.println("Login : "+username+" Password: "+password);
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.adminLogin(username, password), HttpStatus.OK);
	}

	@RequestMapping(value="appVersionCheck", method={RequestMethod.POST})
	public ResponseEntity<String> updateApp(@RequestParam String version) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.appVersion(version), HttpStatus.OK);
	}
	
	@RequestMapping(value="download-user-csv", method={RequestMethod.GET})
	public ResponseEntity<String> downloadUserCsv(@RequestParam long admin_id,HttpServletResponse response,String statdate,String enddate,String report,String starttime,String endtime) throws Exception
	{
		Logger.geturl();
		System.out.println("Admin Id : "+admin_id); 
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.downloadUserCsv(admin_id,response,statdate,enddate,report,starttime,endtime), HttpStatus.OK);
	}
	@RequestMapping(value="download-paytm-csv", method={RequestMethod.GET})
	public ResponseEntity<String> downloadpaytmCsvR(@RequestParam long admin_id,HttpServletResponse response) throws Exception
	{
		Logger.geturl();
		System.out.println("Admin Id : "+admin_id); 
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.downloadPaytmCsvR(admin_id,response), HttpStatus.OK);
	}
	@RequestMapping(value="download-paytm-csv-confirm", method={RequestMethod.GET})
	public ResponseEntity<String> downloadpaytmCsvC(@RequestParam long admin_id,HttpServletResponse response,String statdate,String enddate) throws Exception
	{
		Logger.geturl();
		System.out.println("Admin Id : "+admin_id); 
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.downloadPayTMCsvC(admin_id,response,statdate,enddate), HttpStatus.OK);
	}
	

	@RequestMapping(value="script", method={RequestMethod.GET})
	public ResponseEntity<String> scripr() throws Exception
	{
		Logger.geturl();
		System.out.println("Admin Id : "); 
		Timings.printCurrentTime();
		return new ResponseEntity<String>(profileService.script(), HttpStatus.OK);
	}
	
}