package woodoo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import woodoo.service.RedemptionService;
import woodoo.utils.Timings;

@Controller
public class RedemptionController {
	
	@Autowired
	RedemptionService redemptionService;
	
	@RequestMapping(value="/get-brand-list", method=RequestMethod.GET)
	public ResponseEntity<String> getBrandList(@RequestParam(required=false) Long my_user_id) throws Exception
	{
		System.out.println("get-brand-list");
		Timings.printCurrentTime();
		return new ResponseEntity<String>(redemptionService.getBrandList(my_user_id),HttpStatus.OK);
	}
	
	@RequestMapping(value="/place-order-multiple", method=RequestMethod.GET)
	public ResponseEntity<String> order(@RequestParam String brandIds,@RequestParam String myChoiceMoneyValue,@RequestParam Long my_user_id) throws Exception
	{
		//Logger.geturl();
		System.out.println("/place-order-multiple "+brandIds);
		Timings.printCurrentTime();
		Double myChoiceMoneyValueString = Double.parseDouble(myChoiceMoneyValue);
		Long myChoiceMoneyValueLong =  (new Double(myChoiceMoneyValueString)).longValue();
		System.out.println("double=" + myChoiceMoneyValue + ", long=" + myChoiceMoneyValueLong);
		
		return new ResponseEntity<String>(redemptionService.placeOrder(brandIds,myChoiceMoneyValueLong,my_user_id),HttpStatus.OK);
	}
	
	@RequestMapping(value="/place-order-paytm", method=RequestMethod.GET)
	public ResponseEntity<String> order(@RequestParam String myChoiceMoneyValue,@RequestParam Long my_user_id,@RequestParam String mob_no) throws Exception
	{
		//Logger.geturl();
		Timings.printCurrentTime();
		Double myChoiceMoneyValueString = Double.parseDouble(myChoiceMoneyValue);
		Long myChoiceMoneyValueLong =  (new Double(myChoiceMoneyValueString)).longValue();
		System.out.println("double=" + myChoiceMoneyValue + ", long=" + myChoiceMoneyValueLong);
		
		return new ResponseEntity<String>(redemptionService.placeOrder(myChoiceMoneyValueLong,my_user_id,mob_no),HttpStatus.OK);
	}
	
	@RequestMapping(value="/get-order-of-user", method=RequestMethod.POST)
	public ResponseEntity<String> getOrdersOfUser(@RequestParam Long user_id ) throws Exception
	{
		//Logger.geturl();
		System.out.println("/get-order-of-user "+user_id);
		Timings.printCurrentTime();
		return new ResponseEntity<String>(redemptionService.getOrdersOfUser(user_id),HttpStatus.OK);
	}
	
	@RequestMapping(value="/update-brands", method=RequestMethod.GET)
	public ResponseEntity<String> order() throws Exception
	{
		//Logger.geturl();
		System.out.println("/update-brands ");
		Timings.printCurrentTime();
		redemptionService.scheduler();
		return new ResponseEntity<String>("Updated...",HttpStatus.OK);
	}
	
	@RequestMapping(value="/update-db", method=RequestMethod.GET)
	public ResponseEntity<String> updateDb() throws Exception
	{
		//Logger.geturl();
		System.out.println("/update-brands ");
		Timings.printCurrentTime();
		//redemptionService.sendNotificationAdmin();
		redemptionService.schedulerAtFourPM();
		return new ResponseEntity<String>("Updated...",HttpStatus.OK);
	}
	@RequestMapping(value="/update-notify", method=RequestMethod.GET)
	public ResponseEntity<String> updateDb2() throws Exception
	{
		//Logger.geturl();
		System.out.println("/update-brands ");
		Timings.printCurrentTime();
		redemptionService.sendNotificationAdmin();
		//redemptionService.sendNotificationForNotGetPoint();
		return new ResponseEntity<String>("Updated...",HttpStatus.OK);
	}
}
