package woodoo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import woodoo.service.DooService;
import woodoo.utils.Logger;
import woodoo.utils.Timings;

@Controller
public class DooControllers
{
	@Autowired
	DooService dooService;
	
	@RequestMapping(value="shopping/doo-list", method=RequestMethod.POST)
	public ResponseEntity<String> shoppingDooList(@RequestParam String my_user_id, @RequestParam Integer current_page_no, Integer max_results) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("Doo List");
		return new ResponseEntity<String>(dooService.getDooData(my_user_id, current_page_no,max_results),HttpStatus.OK);
	}
	
	@RequestMapping(value="action/doo-list", method=RequestMethod.GET)
	public ResponseEntity<String> actionDooList(@RequestParam Long my_user_id, @RequestParam Integer current_page_no, Integer max_results) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("Doo List");
		return new ResponseEntity<String>(dooService.actionDooListData(my_user_id, current_page_no,max_results),HttpStatus.OK);
	}
	
	@RequestMapping(value="lockscreen-data", method=RequestMethod.GET)
	public ResponseEntity<String> lockScreenData(@RequestParam Integer flipkartNextIndex,@RequestParam Long my_user_id, @RequestParam Integer espnNextIndex, @RequestParam Integer netGeoNextIndex,@RequestParam Integer toiNextIndex) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("Lock Screen Data");
		return new ResponseEntity<String>(dooService.lockScreenData(my_user_id, espnNextIndex, netGeoNextIndex, toiNextIndex,flipkartNextIndex),HttpStatus.OK);
	}
	
	@RequestMapping(value="doo-days-calculator", method=RequestMethod.POST)
	public ResponseEntity<String> wooDooBax(@RequestParam Long my_user_id, @RequestParam Long time) throws Exception
	{
		Logger.geturl();
		System.out.println("woodoo box api called");
		return new ResponseEntity<String>(dooService.dooDaysCalculator(my_user_id,time),HttpStatus.OK);
	}
	
	@RequestMapping(value="get-notification-of-user", method=RequestMethod.GET)
	public ResponseEntity<String> getNotificationsOfUser(@RequestParam Long my_user_id) throws Exception
	{
		Logger.geturl();
		System.out.println("get-notification-of-user");
		return new ResponseEntity<String>(dooService.getNotificationsOfUser(my_user_id),HttpStatus.OK);
	}
	
	@RequestMapping(value="give-extra-doo-point", method=RequestMethod.POST)
	public ResponseEntity<String> giveExtraDooPoints(@RequestParam Long user_id, @RequestParam Double points, @RequestParam Long date) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("give-extra-doo-point");
		return new ResponseEntity<String>(dooService.giveExtraDooPoints(user_id, points, date),HttpStatus.OK);
	}
	
	@RequestMapping(value="give-extra-doo-point2", method=RequestMethod.POST)
	public ResponseEntity<String> giveExtraDooPoints2(@RequestParam Long user_id, @RequestParam Double points, @RequestParam Long date) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("give-extra-doo-point");
		return new ResponseEntity<String>(dooService.giveExtraDooPoints2(user_id, points, date),HttpStatus.OK);
	}
	
	@RequestMapping(value="give-extra-woo-point", method=RequestMethod.POST)
	public ResponseEntity<String> giveExtraWooPoints(@RequestParam Long user_id, @RequestParam Long points, @RequestParam Long date) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("give-extra-doo-point");
		return new ResponseEntity<String>(dooService.giveExtraWooPoints(user_id, points, date),HttpStatus.OK);
	}
	
	@RequestMapping(value="give-extra-woo-point2", method=RequestMethod.POST)
	public ResponseEntity<String> giveExtraWooPoints2(@RequestParam Long user_id, @RequestParam Long points, @RequestParam Long date) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("give-extra-doo-point");
		return new ResponseEntity<String>(dooService.giveExtraWooPoints2(user_id, points, date),HttpStatus.OK);
	}
	

	@RequestMapping(value="/file-uploader" ,method=RequestMethod.POST,produces = "text/plain;charset=UTF-8")
	public ResponseEntity<String> fileUploader(@RequestPart MultipartFile file, @RequestParam(required=false) String extension)
	{
		Logger.geturl();
		return new ResponseEntity<String>(dooService.fileUpload(file, extension), HttpStatus.OK);
	}

	
	@RequestMapping(value="add-manual-campain", method=RequestMethod.POST)
	public ResponseEntity<String> addManualAdd(@RequestParam String url, @RequestParam String description, @RequestParam String urlToImage,@RequestParam String title , @RequestParam Integer screen_no, @RequestParam Boolean status, @RequestParam Long endDate, @RequestParam Long startDate) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("add-manual-campain"+url+","+description+","+urlToImage+","+title+","+screen_no+","+status+","+endDate+","+startDate);
		return new ResponseEntity<String>(dooService.addManualCampain(url,description,urlToImage,title,screen_no,status,endDate, startDate),HttpStatus.OK);
	}

	@RequestMapping(value="get-all-manual-campain", method=RequestMethod.GET)
	public ResponseEntity<String> getAllManualAdd(@RequestParam(required=false) Long id) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("add-manual-campain");
		return new ResponseEntity<String>(dooService.getAllManualCampain(id),HttpStatus.OK);
	}
	
	@RequestMapping(value="change-status-of-campain", method=RequestMethod.GET)
	public ResponseEntity<String> changeStatusOfManualCampain(@RequestParam Long id) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("add-manual-campain");
		return new ResponseEntity<String>(dooService.changeStatus(id),HttpStatus.OK);
	}

	@RequestMapping(value="delete-camapin", method=RequestMethod.GET)
	public ResponseEntity<String> deleteManualCampain(@RequestParam Long id) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("add-manual-campain");
		return new ResponseEntity<String>(dooService.deleteCampain(id),HttpStatus.OK);
	}
	
}