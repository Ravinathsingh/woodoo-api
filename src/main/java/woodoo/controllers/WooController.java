package woodoo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import woodoo.model.WooPromotions;
import woodoo.service.DooService;
import woodoo.utils.Logger;
import woodoo.utils.Timings;

@Controller
public class WooController {

	@Autowired
	DooService dooService;
	
	@RequestMapping(value="woodoo-box", method=RequestMethod.POST)
	public ResponseEntity<String> wooDooBax(@RequestParam Long my_user_id) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("woodoo box api called");
		return new ResponseEntity<String>(dooService.wooDooBox(my_user_id),HttpStatus.OK);
	}
	
	@RequestMapping(value="add-promotions", method=RequestMethod.POST)
	public ResponseEntity<String> addPromotions(@ModelAttribute WooPromotions promotion) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("add-promotions api called");
		return new ResponseEntity<String>(dooService.addPromotions(promotion),HttpStatus.OK);
	}
	
	@RequestMapping(value="get-all-promotions", method=RequestMethod.GET)
	public ResponseEntity<String> getAllPromotions() throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("get-all-promotions api called");
		return new ResponseEntity<String>(dooService.getAllPromotions(),HttpStatus.OK);
	}
	
	@RequestMapping(value="get-all-promotions-beetween-dates", method=RequestMethod.GET)
	public ResponseEntity<String> getAllPromotionsBetweenBates(@RequestParam Long start_date, @RequestParam Long end_date) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("get-all-promotions-beetween-dates api called");
		return new ResponseEntity<String>(dooService.getAllPromotionsBetweenBates(start_date, end_date),HttpStatus.OK);
	}
	
	@RequestMapping(value="get-all-active-promotions", method=RequestMethod.GET)
	public ResponseEntity<String> getAllActivePromotions() throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("get-all-promotions-beetween-dates api called");
		return new ResponseEntity<String>(dooService.getAllActivePromotions(),HttpStatus.OK);
	}
	
	@RequestMapping(value="missing-woo-cash", method=RequestMethod.POST)
	public ResponseEntity<String> reportMissingWooCash( @RequestParam Long user_id, @RequestParam Long fromDateAndTime, @RequestParam Long  toDateAndTime, @RequestParam String comments) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("missing-woo-cash");
		return new ResponseEntity<String>(dooService.reportMissingWooCash(user_id, fromDateAndTime,toDateAndTime,comments),HttpStatus.OK);
	}
	
	@RequestMapping(value="missing-action-doo-cash", method=RequestMethod.POST)
	public ResponseEntity<String> reportMissingActionDooCash(@RequestParam Long user_id ,@RequestParam String  actionTaken, @RequestParam String  docash, @RequestParam Long  date, @RequestParam String comments) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("missing-action-doo-cash");
		return new ResponseEntity<String>(dooService.reportMissingActionDooCash(actionTaken,docash,date,comments,user_id),HttpStatus.OK);
	}
	
	@RequestMapping(value="missing-shopping-doo-cash", method=RequestMethod.POST)
	public ResponseEntity<String> reportMissingShoppingDooCash(@RequestParam Long user_id ,@RequestParam Long  purchageDate, @RequestParam String  docash, @RequestParam String  storeName, @RequestParam String orderNo, @RequestParam String productName, @RequestParam String productPurchaseValue, @RequestParam String comments) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("missing-shopping-doo-cash");
		return new ResponseEntity<String>(dooService.reportMissingShoppingDooCash(user_id,purchageDate,docash,storeName,orderNo,productName,productPurchaseValue,comments),HttpStatus.OK);
	}
	
	@RequestMapping(value="contact", method=RequestMethod.POST)
	public ResponseEntity<String> contact(@RequestParam String  user_id, @RequestParam String comments) throws Exception
	{
		Logger.geturl();
		Timings.printCurrentTime();
		System.out.println("contact");
		return new ResponseEntity<String>(dooService.contactUs(user_id,comments),HttpStatus.OK);
	}
}
