package woodoo.controllers;

import javax.websocket.server.PathParam;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import woodoo.service.DooService;
import woodoo.utils.Logger;

@Controller
public class LsController {
	
	@Autowired
	DooService dooService;
	
	@RequestMapping(value="woo-time-calculator", method=RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> wooDooBax(@RequestBody String data) throws Exception
	{
		Logger.geturl();
		System.out.println("woodoo box api called");
		JSONObject recoData = new JSONObject(data);
		
		return new ResponseEntity<String>(dooService.wooTimeCalculator(recoData),HttpStatus.OK);
		//return new ResponseEntity<String>("ok",HttpStatus.OK);
	}
	@RequestMapping(value="woo-time-calculator_new", method=RequestMethod.POST)
	public ResponseEntity<String> wooDooBax(@RequestParam Long my_user_id, @RequestParam Long time,@RequestParam Long countdata) throws Exception
	{
		Logger.geturl();
		System.out.println("woodoo box api called");
		return new ResponseEntity<String>(dooService.dooDaysCalculatorNew(my_user_id,time,countdata),HttpStatus.OK);
	}
	
}
