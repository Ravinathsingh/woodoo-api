package woodoo.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.dialect.OracleDialect;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import woodoo.configuration.AppConfig;
import woodoo.model.City;
import woodoo.model.Conversion;
import woodoo.model.RedemptionBrand;
import woodoo.model.State;
import woodoo.model.User;
import woodoo.model.WooLog;
import woodoo.model.WooPromotions;
import woodoo.utils.Timings;

@Repository
public class WooDooDao extends Dao<WooPromotions>{

	@Autowired
	public WooDooDao(AppConfig config) 
	{
		super(config);
	}

	public List<WooPromotions> checkForPromotions(Session session) {

		Criteria criteria = session.createCriteria(WooPromotions.class);
		criteria.add(Restrictions.lt("start_time", Timings.getCurrentTime()));
		criteria.add(Restrictions.gt("end_time", Timings.getCurrentTime()));

		List<WooPromotions> promotions = criteria.list();
		return promotions;
	}
	
	public Boolean checkForPromotionsBeetweenDays(Session session, Long start_date, Long end_date) {

		Criteria criteria = session.createCriteria(WooPromotions.class);
		criteria.add(Restrictions.gt("start_time", start_date));
		criteria.add(Restrictions.le("end_time", end_date));

		List<WooPromotions> promotions = criteria.list();
		if(promotions.size() > 0)
			return true;
		else
			return false;
	}

	public ArrayList<WooPromotions> getPromotionsBetweenDates(Session session, Long start_date, Long end_date) {
		Criteria criteria = session.createCriteria(WooPromotions.class);
		criteria.add(Restrictions.gt("start_time", start_date));
		criteria.add(Restrictions.le("end_time", end_date));

		List<WooPromotions> promotions = criteria.list();
		return (ArrayList<WooPromotions>) promotions;
	}
//
	public Long getLockScreenCashOfUser(Session session, Long my_user_id, String type) {
		Criteria criteria = session.createCriteria(WooLog.class);
		criteria.add(Restrictions.eq("user_id", my_user_id));
		if(type.equalsIgnoreCase("extra"))
		{
			criteria.add(Restrictions.eq("isBonus", true));
			criteria.setProjection(Projections.sum("left_points"));
			Long total = (Long) criteria.uniqueResult();
			System.out.println("To : "+total);
			
			if(total == null)
				return 0l;
			else
				return total;
		}
		else
		{
			criteria.add(Restrictions.eq("isBonus", false));
			criteria.setProjection(Projections.sum("left_points"));
			Long total = (Long) criteria.uniqueResult();
			if(total == null)
				return 0l;
			else
				return total;
		}
		
	}
	
	public Long getLockScreenCashOfUser(Session session, Long my_user_id, String type,Long date1,Long date2,String report) {
		Criteria criteria = session.createCriteria(WooLog.class);
		criteria.add(Restrictions.eq("user_id", my_user_id));
		if(type.equalsIgnoreCase("extra"))
		{
			criteria.add(Restrictions.eq("isBonus", true));
			if(report.equalsIgnoreCase("0")) {
			criteria.add(Restrictions.gt("date_time", date1));
			criteria.add(Restrictions.lt("date_time", date2));
			}
			criteria.setProjection(Projections.sum("left_points"));
			Long total = (Long) criteria.uniqueResult();
			System.out.println("To : "+total);
			
			if(total == null)
				return 0l;
			else
				return total;
		}
		else
		{
			criteria.add(Restrictions.eq("isBonus", false));
			if(report.equalsIgnoreCase("0")) {
			criteria.add(Restrictions.gt("date_time", date1));
			criteria.add(Restrictions.lt("date_time", date2));
			}
			criteria.setProjection(Projections.sum("left_points"));
			Long total = (Long) criteria.uniqueResult();
			if(total == null)
				return 0l;
			else
				return total;
		}
		
	}
	

	public String searchUser(Session session, int maxSize, String email, Long pageNo) {
		
		int start = (int) (pageNo*maxSize);
		int end = start+maxSize;
		Criteria c = session.createCriteria(User.class);
		if(email != null && !email.isEmpty())
		{
			  
			  if(isNumeric(email))
				{			
				  if(email.length()==10)
				  {
					  c.add(Restrictions.like("mobile_no", email, MatchMode.EXACT ));
				  }else
				  {
				   c.add(Restrictions.eq("user_id", Long.parseLong(email)));
				  }
				}else
				{
					c.add(Restrictions.like("email", email, MatchMode.START));
				}
		       
		}
			
		c.setFirstResult(start);
		c.setMaxResults(maxSize);
		c.addOrder(Order.asc("user_id"));
		c.setMaxResults(maxSize);
		
		ArrayList<User> users = (ArrayList<User>) c.list();
		c.setProjection(Projections.rowCount());
		//Long totalEntries = (long) users.size();
		
		
		Criteria c1 = session.createCriteria(User.class);
		if(email != null && !email.isEmpty())
		{
			 if(isNumeric(email))
				{			
				  if(email.length()==10)
				  {
				  c.add(Restrictions.like("mobile_no", email, MatchMode.EXACT ));
				  }else
				  {
				   c.add(Restrictions.eq("user_id", Long.parseLong(email)));
				  }
				}else
				{
					c.add(Restrictions.like("email", email, MatchMode.START));
				}
	}
		c1.addOrder(Order.asc("user_id"));
		c1.setProjection(Projections.rowCount());
		Long totalEntries = (Long) c1.uniqueResult();

		System.out.println("dsjhdsi"+totalEntries);
		JSONObject obj = new JSONObject();
		obj.put("status", "success");
		obj.put("users", new org.json.JSONArray(gson.toJson(users)));
		obj.put("total", totalEntries);
		obj.put("currentPageNo", pageNo);
		
		return obj.toString();
	}
	public static boolean isNumeric(String str) {
	    if (str == null) {
	        return false;
	    }
	    int sz = str.length();
	    for (int i = 0; i < sz; i++) {
	        if (Character.isDigit(str.charAt(i)) == false) {
	            return false;
	        }
	    }
	    return true;
	}
	public static int calculateAgeInYear(Long dob) {
		Long ageInMillis = Timings.getCurrentTime() - dob;
		Long millsinayear =  365 * 24 * 60 * 60 * 1000l;
		int age = (int) (ageInMillis / millsinayear);
		return age;
	}

	public Boolean isNextCheck(Session session, Long user_id) {
		Criteria c = session.createCriteria(User.class);
		c.setMaxResults(1);
		c.add(Restrictions.gt("user_id", user_id));
		if(c.uniqueResult() != null)
		{
			return true;
		}
		return false;
	}

	public Long totalEntries(Session session) {
		Criteria c = session.createCriteria(User.class);
		c.setProjection(Projections.rowCount());
		return (Long) c.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<WooLog> getUnusedWoos(Session session, Long my_user_id) {
		Criteria c = session.createCriteria(WooLog.class);
		c.add(Restrictions.eq("user_id", my_user_id));
		c.add(Restrictions.gt("left_points", 0l));
		c.addOrder(Order.asc("date_time"));
		return (ArrayList<WooLog>) c.list();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Conversion> getUnusedDoos(Session session, Long my_user_id) {
		Criteria c = session.createCriteria(Conversion.class);
		c.add(Restrictions.eq("user_id", my_user_id));
		c.add(Restrictions.gt("left_points", 0.0));
		c.add(Restrictions.eq("status", "MYCHOICE"));
		c.addOrder(Order.asc("conversion_time"));
		return (ArrayList<Conversion>) c.list();
	}


	@SuppressWarnings("unchecked")
	public ArrayList<Conversion> getUnusedDoosOfUser(Session session, Long my_user_id) {
		Criteria c = session.createCriteria(Conversion.class);
		c.add(Restrictions.eq("user_id", my_user_id));
		c.add(Restrictions.gt("left_points", 0.0));
		c.addOrder(Order.asc("conversion_time"));
		return (ArrayList<Conversion>) c.list();
	}
	@SuppressWarnings("unchecked")
	public ArrayList<Conversion> getUnusedDoosOfUser(Session session, Long my_user_id,Long date1,Long date2) {
		Criteria c = session.createCriteria(Conversion.class);
		c.add(Restrictions.eq("user_id", my_user_id));
		c.add(Restrictions.gt("left_points", 0.0));
		c.add(Restrictions.gt("conversion_time", date1));
		c.add(Restrictions.lt("conversion_time", date2));
		c.addOrder(Order.asc("conversion_time"));
		return (ArrayList<Conversion>) c.list();
	}

	
	public ArrayList<Conversion> getUsedDoos(Session session, Long user_id) {
		Criteria c = session.createCriteria(Conversion.class);
		c.add(Restrictions.eq("user_id", user_id));
		c.add(Restrictions.sqlRestriction("users_payout > left_points"));
		return (ArrayList<Conversion>) c.list();
	}

	public Collection<? extends State> getSortedStates(Session session, String string, Long country_id,
			Class<State> class1) {
		Criteria c = session.createCriteria(State.class);
		c.add(Restrictions.eq("country_id", country_id));
		c.addOrder(Order.asc("state_name"));
		return (ArrayList<State>) c.list();
	}

	public Collection<? extends City> getSortedCity(Session session, String string, Long state_id, Class<City> class1) {
		Criteria c = session.createCriteria(City.class);
		c.add(Restrictions.eq("state_id", state_id));
		c.addOrder(Order.asc("city_name"));
		return (Collection<? extends City>) c.list();
	}

	public ArrayList<RedemptionBrand> getallBrands(Session session, Class<RedemptionBrand> class1) {
		Criteria c = session.createCriteria(RedemptionBrand.class);
		c.addOrder(Order.asc("amount"));
		return (ArrayList<RedemptionBrand>) c.list();
	}
}