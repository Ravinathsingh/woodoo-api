package woodoo.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DaoFactory extends Dao
{
	@Autowired
	WooDooDao wooDoodao;

	public WooDooDao getWooDoodao() {
		return wooDoodao;
	}

	public void setWooDoodao(WooDooDao wooDoodao) {
		this.wooDoodao = wooDoodao;
	}
	
	
	
}
