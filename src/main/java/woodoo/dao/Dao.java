package woodoo.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.NonUniqueResultException;
import javax.sql.DataSource;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.google.gson.Gson;

import woodoo.configuration.AppConfig;
import woodoo.utils.NoSuchEntityException;
import woodoo.utils.Timings;


@Component
public class Dao<T> {
	
	protected DataSource dataSource;
	protected Gson gson;
	
	@Autowired
	public Dao(AppConfig config){
		this.dataSource = config.getDataSource();
		this.gson = config.getGson();
	}
	
	public Dao(){
	}
	
	public T saveOrUpdate(Session session , T t) throws ConstraintViolationException
	{
		Long start_time = Timings.getCurrentTime();
		try{
		  	Transaction tx = session.beginTransaction();
	  		session.saveOrUpdate(t);
	  		tx.commit();
	  		return t;
		}finally{
		  System.out.println(Timings.getCurrentTime() - start_time);
		}
	}
	
   public T update(Session session , T t) {
	   Transaction tx= session.beginTransaction();
	   session.update(t);
	   tx.commit();
	   return t;
   }
   
   public T merge(Session session , T t) {
	   Transaction tx= session.beginTransaction();
	   session.merge(t);
	   tx.commit();
	   return t;
   }
	
   public T insert(Session session , T t) {
	   Transaction tx= session.beginTransaction();
	   session.save(t);
	   tx.commit();
	   return t;
   }
   
   public T delete(Session session , T t) {
	   Long start_time = Timings.getCurrentTime();
	try{
	   Transaction tx= session.beginTransaction();
	   session.delete(t);
	   tx.commit();
	   return t;
	}
	finally{
		System.out.println(Timings.getCurrentTime() - start_time);
	}
   }
   
   public int truncate(Session session,  T t)  {
	    int rowsAffected = 0;
	    System.out.println("db cleaning");
	        String hql = "delete from RedemptionBrand";
	        Query q = session.createQuery( hql );
	        rowsAffected = q.executeUpdate();
	    return rowsAffected;
	}
   
   public void deletelistbyRestrictionIn (Session session, List value, Long user_id) throws NoSuchEntityException{
		Transaction tx  = session.beginTransaction();
		try{
			String deleteQuery;
			deleteQuery = "delete from Post where user_id = :value1";
			Query query = session.createQuery(deleteQuery);
			query.setLong("value1", user_id);
			
			query.executeUpdate();
		}
		catch(NonUniqueResultException e){
			e.printStackTrace();
			throw new NonUniqueResultException();
		}
		finally{
			tx.commit();
		}
	}
   
   public <T> T getbyId (Session session, long id, Class<T> clazz){
		Transaction tx  = session.beginTransaction();
		T t = (T)session.get(clazz, id);
		tx.commit();
//		if(t==null)
//			throw new NoSuchEntityException(clazz);
		return t;
	}
	
	public T getbyUniqueColumn (Session session, String columnName, Object value, Class<T> clazz) throws NonUniqueResultException{
		Transaction tx  = session.beginTransaction();
		Criteria cr = session.createCriteria(clazz);
		cr.add(Restrictions.eq(columnName, value));
		try{
			T t = (T)cr.uniqueResult();
			tx.commit();
			return t;
		}catch(NonUniqueResultException e){
			e.printStackTrace();
			throw new NonUniqueResultException();
		}finally{
		}
	}

	public ArrayList<T> getlistbyUniqueColumn (Session session, String columnName, Object value, Class<T> clazz){
		Transaction tx  = session.beginTransaction();
		Criteria cr = session.createCriteria(clazz);
		cr.add(Restrictions.eq(columnName, value));
		try{
			ArrayList<T> t = (ArrayList<T>)cr.list();
			tx.commit();
			return t;
		}catch(NonUniqueResultException e){
			e.printStackTrace();
			throw new NonUniqueResultException();
		}finally{
			
		}
	}
	public ArrayList<T> getlistbyUniqueColumnMAX (Session session, String columnName, Object value, Class<T> clazz, Integer MAX){
		Transaction tx  = session.beginTransaction();
		Criteria cr = session.createCriteria(clazz);
		cr.add(Restrictions.eq(columnName, value));
		cr.setMaxResults(MAX);
		try{
			ArrayList<T> t = (ArrayList<T>)cr.list();
			tx.commit();
			return t;
		}catch(NonUniqueResultException e){
			e.printStackTrace();
			throw new NonUniqueResultException();
		}finally{
			
		}
	}
	public T getbyUniqueColumns (Session session, Map<String,Object> map , Class<T> clazz)throws Exception{
		Transaction tx  = session.beginTransaction();
		Criteria cr = session.createCriteria(clazz);
		cr.add(Restrictions.allEq(map));
		try{
			T t = (T)cr.uniqueResult();
			tx.commit();
			return t;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			
		}
	}
	
	
	public ArrayList<T> getbyUniqueColumnsList (Session session, Map<String,Object> map , Class<T> clazz) throws Exception{
		Transaction tx  = session.beginTransaction();
		Criteria cr = session.createCriteria(clazz);
		cr.add(Restrictions.allEq(map));
		try
		{
			ArrayList<T> t = (ArrayList<T>) cr.list();
			tx.commit();
			return t;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		finally
		{
		}
	}
	
	public ArrayList<T> getbyUniqueColumnsDisjunction(Session session, Map<String,Object> map , Class<T> clazz)throws Exception{
		Transaction tx  = session.beginTransaction();
		Criteria cr = session.createCriteria(clazz);
		Disjunction or = Restrictions.disjunction();
		or.add(Restrictions.allEq(map));
		cr.add(or);
		try{
			ArrayList<T> t = (ArrayList<T>)cr.list();
			tx.commit();
			return t;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		finally
		{
		}
	}
	
	public ArrayList<T> getall (Session session, Class<T> clazz) 
	{
		Transaction tx  = session.beginTransaction();
		Criteria cr = session.createCriteria(clazz);
		try{
			ArrayList<T> t = (ArrayList<T>)cr.list();
			tx.commit();
			return t;
		}finally{
		}
	}
	
	public Long countRows(Session session, Class<T> clazz, Map<String, Object> map) 
	{
		Transaction tx  = session.beginTransaction();
		Criteria cr = session.createCriteria(clazz);
		cr.add(Restrictions.allEq(map));
		cr.setProjection(Projections.rowCount());
		tx.commit();
		return (Long) cr.uniqueResult();
	}
}
