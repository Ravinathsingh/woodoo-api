package woodoo.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;


public class HttpCaller {

	public static Map<String, String> getHeaderMap() {
		HashMap<String, String> map = new HashMap<String, String>();
		// map.put("Content-Type", content_type);
		return map;
	}

	public static HttpURLConnection get(String url) throws IOException {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");

		// add request header
		con.setRequestProperty("User-Agent", "Mozilla/5.0");

		int responseCode = con.getResponseCode();
		if (responseCode != 200)
			return null;
//		System.out.println("response code:" + responseCode);
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		// print result
		return con;
	}

	public static String getCall(String url) throws IOException {
		System.out.println("call:-"+url);
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		
		// add request header
		//con.setRequestProperty("User-Agent", "Mozilla/5.0");
		con.setRequestProperty("Content-Type", "text/html");
		con.setDoInput(true);
		con.setUseCaches(false);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
		int responseCode = con.getResponseCode();
		if (responseCode != 200)
		{
			return null;
		}
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		// print result
		return response.toString();
	}

	public static String post(String url, String body) throws IOException {
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost request = new HttpPost(url);
		StringEntity params = new StringEntity(body);
		request.addHeader("Content-Type", "");
//		request.addHeader("Authorization", "1bafa5d45e96f6ac7cdca214f5a34fd9");
//		request.addHeader("Accept", "application/json");
		request.setEntity(params);
//		System.out.println("parems = " + request);
		HttpResponse result = httpClient.execute(request);
		String json = EntityUtils.toString(result.getEntity(), "UTF-8");
		return json;
	}

	public static String post1(String url, Map<String, String> headers) throws IOException {
//		System.out.println("URL : "+url);
//		System.out.println("header : "+headers);
		
		URL obj = new URL(url);
		
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
//		con.setRequestProperty("User-Agent", "Mozilla/5.0");
//		con.addRequestProperty("content-type", "text/html");
//		con.addRequestProperty("cache-control", "no-cache");
//		
		for (Entry<String, String> e : headers.entrySet()) {
//			con.set
		}
		// add request header
		
		int responseCode = con.getResponseCode();
		if (responseCode != 200)
			return null;
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		// print result
//		System.out.println("Response  "+response.toString());
//		System.out.println(responseCode);
		return response.toString();
	}
	
	
	public static String post(String url1, Map<String, String> headers) throws IOException {
	    URL url = new URL(url1);
//	    System.out.println("URL : "+url1);
//		System.out.println("header : "+headers);
		
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,String> param : headers.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);

        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
       
        StringBuilder sb = new StringBuilder();
        for (int c; (c = in.read()) >= 0;)
            sb.append((char)c);
        String response = sb.toString();
		return response;
	}
	
	public static String postCup(String url1, Map<String, String> headers) throws IOException {
	    URL url = new URL(url1);
//	    System.out.println("URL : "+url1);
//		System.out.println("header : "+headers);
		
        StringBuilder postData = new StringBuilder();
        for (Map.Entry<String,String> param : headers.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes("UTF-8");

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        //conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);

        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
       
        StringBuilder sb = new StringBuilder();
        for (int c; (c = in.read()) >= 0;)
            sb.append((char)c);
        String response = sb.toString();
		return response;
	}
	public static String sendPost(String url1, Map<String, String> headers) throws Exception {

		

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url1);

		// add header
		//post.setHeader("User-Agent", USER_AGENT);

		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		for (Map.Entry<String,String> param : headers.entrySet()) {
			urlParameters.add(new BasicNameValuePair(param.getKey(), param.getValue()));
		}
		
		

		post.setEntity(new UrlEncodedFormEntity(urlParameters));

		HttpResponse response = client.execute(post);
		System.out.println("\nSending 'POST' request to URL : " + url1);
		System.out.println("Post parameters : " + post.getEntity());
		System.out.println("Response Code : " + 
                                    response.getStatusLine().getStatusCode());

		BufferedReader rd = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		System.out.println(result.toString());
     return result.toString();
	}

}