package woodoo.network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;

import com.amazonaws.util.json.JSONException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import woodoo.utils.RSSFeedParser;
import woodoo.views.Coupons;
import woodoo.views.NewsView;
import woodoo.views.OffersView;

public class NewsApiCaller {
	
	//public static String toi = "https://newsapi.org/v1/articles?source=the-times-of-india&sortBy=latest&apiKey=5b2ddd3b20964611a06a265840e199ef";
	//public static String ngc = "https://newsapi.org/v1/articles?source=national-geographic&sortBy=top&apiKey=5b2ddd3b20964611a06a265840e199ef";
	//public static String espn = "https://newsapi.org/v1/articles?source=espn-cric-info&sortBy=latest&apiKey=5b2ddd3b20964611a06a265840e199ef";
	
	public static String toi = "https://news.google.com/news/rss/headlines/section/topic/NATION?ned=in&hl=en-IN&gl=IN";
    public static String ngc = "https://news.google.com/news/rss/headlines/section/topic/ENTERTAINMENT.en_in/Entertainment?ned=in&hl=en-IN&gl=IN";
	public static String espn = "https://news.google.com/news/rss/explore/section/q/Cricket/Cricket?ned=in&hl=en-IN&gl=IN";

	public static ArrayList<NewsView> newsApiCallerTOI() throws IOException{
		/*Gson gson = new Gson();
		String output = HttpCaller.getCall(toi);
		JSONObject obj = new JSONObject(output);
		JSONArray dataArray = new JSONArray(obj.get("articles").toString());
		
		ArrayList<NewsView> userSearchArrayList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(dataArray.toString(), NewsView[].class)));
//		System.out.println(	gson.toJson(userSearchArrayList.get(0)));
		return userSearchArrayList;*/
		 RSSFeedParser parser = new RSSFeedParser(toi);
    	 ArrayList<NewsView> feed = parser.readFeed();
    	 return feed;
	}
	
	public static ArrayList<NewsView> newsApiCallerESPN() throws IOException{
		RSSFeedParser parser = new RSSFeedParser(espn);
   	 ArrayList<NewsView> feed = parser.readFeed();
   	 return feed;
	}
	
	public static ArrayList<NewsView> newsApiCallerNGC() throws IOException, JSONException{
		 RSSFeedParser parser = new RSSFeedParser(ngc);
    	 ArrayList<NewsView> feed = parser.readFeed();
    	 return feed;

	}
	
	public static void main(String[] args) throws IOException, JSONException{ 
		ArrayList<NewsView> offers = newsApiCallerESPN();
		System.out.println(offers.size());
	}
	
}
