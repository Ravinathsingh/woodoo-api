package woodoo.network;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PipedInputStream;
import java.util.Properties;
import java.util.Random;
import java.util.zip.ZipOutputStream;
import javax.activation.*;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.web.multipart.MultipartFile;

import woodoo.utils.Constants;

public class EmailSender extends Thread 
{
	static Random random = new Random();
	//String to_email;
	String text;
	//MultipartFile file;
	MultipartFile file;
	String subject;
	//String file;
	String to_email;
	ZipOutputStream zipFile;
	PipedInputStream in;

	public EmailSender(String text, String subject,String to_email)
	{
		super();
		//this.to_email = to_email;
		this.text = text;
		this.file = null;
		this.subject = subject;
		this.to_email = to_email;
	}

	public EmailSender(String text, MultipartFile file, String subject, String to_email)
	{
		super();
		//this.to_email = to_email;
		this.text = text;
		this.file = file;
		this.subject = subject;
		this.to_email = to_email;
	}

	public EmailSender(String text, ZipOutputStream zipFile, String subject, String to_email)
	{
		super();
		//this.to_email = to_email;
		this.text = text;
		this.zipFile = zipFile;
		this.subject = subject;
		this.to_email = to_email;
	}

	public void run() 
	{
		long start = System.currentTimeMillis();
		final String username = Constants.EMAIL;
		final String password = Constants.PASSWORD;

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator()	{
			protected PasswordAuthentication getPasswordAuthentication()
			{
				return new PasswordAuthentication(username, password);
			}
		});
		//String code = String.valueOf(random.nextInt(9000) + 1000);



		try 
		{

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to_email));
			message.setSubject(subject);
			MimeBodyPart bodyPart1 = new MimeBodyPart();
			bodyPart1.setText(text);

			MimeMultipart multipart = new MimeMultipart();
			File file2 = null;
			if(file != null)
			{
				MimeBodyPart bodyPart2 = new MimeBodyPart();

				try 
				{
					file2 = convert(file);
				} 
				catch (IllegalStateException e) 
				{
					e.printStackTrace();
					System.out.println(e);
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}
				DataSource data = new FileDataSource(file2);  

				bodyPart2.setDataHandler(new DataHandler(data));
				bodyPart2.setFileName("Bill");

				//			multipart.addBodyPart((BodyPart) file);

				multipart.addBodyPart(bodyPart2);
			}

			if(in != null)
			{
				MimeBodyPart bodyPart2 = new MimeBodyPart(in);

				/*try 
				{
					bodyPart2.putByteStream(zipFile);
				} 
				catch (IllegalStateException e) 
				{
					e.printStackTrace();
					System.out.println(e);
				} 
				catch (IOException e) 
				{
					e.printStackTrace();
				}*/
				DataSource data = (DataSource) new ZipOutputStream(zipFile);  

				bodyPart2.setDataHandler(new DataHandler(data));
				bodyPart2.setFileName("Bill");

				//			multipart.addBodyPart((BodyPart) file);

				multipart.addBodyPart(bodyPart2);
			}
			multipart.addBodyPart(bodyPart1);

			message.setContent(multipart);

			Transport.send(message);
			if(file2 != null)
				file2.delete();
			
			System.out.println( subject + " Mail sent");
			long end = System.currentTimeMillis();
			System.out.print((end-start)/1000);

		} catch (MessagingException e) {
			//return new JSONObject().put("status", "fail").toString();
			System.out.println("mail Failed" + " " + e);
		}



	}

	@SuppressWarnings("deprecation")
	public static String verifyPhone(String phone) throws Exception
	{
		String code = String.valueOf(random.nextInt(9000) + 1000);
		/*String url = Constants.SMS_VERIFY_URL.replace("{phone}", phone);
		//url = url.replace("{from}", "Codeyeti");
		url = url.replace("{MESSAGE}",URLEncoder.encode("Your throwback Code is "+ code));
		String json = HttpCaller.get(url);

		JSONObject jobj = new JSONObject(json);
		if(!jobj.getJSONObject("data").getJSONObject("0").get("status").equals("AWAITED-DLR"))
			return null;*/
		return code;
	}
	public static void main(String[] args) throws Exception {
		String text="<p>Hey there!</p><p>Welcome on-board. Congratulations on registering with the Throwback mobile app – a choice well made! We are delighted and can't wait to share our beloved products and services with you. Through every celebration, adventure and achievement, Throwback helps you relive your most precious memories by bringing them to life.</p>Please don't hesitate to get in touch with any questions or comments you may have! We  are here to help</p>Happy Throwbackin’!</p><p align='center'>Let’s be friends</p>><p align='center'>Facebook Instagram Pinterest (logos with embedded links)</p><p align='center'>Sent with Love from Throwback HQ</p><p align='center'>#72, Mahatma Gandhi Road, Bangalore - 560001, Karnataka, INDIA.</p>";
		EmailSender mail = new EmailSender(text, "Thankyou for using emblaze", "chandan@codeyeti.in");
		mail.start();
		System.out.println("dsfsd");
		//verifyPhone("7045032993");
	}

	public File convert(MultipartFile file) throws IOException
	{    
		File convFile = new File("/home/ubuntu/billpic/" + file.getOriginalFilename());
		convFile.createNewFile(); 
		FileOutputStream fos = new FileOutputStream(convFile); 
		fos.write(file.getBytes());
		fos.close(); 
		return convFile;
	}

	/*public File convert(ZipOutputStream zipFile,String fileName) throws IOException
	{    
		//File convFile = new File("/home/ubuntu/throwback/" + fileName);/home/codeyeti/Downloads/
		File convFile = new File("/home/codeyeti/Downloads/" + fileName);
		convFile.createNewFile(); 
		FileOutputStream fos = new FileOutputStream(convFile); 
		fos.write(zipFile.);
		fos.close(); 
		return convFile;
	}*/

}
