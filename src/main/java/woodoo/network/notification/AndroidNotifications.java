package woodoo.network.notification;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;


import com.google.gson.Gson;

import woodoo.model.User;
import woodoo.network.notification.sender.Content;
import woodoo.network.notification.sender.GCMHandler;

public class AndroidNotifications {
	
	Gson gson = new Gson();
	
	public static String generalNotificationToUser(User user, Map<String, String> map) {
		System.out.println(user);
		if(user.getGcm_id() != null)
		{
			List<String> gcm_ids = new ArrayList<>();
			gcm_ids.add(user.getGcm_id());
			Content content = new Content(gcm_ids, map);
			GCMHandler.execute(content);
			System.out.println("Andrioid Notification Success : "+user.getFirst_name()+" Map data: "+map.toString());
			return "Sent Successfully.";
		}
		return "sent";
	}
	
	public static String generalNotificationToUsers(List<String> gcm_ids, Map<String, String> map) {
		
			
			Content content = new Content(gcm_ids, map);
			GCMHandler.execute(content);
		
		return "sent";
	}

	public static void main(String[] args) {
		HashMap<String, String> map = new HashMap<>();
		map.put("message", "Want Upto 300 Assured Paytm Cash + 100% Smartphone ValueBack? Check setting in WooDoo to ensure that it is \"ON\" T&C Apply. www.woodoo.co");
		map.put("type", "general");
		map.put("title", "WooDoo");
//		
//		Scanner scan = new Scanner(System.in);
//		String s = scan.toString();
//		System.out.println(s);
//		
		List<String> gcm_ids = new ArrayList<>();
		gcm_ids.add("fXeHWVzIegs:APA91bG2_FR1H3kjYw4vW156uygDqvGBNSHO6lPspn57l0ThZznYk0iaFxLTrXBX-vH0-CpKOsUSIrdx7YSgiWE8MwAh2SpJB4eF2tCj4FWDQXmFdsdBlsKRZNdvX2IIyKzEWBCDnT6N");

		Content content = new Content(gcm_ids, map);
		GCMHandler.execute(content);
	}
}
