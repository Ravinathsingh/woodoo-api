package woodoo.network.notification.sender;

public class GCMThread implements Runnable{

		Content content;
		String apikey;

		public GCMThread(Content content,String api_key) {
			super();
			this.content = content;
			this.apikey = api_key;
		}
		public void run(){
			PostGCM.post(apikey, content);
		}
}
