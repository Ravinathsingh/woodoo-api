package woodoo.network.notification.sender;


import java.io.Serializable;
import java.util.List;
import java.util.Map;



public class Content implements Serializable{
	List<String> registration_ids;
	Map<String,String> data;
	Boolean delay_while_idle;
	String priority;
	
	public Content(List<String> registration_ids, Map<String, String> data) {
		super();
		this.registration_ids = registration_ids;
		this.data = data;
	}

	public List<String> getRegistration_ids() {
		return registration_ids;
	}

	public void setRegistration_ids(List<String> registration_ids) {
		this.registration_ids = registration_ids;
	}

	public Map<String, String> getData() {
		return data;
	}

	public void setData(Map<String, String> data) {
		this.data = data;
	}

	public Boolean getDelay_while_idle() {
		return delay_while_idle;
	}

	public void setDelay_while_idle(Boolean delay_while_idle) {
		this.delay_while_idle = delay_while_idle;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}
}
