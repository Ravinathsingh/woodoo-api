package woodoo.network.notification.sender;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import woodoo.utils.Constants;



public class GCMHandler {
	
	public static final String API_KEY = Constants.FCM_API_KEY;
	static final int THREADS = 5;
	
//	public static void execute(HashMap<String, String> map, HashMap<Long, String> receivers){
//    	ExecutorService executor = Executors.newFixedThreadPool(THREADS);
//    	for (Entry<Long, String> e : receivers.entrySet()) {
//    		System.out.println(e.getValue());
//			map.put("receiving_user_id", String.valueOf(e.getKey()));
//			Message message = getMessage(map);
//			NotificationWorker worker = new NotificationWorker(sender, message, e.getValue(), RETRIES,e.getKey());
//			executor.execute(worker);
//    	}
//    }
	
	public static void execute(Content content){
		ExecutorService executor = Executors.newFixedThreadPool(THREADS);
		GCMThread gcm = new GCMThread(content, API_KEY);
		executor.execute(gcm);
    	
	}
	
	public static void execute(List<Content> contents){
//		ExecutorService executor = Executors.newFixedThreadPool(THREADS);
//		GCMThread gcm = new GCMThread(content, API_KEY);
//		executor.execute(gcm);
		
		ExecutorService executor = Executors.newFixedThreadPool(Constants.THREADS);
		ArrayList<Runnable> callables = new ArrayList<Runnable>();
		for(Content content : contents){
			GCMThread gcm = new GCMThread(content, API_KEY);
			executor.execute(gcm);
		}
		
    	
	}

}
