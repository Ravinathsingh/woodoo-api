package woodoo.network.redemption;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.springframework.stereotype.Component;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import woodoo.network.HttpCaller;
import woodoo.utils.Constants;
import woodoo.utils.Timings;
import woodoo.views.BrandDenominations;
import woodoo.views.BrandStore;
import woodoo.views.Coupons;
import woodoo.model.RedemptionBrand;

@Component
public class RedemptionList {
	
public String generateAccessToken() throws IOException, JSONException, ParseException {
		System.out.println("Access token : "+Constants.accessToken+" expire At : "+Constants.expiresAt);
//		if(Constants.accessToken == null || Constants.expiresAt == null || Timings.getCurrentTime() > Constants.expiresAt)
//		{
			String output = HttpCaller.getCall(Constants.tokenUrl);
			JSONObject json = new JSONObject(output);
			Constants.accessToken = (String) json.get("accessToken");
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd ss:mm:hh");
			Date date = format.parse((String)json.get("expiresAt"));
			Constants.expiresAt = date.getTime();
			System.out.println("Updated api access token : "+Constants.accessToken+"   expiry date : "+Constants.expiresAt+" or "+(String)json.get("expiresAt"));
//		}
		return "Successfully updated";
	}
	
	public ArrayList<RedemptionBrand> getRedemptionBrandsList() throws Exception, JSONException, ParseException {
		Gson gson = new Gson();
		String accessToken = generateAccessToken();
		System.out.println(Constants.accessToken);
		Map<String, String> map = new HashMap<>();
		String output;
		
			output = HttpCaller.post(Constants.brandList+Constants.accessToken,"");
		
		System.out.println("output :  "+output);
		com.amazonaws.util.json.JSONArray dataArray = new JSONObject(output).getJSONArray("brands");
		
		ArrayList<RedemptionBrand> brands = new ArrayList<>();
		
		for(int i = 0; i < dataArray.length() ; i++)
		{
			JSONObject o = dataArray.getJSONObject(i);
			System.out.println("Object : "+o.toString());
			
			RedemptionBrand b = new RedemptionBrand();
			b.setHash( o.getString("hash"));
			b.setName(o.getString("name"));
			b.setDescription(o.getString("description"));
			b.setTerms(o.getString("terms"));
			b.setWebsite(o.getString("website"));
			b.setImage(o.getString("image"));
		
			brands.add(b);
		}
		
//		ArrayList<RedemptionBrand> brands = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(dataArray.toString(), RedemptionBrand[].class)));
//		System.out.println(	gson.toJson(userSearchArrayList.get(0)));
		return brands;
	}
	
	public ArrayList<BrandDenominations> getBrandDenominationList(String hash) throws Exception, JSONException, ParseException {
		Gson gson = new Gson();
		String accessToken = generateAccessToken();
		Map<String, String> map = new HashMap<>();
		String output = HttpCaller.post(Constants.brandDenomination+Constants.accessToken+"&hash="+hash,"");
		JSONObject obj = new JSONObject(output);
		System.out.println(obj.get("denominations"));
		ArrayList<BrandDenominations> brands = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(obj.get("denominations").toString(), BrandDenominations[].class)));
		return brands;
	}
	
	public ArrayList<BrandStore> getBrandStoreList(String hash) throws Exception, JSONException, ParseException {
		Gson gson = new Gson();
		String accessToken = generateAccessToken();
		Map<String, String> map = new HashMap<>();
		String output = HttpCaller.getCall(Constants.brandStore+hash);
		JSONArray dataArray = new JSONArray(output);
		ArrayList<BrandStore> brands = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(dataArray.toString(), BrandStore[].class)));
//		System.out.println(	gson.toJson(userSearchArrayList.get(0)));
		return brands;
	}
	
}
