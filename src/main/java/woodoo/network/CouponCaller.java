package woodoo.network;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import org.json.JSONArray;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import javassist.bytecode.stackmap.BasicBlock.Catch;
import woodoo.utils.Constants;
import woodoo.views.Coupons;
import woodoo.views.FlipkartView;
import woodoo.views.OffersView;

public class CouponCaller {

	public static String apiUrl = Constants.vCommissionCoupons;
	public static String actionApiUrl = Constants.vCommissionAction;
	public static String flipkartApiUrl  = Constants.vCommissionFlipkartApi;
	public static String shoppingApiUrl = Constants.vCommissionShopping;
	public static String payoutApi = Constants.payoutApi;

	public static ArrayList<Coupons> couponApiCaller() throws Exception{
		try
		{
			Gson gson = new Gson();
			String output = HttpCaller.getCall(apiUrl);
//			System.out.println(output);
			JSONArray dataArray = new JSONArray(output);
			ArrayList<Coupons> userSearchArrayList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(dataArray.toString(), Coupons[].class)));
	//		System.out.println(	gson.toJson(userSearchArrayList.get(0)));

			return userSearchArrayList;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			throw new Exception();
		}
	}

	public static ArrayList<OffersView> actionApiCaller() throws IOException, JSONException{
		Gson gson = new Gson();
		String output = HttpCaller.getCall(actionApiUrl);
		JSONObject jsonObject = new JSONObject(output);
		JSONObject data = ((JSONObject) jsonObject.get("response")).getJSONObject("data").getJSONObject("data");
		DecimalFormat format = new DecimalFormat("##0.0");
		Iterator<String> keys = data.keys();
		ArrayList<OffersView> offers = new ArrayList<>();
		while(keys.hasNext())
		{
			JSONObject offer = data.getJSONObject(keys.next()).getJSONObject("Offer");
			OffersView offerView = new GsonBuilder().create().fromJson(offer.toString(), OffersView.class);

			
			
			if(!offerView.getName().contains("CPS"))
			{

				if(offerView.getPayout_type().equalsIgnoreCase("cpa_flat"))
				{

//					System.out.println(offerView.getName());
//					if(offerView.getDefault_payout().equalsIgnoreCase("0.00000"))
//					{
					if(offerView.getCurrency() != null && offerView.getCurrency().equalsIgnoreCase("USD"))
					{
						Double pay1 = findPayoutOfOffer(offerView,false);
//						Double pay1  = Double.valueOf(offerView.getDefault_payout());
						 DecimalFormat df = new DecimalFormat("#");
						Double pay = ((pay1 *0.7)*65);
						offerView.setPay(Double.parseDouble(df.format(pay)));
					}else
					{
						Double pay1 = findPayoutOfOffer(offerView,false);
//						Double pay1  = Double.valueOf(offerView.getDefault_payout());
						Double pay = (pay1 *0.8);
						offerView.setPay(Double.parseDouble(format.format(pay)));
					}
//						System.out.println("offer pay 1 " + pay);
//					}
//					else
//					{
//						Double pay = (0.8 * Double.parseDouble(offerView.getDefault_payout()));
//						offerView.setPay(Double.parseDouble(format.format(pay)));
//					}
				}
				else
				{
//					if(offerView.getPercent_payout().equalsIgnoreCase("0.00"))
//					{

						Double pay1 = findPayoutOfOffer(offerView,true);
//					Double pay1 = Double.valueOf(offerView.getPercent_payout());
						Double pay = (pay1 *0.8);
						offerView.setPay(Double.parseDouble(format.format(pay)));
//						System.out.println("offer pay 2 " + pay);
//					}
//					else
//					{
//						Double pay = (0.8 * Double.parseDouble(offerView.getPercent_payout()));
//						offerView.setPay(Double.parseDouble(format.format(pay)));
//					}
				}
				offerView.setThumbnail(thumbnailApiCaller(""+offerView.getId()));
//				System.out.println(offerView.getName());
				offers.add(offerView);
			}
			
			
			
//			if(!offerView.getName().contains("CPS"))
//			{
//				System.out.println(offerView.getPayout_type()+"  "+offerView.getName());
//				offerView.setThumbnail(thumbnailApiCaller(""+offerView.getId()));
//				offers.add(offerView);
//
//				if(offerView.getPayout_type().equalsIgnoreCase("cpa_flat") || offerView.getPayout_type().equalsIgnoreCase("cpc"))
//				{
//					Double pay = (Double.parseDouble(offerView.getDefault_payout()) * 0.8);
//					offerView.setPay(Double.parseDouble(format.format(pay)));
//				}
//				else
//				{
//					if(offerView.getHas_goals_enabled().equals("1")){
//
//						Double pay1 = findPayoutOfOffer(offerView,true);
//						Double pay = (pay1 *0.8);
//						offerView.setPay(Double.parseDouble(format.format(pay)));
//
//					}
//					else{
//
//						Double pay1 = findPayoutOfOffer(offerView, false);
//						Double pay = (pay1 *0.8);
//						offerView.setPay(Double.parseDouble(format.format(pay)));
//
//					}
//				}
//			}
		
		
		
		
		}
		Collections.sort(offers, OffersView.compareByPayout);
		
//		System.out.println(gson.toJson(offers));
		return offers;
	}

	private static Double findPayoutOfOffer(OffersView offerView, boolean isPercent) throws JSONException, IOException {

		Gson gson = new Gson();
		String output = HttpCaller.getCall(payoutApi+offerView.getId());
		JSONObject jsonObject = new JSONObject(output);
		JSONObject data = jsonObject.getJSONObject("response").getJSONObject("data");
		String money = "0.0";
//		System.out.println("****************************");
		JSONObject offer_payout = data.getJSONObject("offer_payout");
		if(offer_payout.has("percent_payout") && isPercent){
			if(Double.valueOf(offer_payout.getString("percent_payout")).doubleValue()!=0.0)
				return Double.valueOf(offer_payout.getString("percent_payout"));
		}
		else
		{
			if(Double.valueOf(offer_payout.getString("payout")).doubleValue()!=0.0)
				return Double.valueOf(offer_payout.getString("payout"));
		}

		if(data.has("goal_payouts"))
		{
				com.amazonaws.util.json.JSONArray goals = data.getJSONArray("goal_payouts");
				if(goals.length() > 0)
				{
					for(int i = 0; i< goals.length(); i++){
						if(isPercent)
						{
							if(goals.getJSONObject(i).has("percent_payout")){
								money = goals.getJSONObject(i).getString("percent_payout");
								break;
							}
							if(goals.getJSONObject(i).has("payout")){
								money = goals.getJSONObject(i).getString("payout");
								break;
							}
						}
						else{
							if(goals.getJSONObject(i).has("payout")){
								money = goals.getJSONObject(i).getString("payout");
								break;
							}

						}
					}



//						if(obj.has("percent_payout"))
//						{
//							money = obj.getString("percent_payout");
//						}
//						else
//						{
//							money = obj.getString("payout");
//						}
//					

				}
//				System.out.println("mo       "+money);
				return (Double.parseDouble(money));

			}
			else
			{

				if(isPercent)
				{
					Double payoutPercent = null;
					JSONObject obj = data.getJSONObject("offer_payout");
					if(obj.has("payout_groups")){
						payoutPercent = getMaxPayout(obj.getJSONArray("payout_groups"), true);
					}else{
					  payoutPercent = Double.parseDouble(obj.getString("percent_payout"));
					}

//					System.out.println("offer_id " + offerView.getId() + " "  + "money " + payoutPercent);
					return payoutPercent;
				}
				else
				{
					Double payoutPercent = null;
					JSONObject obj = data.getJSONObject("offer_payout");
					if(obj.has("payout_groups")){
						payoutPercent = getMaxPayout(obj.getJSONArray("payout_groups"), false);
					}else{
					  payoutPercent = Double.parseDouble(obj.getString("payout"));
					}

					return payoutPercent;
				}
			}
		}

private static Double getMaxPayout(com.amazonaws.util.json.JSONArray obj, Boolean is_percent) throws NumberFormatException, JSONException {

	Double maxpayout = 0.0;
	for(int i  = 0 ; i < obj.length(); i++){
		Double current_payout = 0.0;
		if(is_percent)
			current_payout = 	Double.valueOf(obj.getJSONObject(i).getJSONObject("payout_details").getString("percent_payout"));
		else
			current_payout = 	Double.valueOf(obj.getJSONObject(i).getJSONObject("payout_details").getString("payout"));
		if(current_payout.compareTo(maxpayout)>0){
			maxpayout = current_payout;

		}
		}
	return maxpayout;
	}

private static Double findPayoutOfOfferFlat(OffersView offerView) throws JSONException, IOException {


		Gson gson = new Gson();
		String output = HttpCaller.getCall(payoutApi+offerView.getId());
		JSONObject jsonObject = new JSONObject(output);
		com.amazonaws.util.json.JSONArray data = jsonObject.getJSONObject("response").getJSONObject("data").getJSONArray("goal_payouts");
		DecimalFormat format = new DecimalFormat("##0.0");
		ArrayList<OffersView> offers = new ArrayList<>();

		Double cost = 0.0;
		for(int i = 0 ; i < data.length() ; i++)
		{
			JSONObject obj  = data.getJSONObject(i);
			String o = (String) obj.getJSONObject("payout_details").get("percent_payout");
			Double pay = Double.parseDouble(o);

			if(pay > cost)
				cost = pay;
		}
		format.format(cost);
		return cost;
	}



	public static ArrayList<OffersView> shopingApiCaller() throws IOException, JSONException{
		Gson gson = new Gson();
		String output = HttpCaller.getCall(shoppingApiUrl);
		JSONObject jsonObject = new JSONObject(output);
		JSONObject data = ((JSONObject) jsonObject.get("response")).getJSONObject("data").getJSONObject("data");

		Iterator<String> keys = data.keys();
		ArrayList<OffersView> offers = new ArrayList<>();

		while(keys.hasNext())
		{
			JSONObject offer = data.getJSONObject(keys.next()).getJSONObject("Offer");
			OffersView offerView = new GsonBuilder().create().fromJson(offer.toString(), OffersView.class);
			DecimalFormat format = new DecimalFormat("##0.0");

	      System.out.println(offerView.getName());

			if(offerView.getId() != 414 && offerView.getId() != 3140)
			{
				if(offerView.getName().contains("CPS"))
				{

					if(offerView.getPayout_type().equalsIgnoreCase("cpa_flat"))
					{

					//	System.out.println(offerView);
//						if(offerView.getDefault_payout().equalsIgnoreCase("0.00000"))
//						{
							Double pay1 = findPayoutOfOffer(offerView,false);
//							Double pay1  = Double.valueOf(offerView.getDefault_payout());
							Double pay = (pay1 *0.8);
							offerView.setPay(Double.parseDouble(format.format(pay)));
//							System.out.println("offer pay 1 " + pay);
//						}
//						else
//						{
//							Double pay = (0.8 * Double.parseDouble(offerView.getDefault_payout()));
//							offerView.setPay(Double.parseDouble(format.format(pay)));
//						}
					}
					else
					{
//						if(offerView.getPercent_payout().equalsIgnoreCase("0.00"))
//						{

							Double pay1 = findPayoutOfOffer(offerView,true);
//						Double pay1 = Double.valueOf(offerView.getPercent_payout());
							Double pay = (pay1 *0.8);
							offerView.setPay(Double.parseDouble(format.format(pay)));
//							System.out.println("offer pay 2 " + pay);
//						}
//						else
//						{
//							Double pay = (0.8 * Double.parseDouble(offerView.getPercent_payout()));
//							offerView.setPay(Double.parseDouble(format.format(pay)));
//						}
					}
					offerView.setThumbnail(thumbnailApiCaller(""+offerView.getId()));
//					System.out.println(offerView.getName());
					offers.add(offerView);
				}
			}
		}
		Collections.sort(offers, new Comparator<OffersView>() {
		    @Override
		    public int compare(OffersView o1, OffersView o2) {              
		        return o1.getName().compareToIgnoreCase(o2.getName());
		    }
		});
		return offers;
	}
	

	public static String thumbnailApiCaller(String offerId) throws IOException, JSONException{
		try{
			Gson gson = new Gson();
			String output = HttpCaller.getCall("https://api.hasoffers.com/Apiv3/json?NetworkId=vcm&Target=Affiliate_Offer&Method=getThumbnail&api_key=6611b47aa3fcaded85859c6fa4cab0038d7188f089149fd27bab5fededab38fa&ids%5B%5D="+offerId);
//			System.out.println(output);
			JSONObject jsonObject = new JSONObject(output);
			JSONObject jsonObject1 = jsonObject.getJSONObject("response");
			JSONObject obj = jsonObject1.getJSONArray("data").getJSONObject(0).getJSONObject("Thumbnail");

			Iterator<String> keys = obj.keys();
//			System.out.println(keys.toString());

			JSONObject offer = obj.getJSONObject(keys.next());
//			System.out.println(offer.getString("thumbnail"));

			return offer.getString("thumbnail");
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "NA";
		}

	}
	public static void main(String[] args) throws IOException, JSONException{
	
		ArrayList<OffersView> offers = actionApiCaller();
//		System.out.println(offers.size());
//		OffersView viw = new OffersView();
//		viw.setId(6964l);
//		System.out.println(findPayoutOfOffer(viw, true));
		
//		campaignApiCaller();
		
	}

//	public static ArrayList<FlipkartView> flipkartApiCaller() throws IOException, JSONException{
//		Gson gson = new Gson();
//		String output = HttpCaller.getCall(flipkartApiUrl);
//		JSONObject jsonObject = new JSONObject(output);
//		try{
//		JSONObject data = ((JSONObject) jsonObject.get("response")).getJSONObject("data").getJSONObject("data");
//		Iterator<String> keys = data.keys();
//		ArrayList<FlipkartView> offers = new ArrayList<>();
//		while(keys.hasNext())
//		{
//			JSONObject offer = data.getJSONObject(keys.next()).getJSONObject("OfferFile");
//			FlipkartView offerView = new GsonBuilder().create().fromJson(offer.toString(), FlipkartView.class);
//			offers.add(offerView);
//		}
//		return offers;
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//			return new ArrayList<>();
//		}
//	}


	public static ArrayList<FlipkartView> flipkartApiCaller() throws IOException, JSONException{
		Gson gson = new Gson();
		String output = HttpCaller.getCall(flipkartApiUrl);
		
		
		JSONObject jsonObject = new JSONObject(output);
		try{
		JSONObject data = ((JSONObject) jsonObject.get("response")).getJSONObject("data").getJSONObject("data");
		Iterator<String> keys = data.keys();
		ArrayList<FlipkartView> offers = new ArrayList<>();
		while(keys.hasNext())
		{
			JSONObject offer = data.getJSONObject(keys.next()).getJSONObject("OfferFile");
			FlipkartView offerView = new GsonBuilder().create().fromJson(offer.toString(), FlipkartView.class);
			offers.add(offerView);
		}
		return offers;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ArrayList<>();
		}
	}


	public static ArrayList<FlipkartView> campaignApiCaller() throws IOException, JSONException{
		Gson gson = new Gson();
		String output = HttpCaller.getCall(Constants.FIND_CAMPAIGN_API);
		
		JSONObject jsonObject = new JSONObject(output);
		try{
		JSONObject data = ((JSONObject) jsonObject.get("response")).getJSONObject("data");
		Iterator<String> keys = data.keys();
		ArrayList<FlipkartView> offers = new ArrayList<>();
		
		while(keys.hasNext())
		{
			JSONObject offer = data.getJSONObject(keys.next()).getJSONObject("AdCampaign");
			
			String output1 = HttpCaller.getCall(Constants.FIND_CREATIVE_API+offer.getLong("id"));
			
			JSONObject jsonObject1 = new JSONObject(output1);
			try
			{
			JSONObject data1 = ((JSONObject) jsonObject1.get("response")).getJSONObject("data");
			
			Iterator<String> keysData1 = data1.keys();
			
			while(keysData1.hasNext())
			{
				JSONObject creatives = data1.getJSONObject(keysData1.next()).getJSONObject("AdCampaignCreative");
				
				
				FlipkartView offerView = new FlipkartView();
				offerView.setId(creatives.getLong("id"));
				offerView.setOffer_id(creatives.getLong("offer_id"));
				
				String file = HttpCaller.getCall(Constants.OFFER_FILE_API+creatives.getLong("offer_file_id"));
				
				JSONObject fileJson = new JSONObject(file);
				
					JSONObject fileData = ((JSONObject) fileJson.get("response")).getJSONObject("data");

					
					Iterator<String> ofITR = fileData.keys();
					
					while(ofITR.hasNext())
					{
						JSONObject o =  fileData.getJSONObject(ofITR.next());
						
						if(o.get("url").equals(null) || o.get("url")==null)
							throw new Exception();
						
						offerView.setUrl(o.getString("url"));
					
					}

					offers.add(offerView);		
				
			}
			}catch (Exception ex) {
				System.out.println("Exce occured.");
				ex.printStackTrace();
			}
			
		}

		
//		System.out.println("Final oot :   "+gson.toJson(offers));
		return offers;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	




}
