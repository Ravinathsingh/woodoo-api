package woodoo.network;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;
import com.amazonaws.util.json.JSONException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import woodoo.utils.Constants;
import woodoo.utils.Timings;
import woodoo.views.ConversionView;

public class ConversionApiCaller {
	public static String apiUrl = Constants.vCommissionConversion;

	public static ArrayList<ConversionView> conversionApiCaller(Integer pageNumber) throws IOException, ParseException{
		Gson gson = new Gson();
		String output = HttpCaller.getCall(apiUrl+pageNumber);
		JSONObject obj = new JSONObject(output);
		JSONArray dataArray = obj.getJSONObject("response").getJSONObject("data").getJSONArray("data");
		
		ArrayList<ConversionView> conversionList = new ArrayList<>();
		
		for(int i = 0 ; i < dataArray.length() ; i++)
		{
			JSONObject object = dataArray.getJSONObject(i);
			ConversionView view = new ConversionView();
			
			view.setAffiliate_info(object.getJSONObject("Stat").getString("affiliate_info").toString());
			view.setOffer_id(Long.parseLong(object.getJSONObject("Stat").getString("offer_id").toString()));
			view.setDate(object.getJSONObject("Stat").getString("date").toString());
			view.setPayout(object.getJSONObject("Stat").getString("payout").toString());
			view.setPayout_type(object.getJSONObject("Stat").getString("payout_type").toString());
			view.setOfferName(object.getJSONObject("Offer").getString("name").toString());
			view.setConversion(Long.valueOf((object.getJSONObject("Stat").getString("conversions"))));
			view.setHour(Integer.valueOf((object.getJSONObject("Stat").getString("hour"))));
			
			if(object.getJSONObject("Stat").has("affiliate_info2") && !object.getJSONObject("Stat").getString("affiliate_info2").equalsIgnoreCase("") )
			{
				view.setUniqueId(object.getJSONObject("Stat").getLong("affiliate_info2") );
			}
			else
			{
				view.setUniqueId(9999999l);
			}
			
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date date = format.parse(view.getDate());
			view.setDatetime(date.getTime());
			conversionList.add(view);
		}
		
//		ArrayList<ConversionApiCaller> userSearchArrayList = new ArrayList<>(Arrays.asList(new GsonBuilder().create().fromJson(dataArray.toString(), ConversionApiCaller[].class)));
//		System.out.println(	gson.toJson(conversionList));
		return conversionList;
	}
	
	public static void main(String[] args) throws IOException, JSONException, ParseException{ 
		ArrayList<ConversionView> offers = conversionApiCaller(1);
		Gson gson = new Gson();
//		System.out.println(gson.toJson(offers));
	}
}
