package woodoo.views;

public class Coupons {
	String featured;
	String exclusive;
	String promo_id;
	String offer_id;
	String offer_name;
	String coupon_title;
	String category;
	String coupon_description;
	String coupon_type;
	String coupon_code;
	String ref_id;
	String link;
	String coupon_expiry;
	String added;
	String preview_url;
	String store_link;
	String store_image;
	
	
	public String getFeatured() {
		return featured;
	}
	public void setFeatured(String featured) {
		this.featured = featured;
	}
	public String getExclusive() {
		return exclusive;
	}
	public void setExclusive(String exclusive) {
		this.exclusive = exclusive;
	}
	public String getPromo_id() {
		return promo_id;
	}
	public void setPromo_id(String promo_id) {
		this.promo_id = promo_id;
	}
	public String getOffer_id() {
		return offer_id;
	}
	public void setOffer_id(String offer_id) {
		this.offer_id = offer_id;
	}
	public String getOffer_name() {
		return offer_name;
	}
	public void setOffer_name(String offer_name) {
		this.offer_name = offer_name;
	}
	public String getCoupon_title() {
		return coupon_title;
	}
	public void setCoupon_title(String coupon_title) {
		this.coupon_title = coupon_title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCoupon_description() {
		return coupon_description;
	}
	public void setCoupon_description(String coupon_description) {
		this.coupon_description = coupon_description;
	}
	public String getCoupon_type() {
		return coupon_type;
	}
	public void setCoupon_type(String coupon_type) {
		this.coupon_type = coupon_type;
	}
	public String getCoupon_code() {
		return coupon_code;
	}
	public void setCoupon_code(String coupon_code) {
		this.coupon_code = coupon_code;
	}
	public String getRef_id() {
		return ref_id;
	}
	public void setRef_id(String ref_id) {
		this.ref_id = ref_id;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getCoupon_expiry() {
		return coupon_expiry;
	}
	public void setCoupon_expiry(String coupon_expiry) {
		this.coupon_expiry = coupon_expiry;
	}
	public String getAdded() {
		return added;
	}
	public void setAdded(String added) {
		this.added = added;
	}
	public String getPreview_url() {
		return preview_url;
	}
	public void setPreview_url(String preview_url) {
		this.preview_url = preview_url;
	}
	public String getStore_link() {
		return store_link;
	}
	public void setStore_link(String store_link) {
		this.store_link = store_link;
	}
	public String getStore_image() {
		return store_image;
	}
	public void setStore_image(String store_image) {
		this.store_image = store_image;
	}
	
	
}
