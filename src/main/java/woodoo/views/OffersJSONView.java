package woodoo.views;

import java.util.Comparator;

public class OffersJSONView {
	String link;
	String payout;
	String description;
	String conversion_flow;
	String special_instructions;
	String thumbnail;
	Double intPayout;
	String our_instructions;
	String name;
	
	public static Comparator<OffersJSONView> compareByPayout = new Comparator<OffersJSONView>() {
        
        @Override
        public int compare(OffersJSONView o2, OffersJSONView o1)
        {
                return o1.getIntPayout().compareTo(o2.getIntPayout());
        }
	};
	
	
	
	
	public String getOur_instructions() {
		return our_instructions;
	}
	public void setOur_instructions(String our_instructions) {
		this.our_instructions = our_instructions;
	}
	
	public Double getIntPayout() {
		return intPayout;
	}
	public void setIntPayout(Double intPayout) {
		this.intPayout = intPayout;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getConversion_flow() {
		return conversion_flow;
	}
	public void setConversion_flow(String conversion_flow) {
		this.conversion_flow = conversion_flow;
	}
	public String getSpecial_instructions() {
		return special_instructions;
	}
	public void setSpecial_instructions(String special_instructions) {
		this.special_instructions = special_instructions;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getPayout() {
		return payout;
	}
	public void setPayout(String payout) {
		this.payout = payout;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}