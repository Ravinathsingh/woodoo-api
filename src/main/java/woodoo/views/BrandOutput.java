package woodoo.views;

public class BrandOutput {
	Long denomination_value;
	Long id;
	String type;
	String skuId;
	
	public String getSkuId() {
		return skuId;
	}
	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}
	public Long getDenomination_value() {
		return denomination_value;
	}
	public void setDenomination_value(Long denomination_value) {
		this.denomination_value = denomination_value;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
}
