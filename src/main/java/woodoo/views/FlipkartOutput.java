package woodoo.views;

import java.util.Random;

public class FlipkartOutput {
	String urlToImage;
	String url;
	String title;
	String description;
	Boolean is_manual;
	Integer nextIndex;
	
	
	
	public String getUrlToImage() {
		return urlToImage;
	}
	public void setUrlToImage(String urlToImage) {
		this.urlToImage = urlToImage;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getIs_manual() {
		return is_manual;
	}
	public void setIs_manual(Boolean is_manual) {
		this.is_manual = is_manual;
	}
	public Integer getNextIndex() {
		return nextIndex;
	}
	public void setNextIndex(Integer nextIndex) {
		this.nextIndex = nextIndex;
	}
	
	
	
	
}
