package woodoo.views;

import java.util.Arrays;

public class BrandDenominations {
	Long id;
    Long name;
    String skuId;
    String type;
    String valueType;
    Long minimumDenomination;
    Long maximumDenomination;
    
	public Long getMinimumDenomination() {
		return minimumDenomination;
	}
	public void setMinimumDenomination(Long minimumDenomination) {
		this.minimumDenomination = minimumDenomination;
	}
	public void setMaximumDenomination(Long maximumDenomination) {
		this.maximumDenomination = maximumDenomination;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getName() {
		return name;
	}
	public void setName(Long name) {
		this.name = name;
	}
	public String getSkuId() {
		return skuId;
	}
	public void setSkuId(String skuId) {
		this.skuId = skuId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValueType() {
		return valueType;
	}
	public void setValueType(String valueType) {
		this.valueType = valueType;
	}
	
	@Override
	public String toString() {
		return "BrandDenominations [id=" + id + ", name=" + name + ", skuId=" + skuId + ", type=" + type
				+ ", valueType=" + valueType + "]";
	}
    
}
