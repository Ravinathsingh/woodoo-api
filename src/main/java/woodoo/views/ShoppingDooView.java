package woodoo.views;

public class ShoppingDooView {
	 String name;//
	 String description;//
	 String link;//
	 String payout;
	 String thumbnail;
	 String special_instructions;
	 String our_instructions;
	 
	 
	public String getOur_instructions() {
		return our_instructions;
	}
	public void setOur_instructions(String our_instructions) {
		this.our_instructions = our_instructions;
	}
	public String getSpecial_instructions() {
		return special_instructions;
	}
	public void setSpecial_instructions(String special_instructions) {
		this.special_instructions = special_instructions;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getPayout() {
		return payout;
	}
	public void setPayout(String payout) {
		this.payout = payout;
	}
	
	@Override
	public String toString() {
		return "ShoppingDooView [name=" + name + ", description=" + description + ", link=" + link + ", payout="
				+ payout + "]";
	}
	 
}
