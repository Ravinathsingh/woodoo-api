package woodoo.views;

import java.util.Comparator;

public class OffersView {
	Long id;
    String name;
    String description;
    String require_approval;
    String require_terms_and_conditions;
    String terms_and_conditions;
    String preview_url;
    String currency;
    String default_payout;
    String status;
    String expiration_date;
    String payout_type;
    String percent_payout;
    String featured;
    String allow_website_links;
    String allow_direct_links;
    String show_custom_variables;
    String show_mail_list;
    String dne_list_id;
    String email_instructions;
    String email_instructions_from;
    String email_instructions_subject;
    String has_goals_enabled;
    String default_goal_name;
    String use_target_rules;
    String is_expired;
    String dne_download_url;
    String dne_unsubscribe_url;
    String dne_third_party_list;
    String approval_status;
    String thumbnail;
    
    Double pay;
    
    public static Comparator<OffersView> compareByPayout = new Comparator<OffersView>() {
        @Override
        public int compare(OffersView o2, OffersView o1)
        {
             return o1.getPay().compareTo(o2.getPay());
        }
	};
	
	 public static Comparator<OffersView> compareByName = new Comparator<OffersView>() {
	        @Override
	        public int compare(OffersView o1, OffersView o2)
	        {
	             return o1.getName().compareTo(o2.getName());
	        }
		};
	
	
	public Double getPay() {
		return pay;
	}
	public void setPay(Double pay) {
		this.pay = pay;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRequire_approval() {
		return require_approval;
	}
	public void setRequire_approval(String require_approval) {
		this.require_approval = require_approval;
	}
	public String getRequire_terms_and_conditions() {
		return require_terms_and_conditions;
	}
	public void setRequire_terms_and_conditions(String require_terms_and_conditions) {
		this.require_terms_and_conditions = require_terms_and_conditions;
	}
	public String getTerms_and_conditions() {
		return terms_and_conditions;
	}
	public void setTerms_and_conditions(String terms_and_conditions) {
		this.terms_and_conditions = terms_and_conditions;
	}
	public String getPreview_url() {
		return preview_url;
	}
	public void setPreview_url(String preview_url) {
		this.preview_url = preview_url;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDefault_payout() {
		return default_payout;
	}
	public void setDefault_payout(String default_payout) {
		this.default_payout = default_payout;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getExpiration_date() {
		return expiration_date;
	}
	public void setExpiration_date(String expiration_date) {
		this.expiration_date = expiration_date;
	}
	public String getPayout_type() {
		return payout_type;
	}
	public void setPayout_type(String payout_type) {
		this.payout_type = payout_type;
	}
	public String getPercent_payout() {
		return percent_payout;
	}
	public void setPercent_payout(String percent_payout) {
		this.percent_payout = percent_payout;
	}
	public String getFeatured() {
		return featured;
	}
	public void setFeatured(String featured) {
		this.featured = featured;
	}
	public String getAllow_website_links() {
		return allow_website_links;
	}
	public void setAllow_website_links(String allow_website_links) {
		this.allow_website_links = allow_website_links;
	}
	public String getAllow_direct_links() {
		return allow_direct_links;
	}
	public void setAllow_direct_links(String allow_direct_links) {
		this.allow_direct_links = allow_direct_links;
	}
	public String getShow_custom_variables() {
		return show_custom_variables;
	}
	public void setShow_custom_variables(String show_custom_variables) {
		this.show_custom_variables = show_custom_variables;
	}
	public String getShow_mail_list() {
		return show_mail_list;
	}
	public void setShow_mail_list(String show_mail_list) {
		this.show_mail_list = show_mail_list;
	}
	public String getDne_list_id() {
		return dne_list_id;
	}
	public void setDne_list_id(String dne_list_id) {
		this.dne_list_id = dne_list_id;
	}
	public String getEmail_instructions() {
		return email_instructions;
	}
	public void setEmail_instructions(String email_instructions) {
		this.email_instructions = email_instructions;
	}
	public String getEmail_instructions_from() {
		return email_instructions_from;
	}
	public void setEmail_instructions_from(String email_instructions_from) {
		this.email_instructions_from = email_instructions_from;
	}
	public String getEmail_instructions_subject() {
		return email_instructions_subject;
	}
	public void setEmail_instructions_subject(String email_instructions_subject) {
		this.email_instructions_subject = email_instructions_subject;
	}
	public String getHas_goals_enabled() {
		return has_goals_enabled;
	}
	public void setHas_goals_enabled(String has_goals_enabled) {
		this.has_goals_enabled = has_goals_enabled;
	}
	public String getDefault_goal_name() {
		return default_goal_name;
	}
	public void setDefault_goal_name(String default_goal_name) {
		this.default_goal_name = default_goal_name;
	}
	public String getUse_target_rules() {
		return use_target_rules;
	}
	public void setUse_target_rules(String use_target_rules) {
		this.use_target_rules = use_target_rules;
	}
	public String getIs_expired() {
		return is_expired;
	}
	public void setIs_expired(String is_expired) {
		this.is_expired = is_expired;
	}
	public String getDne_download_url() {
		return dne_download_url;
	}
	public void setDne_download_url(String dne_download_url) {
		this.dne_download_url = dne_download_url;
	}
	public String getDne_unsubscribe_url() {
		return dne_unsubscribe_url;
	}
	public void setDne_unsubscribe_url(String dne_unsubscribe_url) {
		this.dne_unsubscribe_url = dne_unsubscribe_url;
	}
	public String getDne_third_party_list() {
		return dne_third_party_list;
	}
	public void setDne_third_party_list(String dne_third_party_list) {
		this.dne_third_party_list = dne_third_party_list;
	}
	public String getApproval_status() {
		return approval_status;
	}
	public void setApproval_status(String approval_status) {
		this.approval_status = approval_status;
	}
	@Override
	public String toString() {
		return "OffersView [id=" + id + ", name=" + name + ", description=" + description + ", require_approval="
				+ require_approval + ", require_terms_and_conditions=" + require_terms_and_conditions
				+ ", terms_and_conditions=" + terms_and_conditions + ", preview_url=" + preview_url + ", currency="
				+ currency + ", default_payout=" + default_payout + ", status=" + status + ", expiration_date="
				+ expiration_date + ", payout_type=" + payout_type + ", percent_payout=" + percent_payout
				+ ", featured=" + featured + ", allow_website_links=" + allow_website_links + ", allow_direct_links="
				+ allow_direct_links + ", show_custom_variables=" + show_custom_variables + ", show_mail_list="
				+ show_mail_list + ", dne_list_id=" + dne_list_id + ", email_instructions=" + email_instructions
				+ ", email_instructions_from=" + email_instructions_from + ", email_instructions_subject="
				+ email_instructions_subject + ", has_goals_enabled=" + has_goals_enabled + ", default_goal_name="
				+ default_goal_name + ", use_target_rules=" + use_target_rules + ", is_expired=" + is_expired
				+ ", dne_download_url=" + dne_download_url + ", dne_unsubscribe_url=" + dne_unsubscribe_url
				+ ", dne_third_party_list=" + dne_third_party_list + ", approval_status=" + approval_status + "]";
	}
    
    
}
