package woodoo.views;


public class OrderResponse {

private String receiptNo;
private Boolean error;
private String message;

public String getReceiptNo() {
return receiptNo;
}

public void setReceiptNo(String receiptNo) {
this.receiptNo = receiptNo;
}

public Boolean getError() {
return error;
}

public void setError(Boolean error) {
this.error = error;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

}