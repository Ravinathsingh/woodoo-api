package woodoo.views;

public class BrandStore {
	 String addressLine1;
     String addressLine2;
     String location;
     String city;
     String state;
     String country;
     String pin;
     String latitude;
     String longitude;
     String verified;
     String hash;
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getVerified() {
		return verified;
	}
	public void setVerified(String verified) {
		this.verified = verified;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
	
	@Override
	public String toString() {
		return "BrandStore [addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", location=" + location
				+ ", city=" + city + ", state=" + state + ", country=" + country + ", pin=" + pin + ", latitude="
				+ latitude + ", longitude=" + longitude + ", verified=" + verified + ", hash=" + hash + "]";
	}
     
}
