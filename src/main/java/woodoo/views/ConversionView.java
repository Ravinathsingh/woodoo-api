package woodoo.views;

/**
 * @author codeyeti
 *
 */
public class ConversionView {
	
	
	String offerName;
	String affiliate_info;
    String date;
    String payout_type;
    String payout;
    Long datetime;
    Long offer_id;
    Long conversion;
    Integer hour;
    Long uniqueId;
    
    
	public Long getOffer_id() {
		return offer_id;
	}
	public void setOffer_id(Long offer_id) {
		this.offer_id = offer_id;
	}
	public String getOfferName() {
		return offerName;
	}
	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}
	public String getAffiliate_info() {
		return affiliate_info;
	}
	public void setAffiliate_info(String affiliate_info) {
		this.affiliate_info = affiliate_info;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getPayout_type() {
		return payout_type;
	}
	public void setPayout_type(String payout_type) {
		this.payout_type = payout_type;
	}
	public String getPayout() {
		return payout;
	}
	public void setPayout(String payout) {
		this.payout = payout;
	}
	public Long getDatetime() {
		return datetime;
	}
	public void setDatetime(Long datetime) {
		this.datetime = datetime;
	}
	@Override
	public String toString() {
		return "ConversionView [offerName=" + offerName + ", affiliate_info=" + affiliate_info + ", date=" + date
				+ ", payout_type=" + payout_type + ", payout=" + payout + ", datetime=" + datetime + ", offer_id="
				+ offer_id + "]";
	}
	public Long getConversion() {
		return conversion;
	}
	public void setConversion(Long conversion) {
		this.conversion = conversion;
	}
	public Integer getHour() {
		return hour;
	}
	public void setHour(Integer hour) {
		this.hour = hour;
	}
	public Long getUniqueId() {
		return uniqueId;
	}
	public void setUniqueId(Long uniqueId) {
		this.uniqueId = uniqueId;
	}
	
}
