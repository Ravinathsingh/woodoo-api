package woodoo.configuration;


import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Configuration
public class AppConfig{
	
	@Autowired
	DriverManagerDataSource dataSource;
	
	@Autowired 
	SessionFactory sessionFactory;
	
	GsonBuilder gson_builder  = new GsonBuilder().serializeNulls(); 
	
	Gson gson = gson_builder.create();
	
	public Gson getGson() {
		return gson_builder.create();
	}
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	public DriverManagerDataSource getDataSource() {
		return dataSource;
	}
	public void setDataSource(DriverManagerDataSource dataSource) {
		this.dataSource = dataSource;
	}
	public void setGson(Gson gson) {
		this.gson = gson;
	}
	
	
	
	
	
	
}