drop database if exists `woodoo`;
create database `woodoo`;
use `woodoo`;

# 24-09-2016

CREATE TABLE USER(
	user_id BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	first_name text,
	last_name text,
	dob varchar(50),
	gender varchar(10),
	email varchar(50),
	mobile_no bigint unsigned,
	password text,
	phone_value bigint unsigned,
	state text,
	city text,
	pin_code text,
	address_one text,  # pic, video
	address_two text,
	membership_plan text,
	facebook_id varchar(50),
	my_referral_code varchar(15),
	used_referral_code varchar(15),
	
	PRIMARY KEY(user_id),
	INDEX user_table_index0(email),
	INDEX user_table_index1(facebook_id),
	INDEX user_table_index2(user_id),
	INDEX uesr_table_index3(my_referral_code),
	INDEX uesr_table_index4(used_referral_code),
	UNIQUE KEY user_table_referral_code(my_referral_code)
	) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;